import math
import numpy as np
from itertools import combinations, product

def get_iVtx(run, muons, i) :
    # no need to fix vertices after run 305405
    # also no need to fix vertices ir run == 1 (MC)
    #print("Entering get_iVtx() i = {0:d}".format(i))
    if run > 305405 or run == 1:
        return muons[i].vtxIndx()
    else :
        iPtr = 0 
        if i > 0:
            iPtr = len(muons[i-1].vtxIndx())
        #print("iPtr={0:d}".format(iPtr))
            
        fixedList = muons[i].vtxIndx()[iPtr:iPtr+len(muons[i].vtxIndx())]
        dummy = str(fixedList)
        return fixedList 

def nGoodVertex(muons, vertices, run, quartet=None):
    # find the number of muon tracks that are associated with a good vertex
    nGood = 0
    xOff, yOff = 0.086, -0.034
    muonList = quartet if quartet is not None else range(len(muons))
    for j in muonList:
        vtxList = get_iVtx(run, muons, j) 
        for k in range(len(vtxList)) :
            rr = math.sqrt((vertices[k].x()-xOff)**2 + (vertices[k].y()-yOff)**2)
            if rr > 1.0 or vertices[k].chi2() > 20.0:
                continue
            else:
                nGood += 1
                break 
    return nGood

def isGoodPairing_old(vertices, intersections):
    xOff, yOff = 0.086, -0.034
    for i in intersections:
        vtx = vertices[i]
        if (math.sqrt((vtx.x()-xOff)**2 + (vtx.y()-yOff)**2) < 0.75):
            return True
    return False

def isGoodPairing(vtx):
    xOff, yOff = 0.086, -0.034
    if math.sqrt((vtx.x()-xOff)**2 + (vtx.y()-yOff)**2) < 0.75:
        return True
    return False

def analyzeMuonsInPairings(muons, vertices, run, quartet=None):
    muonList = quartet if quartet is not None else range(len(muons))
    vtxLists = {}
    for i in muonList:
        vtxList[i] = get_iVtx(run,muons,i)

    nMuonsInPairings = 0
    for i in muonList:
        for j in muonList:
            if i == j:
                continue
            commonVtxs = np.intersect1d(vtxLists[i], vtxLists[j])
            if commonVtxs.size > 0:
                nMuonsInPairings += 1
                break
    return nMuonsInPairings

def analyzePairing(muons, vertices, run, quartet=None):
    muonList = quartet if quartet is not None else range(len(muons))
    vtxLists = {}
    for i in muonList:
        vtxList = get_iVtx(run,muons,i)
        if len(vtxList) == 0:
            return 0,0
        vtxLists[i] = vtxList 

    vtxMuonMap = {}
    for i in muonList:
        for j in muonList:
            if i >= j:
                continue
            commonVtxs = np.intersect1d(vtxLists[i], vtxLists[j])
            for commonVtx in commonVtxs:
                vtxMuonMap[commonVtx] = [i, j]

    nPairings = len(vtxMuonMap)
    nGoodPairings, nBadPairings = 0, 0
    
    for vtxIdx, muonIdxs in vtxMuonMap.items():
        # Can check if muon charges are the same, but I think we don't want that
        # For eta->4mu, we don't mind if there are pairwise muons with the same charge vertexed together
        # All four muons should come from the same vertex anyway
        if isGoodPairing(vertices[vtxIdx]):
            nGoodPairings += 1
        else:
            nBadPairings += 1

    #print('Total, Good, Bad = {0}, {1}, {2}'.format(nPairings, nGoodPairings, nBadPairings))
    return nGoodPairings, nBadPairings

def analyzePairing_old(muons, vertices, run, evt):
    nMuons = len(muons)
    vLists = []
    for i in range(nMuons):
        vLists.append(get_iVtx(run,muons,i))
        if (len(vLists[-1]) == 0):
            return 0,0

    # printEvent(muons,vertices,run,evt)
    for i in np.arange(nMuons-1)+1:
        if (muons[i].charge() == muons[0].charge()): 
            same = i
            break   
    opposite = np.delete(np.arange(nMuons-1)+1,same-1)

    nGoodPairing, nBadPairing = 0, 0
    vtx1, vtx2 = np.intersect1d(vLists[0], vLists[opposite[0]]), np.intersect1d(vLists[same],vLists[opposite[1]])
    if (len(vtx1) > 0 and len(vtx2) > 0):	# both pairs have vertices
        if (isGoodPairing(vertices,vtx1) and isGoodPairing(vertices,vtx2)):
            nGoodPairing += 1
        else:
            nBadPairing += 1

    vtx1, vtx2 = np.intersect1d(vLists[0], vLists[opposite[1]]), np.intersect1d(vLists[same],vLists[opposite[0]])
    if (len(vtx1) > 0 and len(vtx2) > 0):
        if (isGoodPairing(vertices,vtx1) and isGoodPairing(vertices,vtx2)):
            nGoodPairing += 1
        else:
            nBadPairing += 1

    # print('Good, Bad = {0}, {1}'.format(nGoodPairing, nBadPairing))
    return nGoodPairing, nBadPairing

def printEvent(muons, vertices, run, evt) :
    xOff, yOff = 0.086, -0.034
    nGoodPairings, nBadPairings = analyzePairing(muons, vertices, run)
    print("\n Run={0:d} Evt={1:d} nGoodVtx={2:d} nGoodPairings={3:d} nBadPairings={4:d}".format(
        run,evt,nGoodVertex(muons,vertices,run),nGoodPairings,nBadPairings))
    print("Muons\n #   Q      pt     eta     phi     dxy      dz     iso   iVtx")
    for i in range(len(muons)) :
        vList = get_iVtx(run, muons, i)
        vtxList = ' '
        for j in range(len(vList)) : vtxList += " {0:d}".format(int(vList[j])) 
        print("{0:2d}{1:5.1f}{2:8.2f}{3:8.2f}{4:8.2f}{5:8.4f}{6:8.4f}{7:8.3f}{8:s}".format(
            i,muons[i].charge(),muons[i].pt(),muons[i].eta(),muons[i].phi(),
            muons[i].dxy(),muons[i].dz(),muons[i].trackIso(),vtxList))
    print("Vertices\n #     x      xErr     y     yErr      z       r      chi2 ")
    for i in range(len(vertices)) :
        rr = math.sqrt((vertices[i].x()-xOff)**2 + (vertices[i].y()-yOff)**2)
        print("{0:2d}{1:8.3f}{2:8.3f}{3:8.3f}{4:8.3f}{5:8.3f}{6:8.3f}{7:8.3f}".format(
        i,vertices[i].x()-xOff,vertices[i].xError(),vertices[i].y()-yOff,vertices[i].yError(),vertices[i].z(),rr,vertices[i].chi2()))

def dumpEvent(mass, muons, vertices, tag, count, run, evt) :
    xOff, yOff = 0.086, -0.034
    nGoodPairings, nBadPairings = analyzePairing(muons, vertices, run)
    lines = [
        '\n***\nTag={0:s} Event={1:d} Mass = {2:.3f} nGoodVtx={3:d} nGoodPairings={4:d} nBadPairings={5:d}\n'.format(
        tag,evt,mass,nGoodVertex(muons,vertices,run),nGoodPairings,nBadPairings)]
    lines.append("Muons\n #   Q      pt     eta     phi     dxy      dz     iso    iVtx\n")
    for i in range(len(muons)) :
        vList = get_iVtx(run, muons, i)
        vtxList = ' '
        for j in range(len(vList)) : vtxList += " {0:d}".format(int(vList[j]))
        lines.append("{0:2d}{1:5.1f}{2:8.2f}{3:8.2f}{4:8.2f}{5:8.4f}{6:8.4f}{7:8.3}{8:s}\n".format(
            i,muons[i].charge(),muons[i].pt(),muons[i].eta(),muons[i].phi(),
            muons[i].dxy(),muons[i].dz(),muons[i].trackIso(),vtxList))
    lines.append("Vertices\n #    x       y       z        r      chi2 \n")
    for i in range(len(vertices)) :
        rr = math.sqrt((vertices[i].x()-xOff)**2 + (vertices[i].y()-yOff)**2)
        lines.append("{0:2d}{1:8.3f}{2:8.3f}{3:8.3f}{4:8.3f}{5:8.3f}\n".format(
            i,vertices[i].x()-xOff,vertices[i].y()-yOff,vertices[i].z(),rr,vertices[i].chi2()))

    open('{0:s}_{1:03d}.evt'.format(tag,count),'w').writelines(lines)

def getQuartets(muons, decay_mode):
    nMuons = len(muons)
    if nMuons < decay_mode:
        return []

    quartets = combinations(range(nMuons), decay_mode)
    return list(quartets)

def getQuartets2mu2e(muons, electrons):
    nMuons = len(muons)
    nElectrons = len(electrons)
    if nMuons + nElectrons < 4:
        return []

    combMu = list(combinations(range(nMuons), 2))
    combEle = list(combinations(range(nElectrons), 2))
    quartets = product(combMu, combEle)
    return list(quartets) 


class triggerClass() :

    def __init__(self, triggerName) :
        self.triggerName = triggerName 
        self.triggerIndex = -1
        self.nError = 0 

    def getTriggerIndex(self, event, handleTrg) :
        names = event.object().triggerNames(handleTrg.product())
        for i in xrange(handleTrg.product().size()):
            #print("In getTriggerIndex name={0:s} listName[{1:d}]={2:s}".format(self.triggerName,i,names.triggerName(i)))
            if self.triggerName in names.triggerName(i) :
                self.triggerIndex = i
                #print("Found trigger index ={0:d} for {1:s}".format(i,self.triggerName)) 
                return True

        return False 

    def triggerFired(self, event, handleTrg) :
        if self.triggerIndex < 0 : 
            if not self.getTriggerIndex(event, handleTrg) :
                if self.nError < 100 :
                    self.nError += 1 
                    print("*** WARNING: Trigger index for {0:s} not found. ***".format(self.triggerName)) 
                    return False 
        else :
            #print("In triggerFired() triggerName={0:s} triggerIndex={1:d} status={2}".format(self.triggerName, self.triggerIndex, 
            #handleTrg.product().accept(self.triggerIndex)))
            return handleTrg.product().accept(self.triggerIndex)



