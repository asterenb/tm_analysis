#! /usr/bin/env python
import ROOT
import sys
from DataFormats.FWLite import Events, Handle
from array import array
import math
import numpy as np
from utils import *
import time 
import os 
import subprocess
import argparse

def getArgs() :
    parser = argparse.ArgumentParser()
    parser.add_argument("-v","--verbose",default=0,type=int,help="Print level.")
    parser.add_argument("-f","--inFileName",default='data1.root',help="File to be analyzed.")
    parser.add_argument("-l","--inListFileNames",help="List of files (one per line) to be analyzed.")
    parser.add_argument("--mc",action='store_true')
    parser.add_argument("-d", "--decayMode", dest='decayMode', default='four', choices=['two','four'], help="Decay mode (two- or four-mu)")
    parser.add_argument("--dontDelete",action='store_true')
    parser.add_argument("-n","--nEvents",default=0,type=int,help="Number of events to process.")
    parser.add_argument("-m","--maxPrint",default=20,type=int,help="Maximum number of events to print.")
    parser.add_argument("-s","--sequence",default=0,type=int,help="Sequence number")
    parser.add_argument("-t","--tag",default='AAAAAAAA',help="Tag")
        
    return parser.parse_args()

# begin execution here 

args = getArgs()
nPrint = args.maxPrint 
tag = args.tag
count = 0
dm = 4 if args.decayMode == 'four' else 2

# Events takes either
# - single file name
# - list of file names
# - VarParsing options
filenames = []
filename = 'multiple'
if args.inListFileNames is not None:
    with open(args.inListFileNames, "r") as f:
        # Read each line of the file
        for line in f.readlines():
            filenames += [line.split()[0]]
            print(line.split()[0])
else :
    filenames = [args.inFileName]  

print("filenames={0:s}".format(str(filenames)))

if '/store' in filenames[0] :
    os.system("rm data1.root")
    filename = '/store' + filenames[0].split('/store')[1]
    # check to see if file is there using fermilab redirector
    # if not use CERN global redirector 
    redirector = filenames[0].split('/store/')[0]
    path = '/store/' + filenames[0].split('/store/')[1]
    command = "xrdfs {0:s} locate -d -m {1:s}".format(redirector,path)
    try:
        endpoint = subprocess.check_output(command, shell=True).decode()
        print("***FNAL REDIRECTOR: {0:s}".format(filename))
    except subprocess.CalledProcessError as e:
        redirector = "root://cms-xrd-global.cern.ch/"
        path = '/store/' + filenames[0].split('/store/')[1]
        command = "xrdfs {0:s} locate -d -m {1:s}".format(redirector,path)
        try: 
            endpoint = subprocess.check_output(command, shell=True).decode()
            print("***CERN REDIRECTOR: {0:s}".format(filename))
        except subprocess.CalledProcessError as e:
            print("***FAILURE: {0:s}".format(filename))
            exit()

    os.system("xrdcp {0:s} data1.root".format(redirector+path))
    filenames[0] = 'data1.root'
else:
    filename = filenames[0]
    
print("***FILENAME: {0:s}".format(filename))

events = Events (filenames)
print("***OPENED: {0:s}".format(filename))

# create handle outside of loop
handleMu  = Handle ("std::vector<ScoutingMuon>")
labelMu = ("hltScoutingMuonPackerCalo")
handleTrg = Handle ("edm::TriggerResults")
labelTrg = ("TriggerResults", "", "HLT")

# instantiate a trigger object for each trigger on the trigger list
triggerList = [#'DST_DoubleMu1_noVtx_CaloScouting_v',
    'DST_DoubleMu3_noVtx_CaloScouting_Monitoring_v',
    'DST_DoubleMu3_noVtx_CaloScouting_v']
trigObject = {} 
for trigger in triggerList : trigObject[trigger] = triggerClass(trigger)

handleVtxDisp = Handle ("std::vector<ScoutingVertex>")
labelVtxDisp = ("hltScoutingMuonPackerCalo","displacedVtx", "HLT")
#labelVtxDisp = ("hltScoutingPrimaryVertexPackerCaloMuon","primaryVtx", "HLT")
if args.mc == True:
    handleGenPart = Handle ("vector<reco::GenParticle>")
    labelGenPart = ("genParticles")

ROOT.gROOT.SetBatch()        
ROOT.gROOT.SetStyle('Plain') 

# hM_mumuOS = ROOT.TH1F ("mumuOS", "mumuOS", 100, 0., cutoff)
# hM_mumuSS = ROOT.TH1F ("mumuSS", "mumuSS", 100, 0., cutoff)

hnMuon = ROOT.TH1D("nMuon","nMuon",20,0.,20.)
hnPosMuon = ROOT.TH1D("nPosMuon","nPosMuon",20,0.,20.)

outFilename = "{0:s}Muon_{1:s}_{2:03d}.root".format(args.decayMode, ("mc" if args.mc else "data"), args.sequence)
outputFile = ROOT.TFile(outFilename,"recreate")

t = ROOT.TTree('Events', '{0:s} muon mass'.format(args.decayMode))

# allow for an array of up to four muons
run, evt, nVtx, nMuons = array('I',[0]), array('I',[0]), array('I',[0]), array('I',[0]) 
nGood, trigFired, nCombo = array('I',[0]), array('I',[0]), array('I',[0])

# variables for the four- or two-muon system
inv_mass, inv_pt, inv_eta, inv_charge = array('f',[0.]), array('f',[0.]), array('f',[0.]), array('f',[0.])

# variables for the muon list
charge, pt, eta, phi = array('f', dm*[0.]), array('f', dm*[0.]), array('f', dm*[0.]), array('f', dm*[0.])
dxy, dz, iso = array('f', dm*[0]), array('f', dm*[0.]), array('f', dm*[0.])
vtxList = array('I', dm*[0])

#varibles for the vertex list 
vtx_x, vtx_y, vtx_z = array('f', dm*[0.]), array('f', dm*[0.]), array('f',dm*[0.])
vtx_chi2 = array('f', dm*[0.])

if args.mc:
    charge_gen, pt_gen, eta_gen, phi_gen = array('f', dm*[0]), array('f', dm*[0]), array('f', dm*[0]), array('f', dm*[0])
    mass_gen_eta, pt_gen_eta, eta_gen_eta, phi_gen_eta = array('f',[0]), array('f',[0]), array('f',[0]), array('f',[0])

t.Branch('run',	        run, 	'run/I')
t.Branch('evt',	        evt, 	'evt/I')

t.Branch('nVtx',    nVtx, 	'nVtx/I')
t.Branch('nMuons', nMuons, 'nMuons/I')
t.Branch('nGood',    nGood, 	'nGood/I')
t.Branch('trigFired', trigFired, 'trigFired/I')
t.Branch('nCombo', nCombo, 'nCombo/I')

t.Branch('inv_mass',	   inv_mass, 	'inv_mass/F')
t.Branch('inv_eta',	inv_eta, 'inv_eta/F')
t.Branch('inv_pt',	 inv_pt,  'inv_pt/F')
t.Branch('inv_charge', inv_charge, 'inv_charge/F')

t.Branch('charge', charge, 'charge[{0}]/F'.format(dm))
t.Branch('pt',         pt,     'pt[{0}]/F'.format(dm))
t.Branch('eta',       eta,    'eta[{0}]/F'.format(dm))
t.Branch('phi',       phi,    'phi[{0}]/F'.format(dm))
t.Branch('dxy',       dxy,    'dxy[{0}]/F'.format(dm))
t.Branch('dz',         dz,     'dz[{0}]/F'.format(dm))
t.Branch('iso',       iso,    'iso[{0}]/F'.format(dm))
t.Branch('vtxList', vtxList, 'vtxList[{0}]/I'.format(dm))

t.Branch('vtx_x',       vtx_x, 'vtx_x[{0}]/F'.format(dm))
t.Branch('vtx_y',       vtx_y, 'vtx_y[{0}]/F'.format(dm))
t.Branch('vtx_z',       vtx_z, 'vtx_z[{0}]/F'.format(dm))
t.Branch('vtx_chi2', vtx_chi2, 'vtx_chi2[{0}]/F'.format(dm))

if args.mc:
    t.Branch('charge_gen', charge_gen, 'charge_gen[{0}]/F'.format(dm))
    t.Branch('pt_gen',         pt_gen,     'pt_gen[{0}]/F'.format(dm))
    t.Branch('eta_gen',       eta_gen,    'eta_gen[{0}]/F'.format(dm))
    t.Branch('phi_gen',       phi_gen,    'phi_gen[{0}]/F'.format(dm))

    t.Branch('mass_gen_eta', mass_gen_eta, 'mass_gen_eta/F')
    t.Branch('pt_gen_eta',     pt_gen_eta,   'pt_gen_eta/F')
    t.Branch('eta_gen_eta',   eta_gen_eta,  'eta_gen_eta/F')
    t.Branch('phi_gen_eta',   phi_gen_eta,  'phi_gen_eta/F')


# loop over events
print("Entering event loop") 
tStart = time.time()
for kl, event in enumerate(events) :
    printStep = 100000 if not args.mc else 1000
    if kl % printStep == 0 : print("Reading event {0:d}".format(kl))
    aux = event.eventAuxiliary()
    run[0] = aux.run()
    evt[0] = aux.event()
            
    event.getByLabel (labelMu, handleMu)  
    muons = handleMu.product()

    numMuons = len(muons)
    nMuons[0] = numMuons
    hnMuon.Fill(numMuons)
    # if MC fill ntuple with dummy values 
    if args.mc :
        nVtx[0] = 0
        nGood[0] = 0
        trigFired[0] = 0
        nCombo[0] = 0
        inv_mass[0] = -99.
        inv_eta[0] = -99.
        inv_pt[0] = -99.
        inv_charge[0] = -99.
        for j in range(dm):
            charge[j] = -99.
            pt[j] = -99.
            eta[j] = -99.
            phi[j] = -99.
            dxy[j] = -99.
            dz[j] = -99.
            iso[j] = -99.
            vtxList[j] = 99
            vtx_x[j] = -99.
            vtx_y[j] = -99.
            vtx_z[j] = -99.
            vtx_chi2[j] = -99.

    
    if (not args.mc) and numMuons < dm:
        continue
    
    event.getByLabel(labelVtxDisp, handleVtxDisp)  
    try:
        vertices = handleVtxDisp.product()
    except:
        # print("WARNING: No good vertices in event {}".format(kl))
        vertices = []

    if args.mc:
        genEta = None
        event.getByLabel(labelGenPart, handleGenPart)
        particles = handleGenPart.product()
        count = 0
        genmuon = []
        for j in range(dm):
            genmuon.append(ROOT.TLorentzVector())
        for j in range(len(particles)):
            if abs(particles[j].pdgId()) == 13:
                charge_gen[count] = particles[j].charge()
                pt_gen[count] = particles[j].pt()
                eta_gen[count] = particles[j].eta()
                phi_gen[count] = particles[j].phi()
                genmuon[count].SetPtEtaPhiM(pt_gen[count], eta_gen[count], phi_gen[count], 0.1057)
                genEta = genEta + genmuon[count] if genEta is not None else genmuon[count]
                count += 1
                if count >= dm:
                    break 

        mass_gen_eta[0] = genEta.M()
        eta_gen_eta[0] = genEta.Eta()
        pt_gen_eta[0] = genEta.Pt()

    event.getByLabel(labelTrg, handleTrg)
    names = event.object().triggerNames(handleTrg.product())
    trigFired[0] = 0
    for trigger in triggerList:
        if trigObject[trigger].triggerFired(event,handleTrg):
            trigFired[0] = 1 
            break
    
    nPosMuon = 0 
    for j in range(numMuons):
        if muons[j].charge() > 0.:
            nPosMuon += 1
        if args.mc and j < dm:
            charge[j] = muons[j].charge()
            pt[j]  = muons[j].pt()
            eta[j] = muons[j].eta()
            phi[j] = muons[j].phi()
            dxy[j] = muons[j].dxy()
            dz[j]  = muons[j].dz()
            iso[j] = muons[j].trackIso()
            #muon[j].SetPtEtaPhiM(muons[j].pt(), muons[j].eta(), muons[j].phi(), 0.1057)
            vtxList[j] = 0
            for v in get_iVtx(run,muons,j):
                vtxList[j] += 2**v 

    if numMuons == dm:
        hnPosMuon.Fill(nPosMuon) 
    hnPosMuon.Fill(nPosMuon)
    
    quartets = getQuartets(muons, dm) 
    if len(quartets) == 0: 
        if args.mc:
            t.Fill() 
        else:
            continue 

    nCombo[0] = len(quartets)
    muon = []
    for j in range(dm):
        muon.append(ROOT.TLorentzVector())
    for quartet in quartets:
        etaMeson = None
        for j in range(min(numMuons, dm)):
            mj = quartet[j]
            charge[j] = muons[mj].charge()
            pt[j]  = muons[mj].pt()
            eta[j] = muons[mj].eta()
            phi[j] = muons[mj].phi()
            dxy[j] = muons[mj].dxy()
            dz[j]  = muons[mj].dz()
            iso[j] = muons[mj].trackIso()
            muon[j].SetPtEtaPhiM(muons[mj].pt(), muons[mj].eta(), muons[mj].phi(), 0.1057)
            etaMeson = etaMeson + muon[j] if etaMeson is not None else muon[j]
            vtxList[j] = 0
            for v in get_iVtx(run,muons,mj) : vtxList[j] += 2**v 
    
        invMass = etaMeson.M()
    
        inv_mass[0] = etaMeson.M()
        inv_eta[0] = etaMeson.Eta()
        inv_pt[0] = etaMeson.Pt()
        inv_charge[0] = sum([charge[j] for j in range(dm)])

        nVtx[0] = len(vertices)
        nGood[0] = nGoodVertex(muons, vertices, run[0], quartet)

        xOff, yOff = 0.086, -0.034
        for i in range(min(dm, len(vertices))) :
            vtx_x[i] = vertices[i].x()-xOff
            vtx_y[i] = vertices[i].y()-yOff
            vtx_z[i] = vertices[i].z()
            vtx_chi2[i] = vertices[i].chi2()

        if inv_mass[0] < 10. : 
            t.Fill()
        
    if len(quartets) > 0 :
        if nPrint > 0 and len(vertices) > 0 :
            nPrint -= 1
            print("\n\nM_4/2mu={0:.2f}".format(invMass))
            printEvent(muons,vertices,run[0],evt[0])

        if count < 1000 and invMass < 0.8 :
            count += 1
            dumpEvent(invMass,muons,vertices,tag,count,run[0],evt[0])

    if args.nEvents > 0 and kl > args.nEvents:
        break

if not args.dontDelete:
    os.system("rm data1.root")
print("***SUCCESS: {0:s}".format(filename))
print("Number of events = {0:d}.  Time per event = {1:.3f} ms".format(kl,1000.*(time.time()-tStart)/kl))
outputFile.cd()
t.Write()
hnMuon.Write()
hnPosMuon.Write() 
outputFile.Close()


