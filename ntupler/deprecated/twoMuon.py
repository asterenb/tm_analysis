#! /usr/bin/env python
import ROOT
import sys
from DataFormats.FWLite import Events, Handle
from array import array
import math
import numpy as np
from utils import *
import time

def getArgs() :
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("-v","--verbose",default=0,type=int,help="Print level.")
    parser.add_argument("-f","--inFileName",default='data1.root',help="File to be analyzed.")
    parser.add_argument("-l","--inListFileNames",help="List of files (one per line) to be analyzed.")
    parser.add_argument("--mc",action='store_true')
    parser.add_argument("-n","--nEvents",default=0,type=int,help="Number of events to process.")
    parser.add_argument("-m","--maxPrint",default=20,type=int,help="Maximum number of events to print.")
    parser.add_argument("-s","--sequence",default=0,type=int,help="Sequence number")
    parser.add_argument("-t","--tag",default='AAAAAAAA',help="Tag")
    
    return parser.parse_args()

# begin execution here 

args = getArgs()
nPrint = args.maxPrint 
tag = args.tag
count = 0

# Events takes either
# - single file name
# - list of file names
# - VarParsing options
filenames = []
if args.inListFileNames is not None:
    with open(args.inListFileNames, "r") as f:
        # Read each line of the file
        for line in f.readlines():
            filenames += [line.split()[0]]
            print(line.split()[0])
else:
    filenames = [args.inFileName]

events = Events (filenames) 

# create handle outside of loop
handleMu  = Handle ("std::vector<ScoutingMuon>")
labelMu = ("hltScoutingMuonPackerCalo")
handleTrg = Handle ("edm::TriggerResults")
labelTrg = ("TriggerResults", "", "HLT")


# instantiate a trigger object for each trigger on the trigger list
triggerList = [#'DST_DoubleMu1_noVtx_CaloScouting_v',
    'DST_DoubleMu3_noVtx_CaloScouting_Monitoring_v',
    'DST_DoubleMu3_noVtx_CaloScouting_v']
trigObject = {} 
for trigger in triggerList : trigObject[trigger] = triggerClass(trigger)

handleVtxDisp = Handle ("std::vector<ScoutingVertex>")
labelVtxDisp = ("hltScoutingMuonPackerCalo","displacedVtx", "HLT")
# labelVtxDisp = ("hltScoutingPrimaryVertexPackerCaloMuon","primaryVtx", "HLT")
if args.mc == True:
    handleGenPart = Handle ("vector<reco::GenParticle>")
    labelGenPart = ("genParticles")

ROOT.gROOT.SetBatch()        
ROOT.gROOT.SetStyle('Plain') 

# hM_mumuOS = ROOT.TH1F ("mumuOS", "mumuOS", 100, 0., cutoff)
# hM_mumuSS = ROOT.TH1F ("mumuSS", "mumuSS", 100, 0., cutoff)

hnMuon = ROOT.TH1D("nMuon","nMuon",20,0.,20.)
hnPosMuon = ROOT.TH1D("nPosMuon","nPosMuon",20,0.,20.)

outputFile=ROOT.TFile("twoMuon_{0:s}_{1:03d}.root".format(("mc" if args.mc else "data"), args.sequence),"recreate")

t = ROOT.TTree('Events','Two muon mass')
if args.mc == True:
    genT = ROOT.TTree('GenEvents', 'Generated two muons')

run = array('I',[0]) 
evt = array('I',[0])

mass = array('f',[0])
eta = array('f',[0])
p_t = array('f',[0])
p = array('f',[0])
dxy = array('f',[0])
dz  = array('f',[0])
nVtx  = array('f',[0])
nMuons = array('I', [0])
nGood = array('I',[0])
nGoodPairings = array('I',[0])
nBadPairings = array('I',[0])
nMuonsInPairings = array('I',[0])
trigFired = array('I', [0])

charge_1 = array('f',[0])
pt_1     = array('f',[0])
p_1     = array('f',[0])
eta_1    = array('f',[0])
phi_1    = array('f',[0])
dxy_1    = array('f',[0])
dz_1     = array('f',[0])
iso_1    = array('f',[0])

charge_2 = array('f',[0])
pt_2     = array('f',[0])
p_2     = array('f',[0])
eta_2    = array('f',[0])
phi_2    = array('f',[0])
dxy_2    = array('f',[0])
dz_2     = array('f',[0])
iso_2    = array('f',[0])

if args.mc == True:
    charge_gen_1 = array('f',[0])
    pt_gen_1     = array('f',[0])
    p_gen_1     = array('f',[0])
    eta_gen_1    = array('f',[0])
    phi_gen_1    = array('f',[0])

    charge_gen_2 = array('f',[0])
    pt_gen_2     = array('f',[0])
    p_gen_2     = array('f',[0])
    eta_gen_2    = array('f',[0])
    phi_gen_2    = array('f',[0])

    mass_gen_eta = array('f',[0])
    pt_gen_eta     = array('f',[0])
    p_gen_eta     = array('f',[0])
    eta_gen_eta    = array('f',[0])
    phi_gen_eta    = array('f',[0])

t.Branch('run',	        run, 	'run/I')
t.Branch('evt',	        evt, 	'evt/I')
t.Branch('mass',	mass, 	'mass/F')
t.Branch('eta',	eta, 	'eta/F')
t.Branch('p_t',	p_t, 	'p_t/F')
t.Branch('p',	p, 	'p/F')
t.Branch('nVtx',    nVtx, 	'nVtx/F')
t.Branch('nMuons', nMuons, 'nMuons/I')
t.Branch('nGood',    nGood, 	'nGood/I')
t.Branch('nGoodPairings', nGoodPairings, 'nGoodPairings/I')
t.Branch('nBadPairings', nBadPairings, 'nBadPairings/I')
t.Branch('nMuonsInPairings', nMuonsInPairings, 'nMuonsInPairings/I')
t.Branch('trigFired', trigFired, 'trigFired/I')

t.Branch('charge_1',  charge_1, 'charge_1/F')
t.Branch('pt_1',      pt_1,     'pt_1/F')
t.Branch('p_1',      p_1,     'p_1/F')
t.Branch('eta_1',     eta_1,    'eta_1/F')
t.Branch('phi_1',     phi_1,    'phi_1/F')
t.Branch('dxy_1',     dxy_1,    'dxy_1/F')
t.Branch('dz_1',      dz_1,     'dz_1/F')
t.Branch('iso_1',     iso_1,    'iso_1/F')

t.Branch('charge_2',  charge_2, 'charge_2/F')
t.Branch('pt_2',      pt_2,     'pt_2/F')
t.Branch('p_2',      p_2,     'p_2/F')
t.Branch('eta_2',     eta_2,    'eta_2/F')
t.Branch('phi_2',     phi_2,    'phi_2/F')
t.Branch('dxy_2',     dxy_2,    'dxy_2/F')
t.Branch('dz_2',      dz_2,     'dz_2/F')
t.Branch('iso_2',     iso_2,    'iso_2/F')

if args.mc == True:
    genT.Branch('charge_gen_1',  charge_gen_1, 'charge_gen_1/F')
    genT.Branch('pt_gen_1',      pt_gen_1,     'pt_gen_1/F')
    genT.Branch('p_gen_1',      p_gen_1,     'p_gen_1/F')
    genT.Branch('eta_gen_1',     eta_gen_1,    'eta_gen_1/F')
    genT.Branch('phi_gen_1',     phi_gen_1,    'phi_gen_1/F')

    genT.Branch('charge_gen_2',  charge_gen_2, 'charge_gen_2/F')
    genT.Branch('pt_gen_2',      pt_gen_2,     'pt_gen_2/F')
    genT.Branch('p_gen_2',      p_gen_2,     'p_gen_2/F')
    genT.Branch('eta_gen_2',     eta_gen_2,    'eta_gen_2/F')
    genT.Branch('phi_gen_2',     phi_gen_2,    'phi_gen_2/F')

    genT.Branch('mass_gen_eta',      mass_gen_eta,     'mass_gen_eta/F')
    genT.Branch('pt_gen_eta',      pt_gen_eta,     'pt_gen_eta/F')
    genT.Branch('p_gen_eta',      p_gen_eta,     'p_gen_eta/F')
    genT.Branch('eta_gen_eta',     eta_gen_eta,    'eta_gen_eta/F')
    genT.Branch('phi_gen_eta',     phi_gen_eta,    'phi_gen_eta/F')



# loop over events
print("Entering event loop") 
tStart = time.time() 
for kl, event in enumerate(events) :
    stepSize = 100000 if args.mc == False else 1000
    if kl % stepSize == 0 : print("Reading event {0:d}".format(kl))
    aux = event.eventAuxiliary()
    run[0] = aux.run()
    evt[0] = aux.event()
            
    event.getByLabel (labelMu, handleMu)  
    muons = handleMu.product()

    event.getByLabel (labelVtxDisp, handleVtxDisp)  
    try:
        vertices = handleVtxDisp.product()
    except:
        # print("WARNING: No good vertices in event {}".format(kl))
        vertices = []

    if args.mc == True:
        event.getByLabel (labelGenPart, handleGenPart)
        particles = handleGenPart.product()

    

    numMuons = len (muons)
    nMuons[0] = numMuons
    hnMuon.Fill(numMuons)

    # prefilter for data 
    if not args.mc :
        if numMuons < 2 : continue
        muon1 = ROOT.TLorentzVector ()
        muon2 = ROOT.TLorentzVector ()  
        if muons[0].charge()*muons[1].charge() > 0 : continue 
        muon1.SetPtEtaPhiM(muons[0].pt(), muons[0].eta(), muons[0].phi(), 0.1057)
        muon2.SetPtEtaPhiM(muons[1].pt(), muons[1].eta(), muons[1].phi(), 0.1057)
        invMass = (muon1 + muon2).M()
        if invMass < 0.5 or invMass > 0.6 : continue

    event.getByLabel(labelTrg, handleTrg)
    names = event.object().triggerNames(handleTrg.product())
    trigFired[0] = 0
    for trigger in triggerList :
        if trigObject[trigger].triggerFired(event,handleTrg) :
            trigFired[0] = 1 
            break
    #print("trigFired={0:d}".format(trigFired[0]))

    nPosMuon = 0 
    for j in range(numMuons) :
        if muons[j].charge() > 0. : nPosMuon += 1
    # if numMuons == 2 : hnPosMuon.Fill(nPosMuon) 
    # if nPosMuon != 1 : continue 
    hnPosMuon.Fill(nPosMuon)
    
    if numMuons > 0:
        charge_1[0] = muons[0].charge()
        pt_1[0]  = muons[0].pt()
        eta_1[0] = muons[0].eta()
        phi_1[0] = muons[0].phi()
        temp_muon = ROOT.TLorentzVector()
        temp_muon.SetPtEtaPhiM(pt_1[0], eta_1[0], phi_1[0], 0.1057)
        p_1[0] = temp_muon.P()
        dxy_1[0] = muons[0].dxy()
        dz_1[0]  = muons[0].dz()
        iso_1[0] = muons[0].trackIso()

    if numMuons > 1:
        charge_2[0] = muons[1].charge()
        pt_2[0]  = muons[1].pt()
        eta_2[0] = muons[1].eta()
        phi_2[0] = muons[1].phi()
        temp_muon = ROOT.TLorentzVector()
        temp_muon.SetPtEtaPhiM(pt_2[0], eta_2[0], phi_2[0], 0.1057)
        p_2[0] = temp_muon.P()
        dxy_2[0] = muons[1].dxy()
        dz_2[0]  = muons[1].dz()
        iso_2[0] = muons[1].trackIso()
    
    muon1 = ROOT.TLorentzVector ()
    muon2 = ROOT.TLorentzVector ()
    invMass = 1000.0

    if numMuons == 2:
        muon1.SetPtEtaPhiM(muons[0].pt(), muons[0].eta(), muons[0].phi(), 0.1057)
        muon2.SetPtEtaPhiM(muons[1].pt(), muons[1].eta(), muons[1].phi(), 0.1057)

        diMuon = muon1 + muon2 
        invMass = diMuon.M()
        
        mass[0] = invMass
        eta[0] = diMuon.Eta()
        p_t[0] = diMuon.Pt()
        p[0] = diMuon.P()

    nVtx[0] = float(len(vertices))
    nGood[0] = nGoodVertex(muons, vertices, run[0])
    nGoodPairings[0], nBadPairings[0] = analyzePairing(muons, vertices, run[0])
    nMuonsInPairings[0] = analyzeMuonsInPairings(muons, vertices, run[0])

    t.Fill()

    if args.mc == True:
        count = 0
        for j in range(len(particles)):
            if abs(particles[j].pdgId()) == 13:
                if count == 0:
                    charge_gen_1[0] = particles[j].charge()
                    pt_gen_1[0] = particles[j].pt()
                    p_gen_1[0] = particles[j].p()
                    eta_gen_1[0] = particles[j].eta()
                    phi_gen_1[0] = particles[j].phi()
                elif count == 1:
                    charge_gen_2[0] = particles[j].charge()
                    pt_gen_2[0] = particles[j].pt()
                    p_gen_2[0] = particles[j].p()
                    eta_gen_2[0] = particles[j].eta()
                    phi_gen_2[0] = particles[j].phi()
                else:
                    break
                count += 1

        genmuon1 = ROOT.TLorentzVector ()
        genmuon2 = ROOT.TLorentzVector ()

        genmuon1.SetPtEtaPhiM(pt_gen_1[0], eta_gen_1[0], phi_gen_1[0], 0.1057)
        genmuon2.SetPtEtaPhiM(pt_gen_2[0], eta_gen_2[0], phi_gen_2[0], 0.1057)

        genDiMuon = genmuon1 + genmuon2 
        genInvMass = genDiMuon.M()
        
        mass_gen_eta[0] = genInvMass
        eta_gen_eta[0] = genDiMuon.Eta()
        pt_gen_eta[0] = genDiMuon.Pt()
        p_gen_eta[0] = genDiMuon.P()

        genT.Fill()

    if nPrint > 0 and invMass < 20.0 and len(vertices) > 0 :
        nPrint -= 1
        print("\n\nM_2mu={0:.2f}".format(invMass))
        printEvent(muons,vertices,run[0],evt[0])

    if count < 0 and (invMass < 0.8 or invMass < 0.8) :

        count += 1
        dumpEvent(invMass,muons,vertices,tag,count,run[0],evt[0])
        
    if args.nEvents > 0 and kl > args.nEvents : break

print("Number of events={0:d} \nTime per event = {1:.3f} ms.".format(kl-1,1000.*(time.time()-tStart)/(kl-1)))
outputFile.cd()
t.Write()
if args.mc == True:
    genT.Write()
hnMuon.Write()
hnPosMuon.Write() 
outputFile.Close()


