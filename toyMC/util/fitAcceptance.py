from ROOT import TFile, TF1

f = TFile.Open("acceptance_EtaTo4mu.root")
g = f.Get("acceptance_EtaTo4Mu")
accFit = f.Get("accFit")

g.Fit("accFit",'R')

g.Draw("A*")

raw_input()
