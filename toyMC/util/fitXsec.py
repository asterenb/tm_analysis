from ROOT import TFile, TF1, TH1D, gPad, TCanvas

f = TFile.Open("corrected_xsec.root")
h = f.Get("h_corrXsec")


# get the sum of the cross section values above 5 GeV

nBins = h.GetNbinsX()

sum, sum16 = 0., 0.
for i in range(1,nBins):
    pt = h.GetBinCenter(i)
    if pt < 6. : continue
    val = h.GetBinContent(i)
    sum += val
    if pt > 16. : 
        sum16 += val
    #print("i={0:d} pt={1:8.3f} contents={2:e} sum={3:e}".format(i,pt,h.GetBinContent(i),sum))

print("Sum for pt>6.={0:e}; for pt>16.={1:e}".format(sum,sum16))

if True :
    c1 = TCanvas("c1","Corrected Cross Section",750,600)
    c1.cd(1) 
    gPad.SetLogy()
    f1 = TF1("f1","[0]*exp(-x/[1])",5.,16.)
    f1.SetParameter(0,1.0e11)
    f1.SetParameter(1,6.)
    f2 = TF1("f2","[0]*exp(-x/[1])",16.,100.)
    f2.SetParameter(0,1.0e11)
    f2.SetParameter(1,6.)
    h.GetXaxis().SetTitle("p_t (GeV)")
    h.GetYaxis().SetTitle("dsigma/dpt (fb/GeV)")
    h.Fit("f1","R")
    h.Draw()
    raw_input()
    h.Fit("f2","R")
    h.Draw("SAME")
    c1.Update() 
    raw_input()

    
