# simulate eta -> mu+mu-gamma events, followed by gamma -> mu+mu- conversion

from ROOT import TLorentzVector, TH1D, TH2D, TFile 
from math import sqrt, cos, sin, pi
import random
import photConFun

# calculate outgoing momentum in CM for M -> m1 + m2
def twoBody(M,m1,m2) :
    Msq, mp, mm = M*M, (m1+m2), (m1-m2)
    return sqrt((Msq - mp*mp)*(Msq - mm*mm))/(2.*M)

def getPxPyPz(p) :
    cs = random.uniform(-1.,1.)
    sn = sqrt(1. - cs*cs)
    phi = random.uniform(0.,2*pi)
    return p*sn*cos(phi), p*sn*sin(phi), p*cs

pEta = 20.
hMass1 = TH1D("hMass1","hMass1",100,0.4,0.6)
hMass1.GetXaxis().SetTitle("mu mu gamma mass (GeV)")
hMass2 = TH1D("hMass2","hMass2",100,0.,.2*pEta)
hMass2.GetXaxis().SetTitle("M4mu mass (GeV)")
hGamma = TH1D("hGamma","hGamma",100,0.,pEta)
hm12 = TH1D("hm12","hm12",100,0.,0.6)
hm12.GetXaxis().SetTitle("m12 mass (GeV)")
hm34 = TH1D("hm34","hm34",100,0.,2.)
hm34.GetXaxis().SetTitle("m34 mass (GeV)")
hM4muVsEgamma = TH2D('M4muVsEgamma','M4muVsEgamma',100,0.,pEta,100,0.,0.2*pEta) 
hM4muVsEgamma.GetXaxis().SetTitle("E gamma (GeV)")
hM4muVsEgamma.GetYaxis().SetTitle("M4mu (GeV)")
hM4muVsM12 = TH2D('M4muVsM12','M4muVsM12',100,0.,0.6,100,0.,0.2*pEta) 
hM4muVsM12.GetXaxis().SetTitle("m12 (GeV)")
hM4muVsM12.GetYaxis().SetTitle("M4mu (GeV)")
hM4muVsM34 = TH2D('M4muVsM34','M4muVsM34',100,0.,2.0,100,0.,0.2*pEta) 
hM4muVsM34.GetXaxis().SetTitle("m34 (GeV)")
hM4muVsM34.GetYaxis().SetTitle("M4mu (GeV)")
hM4muVsM1234 = TH2D('M4muVsM1234','M4muVsM1234',100,0.,2.0,100,0.,0.2*pEta) 
hM4muVsM1234.GetXaxis().SetTitle("m1234 (GeV)")
hM4muVsM1234.GetYaxis().SetTitle("M4mu (GeV)")


nEvents, nPrint = 10000, 10 
Meta, Mmu = 0.5479, 0.1057
 
for i in range(nEvents) :
    # get properties of 2Mu system in eta rest frame
    M2mu = random.uniform(2.*Mmu,Meta)
    p = twoBody(Meta,M2mu,0.)
    px, py, pz = getPxPyPz(p)
    E2Mu = sqrt(M2mu*M2mu + p*p)
    p2Mu_4 = TLorentzVector(px, py, pz, E2Mu)
    pGamma = TLorentzVector(-px, -py, -pz, p)
    
    # get properties of muons in 2M system rest frame
    p = twoBody(M2mu,Mmu,Mmu)
    px, py, pz = getPxPyPz(p)
    EMu = sqrt(Mmu*Mmu + p*p)
    muon1 = TLorentzVector(px, py, pz, EMu)
    muon2 = TLorentzVector(-px, -py, -pz, EMu)
    
    # Boost the muons to the eta rest frame
    beta = p2Mu_4.BoostVector()
    muon1.Boost(beta) 
    muon2.Boost(beta) 

    # eta momentum in lab frame
    Eeta = sqrt(Meta*Meta + pEta*pEta)
    pEta_4 = TLorentzVector(0.,0.,pEta,Eeta)

    # boost all three particles to the lab
    beta = pEta_4.BoostVector()
    muon1.Boost(beta)
    muon2.Boost(beta)
    pGamma.Boost(beta)
    hGamma.Fill(pGamma.E())
    if pGamma.E() < 4.*Mmu : 
        #print("Event rejected because gamma energy is too low i={0:d} E_gamma={1:f}".format(i,pGamma.E())) 
        continue 

    mass1 = (muon1+muon2+pGamma).M()
    hMass1.Fill(mass1)

    muon3, muon4 = photConFun.photConFun(pGamma,"Si")
    mass2 = (muon1 + muon2 + muon3 + muon4).M() 
    hMass2.Fill(mass2)
    hm12.Fill((muon1 + muon2).M())
    hm34.Fill((muon3 + muon4).M())
    hM4muVsEgamma.Fill(pGamma.E(),mass2)
    hM4muVsM12.Fill((muon1 + muon2).M(),mass2)
    hM4muVsM34.Fill((muon3 + muon4).M(),mass2)
    hM4muVsM1234.Fill((muon1 + muon2).M()+(muon3 + muon4).M(),mass2)
    if nPrint > 0 :
        nPrint -= 1
        print("\n           px      py      pz      ")
        print("  Mu+ : {0:8.3f} {1:8.3f} {2:8.3f}".format(muon1.Px(), muon1.Py(), muon1.Pz()))
        print("  Mu- : {0:8.3f} {1:8.3f} {2:8.3f}".format(muon2.Px(), muon2.Py(), muon2.Pz()))
        print("Gamma : {0:8.3f} {1:8.3f} {2:8.3f}".format(pGamma.Px(), pGamma.Py(), pGamma.Pz()))
        print("Muon3 : {0:8.3f} {1:8.3f} {2:8.3f}".format(muon3.Px(), muon3.Py(), muon3.Pz()))
        print("Muon4 : {0:8.3f} {1:8.3f} {2:8.3f}".format(muon4.Px(), muon4.Py(), muon4.Pz()))
        print("Mass1={0:8.3f} Mass2={1:8.3f}".format(mass1, mass2))

outputFile = TFile("mumuGamma.root","recreate")
outputFile.cd()
hMass1.Write() 
hMass2.Write() 
hGamma.Write() 
hm12.Write()
hm34.Write()
hM4muVsM12.Write()
hM4muVsM34.Write()
hM4muVsM1234.Write()
hM4muVsEgamma.Write() 
outputFile.Close()

