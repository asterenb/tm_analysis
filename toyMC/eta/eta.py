# simulate various eta decays  

from ROOT import TLorentzVector, TH1D, TH2D, TFile 
from math import sqrt, cos, sin, pi, exp
import random
import photConFun
import sys

mode = '4mu'
if len(sys.argv) > 1 : mode = sys.argv[1]

# calculate outgoing momentum in CM for M -> m1 + m2
def twoBody(M,m1,m2) :
    Msq, mp, mm = M*M, (m1+m2), (m1-m2)
    return sqrt((Msq - mp*mp)*(Msq - mm*mm))/(2.*M)

def getPxPyPz(p) :
    cs = random.uniform(-1.,1.)
    sn = sqrt(1. - cs*cs)
    phi = random.uniform(0.,2*pi)
    return p*sn*cos(phi), p*sn*sin(phi), p*cs

# get a four vector for the eta and an associated weight
def getEta() :
    val = random.random()
    # generate pt according to a logarithmic distribution with two slopes 
    if val > 0.022 : 
        pt = 9999.
        while pt > 16. :
            pt = 6. + random.expovariate(1./2.27)
    else :
        pt = 16. + random.expovariate(1./5.08)
    weight = 0.162/(1. + exp(-0.337*(pt-25.1)))
    pEta = TLorentzVector()
    pEta.SetPtEtaPhiM(pt,random.uniform(-2.3,2.3),random.uniform(0.,2.*pi),Meta)
    return pEta, weight

pIDsq = 0.0015*0.0015

# branching ratios and misID rates 
BR = {'4mu':4.0e-9, 'gammagamma':0.3941, 'pipipi0':0.2292, '3pi0':0.327, 'pipigamma':0.0422, 'pipimumu':3.6e-4,'mumugamma':3.1e-4 }
pMuonMisID = {'4mu':1.0, 'gammagamma':1., 'pipipi0':pIDsq, '3pi0':1., 'pipigamma':pIDsq, 'pipimumu':pIDsq,'mumugamma':1.}

hMass1 = TH1D("hMass1","hMass1",100,0.4,0.6)
hMass1.GetXaxis().SetTitle("mu mu pi pi (GeV)")
hMass2 = TH1D("hMass2","hMass2",120,0.4,1.0)
hMass2.GetXaxis().SetTitle("M4mu (GeV)")
hWeight = TH1D("hWeight","hWeight",100,0.,25.)
hWeight.GetXaxis().SetTitle("Weight x 10**8")
hEgamma = TH1D("hEgamma","hEgamma",100,0.,20.)
hEgamma.GetXaxis().SetTitle("E gamma (GeV)")
hPt = TH1D("hPt","hPt",100,0.,100.)
hPt.GetXaxis().SetTitle("pt of eta (GeV)")
hMass2VsPt = TH2D("hMassVsPt","hMassVsPt",100,0.,100.,100,0.,1.)
hMass2VsPt.GetXaxis().SetTitle("pt of eta (GeV)")
hMass2VsPt.GetYaxis().SetTitle("Mass 2 (GeV)")

nEvents, nPrint = 1000000, 10 
Meta, Mmu, Mpi, Mpi0 = 0.5479, 0.1057, 0.1395, 0.1349
totalWeight, nEvent = 0., 0 

xSec = 9.89e10    # total eta cross section above 6 GeV in fb
lumi = 101.       # total lumi in fb-1 
nEta = xSec*lumi  # number of etas
weight0 = pMuonMisID[mode]*nEta*BR[mode]/nEvents
photConWeight = 1.0  
for i in range(nEvents) :
    if i % 10000 == 0 : print("i={0:d}".format(i))
    # get a four vector for the eta and an associated weight    
    pEta, etaWeight = getEta() 
    
    if mode == '4mu' :
        # get properties of the mu-mu systems in eta rest frame
        M2mu1 = random.uniform(2.*Mmu,Meta-2.*Mmu)
        M2mu2 = random.uniform(2.*Mmu,Meta-M2mu1)
        p = twoBody(Meta,M2mu1,M2mu2)
        px, py, pz = getPxPyPz(p)
        E2mu1 = sqrt(M2mu1*M2mu1 + p*p)
        E2mu2 = sqrt(M2mu2*M2mu2 + p*p)
        p2mu1 = TLorentzVector(px, py, pz, E2mu1)
        p2mu2 = TLorentzVector(-px, -py, -pz, E2mu2) 

        # get properties of first mu pair in M2mu1 rest frame 
        p = twoBody(M2mu1,Mmu,Mmu)
        px, py, pz = getPxPyPz(p)
        Emu = sqrt(Mmu*Mmu + p*p)
        muon1 = TLorentzVector(px, py, pz, Emu)
        muon2 = TLorentzVector(-px, -py, -pz, Emu)

        # get properties of second mu pair in M2mu2 rest frame 
        p = twoBody(M2mu2,Mmu,Mmu)
        px, py, pz = getPxPyPz(p)
        Emu = sqrt(Mmu*Mmu + p*p)
        muon3 = TLorentzVector(px, py, pz, Emu)
        muon4 = TLorentzVector(-px, -py, -pz, Emu)

        # Boost the muons to the eta rest frame
        beta = p2mu1.BoostVector()
        muon1.Boost(beta) 
        muon2.Boost(beta) 
        beta = p2mu2.BoostVector()
        muon3.Boost(beta) 
        muon4.Boost(beta) 
   
        # boost all four particles to the lab
        beta = pEta.BoostVector()
        muon1.Boost(beta)
        muon2.Boost(beta)
        muon3.Boost(beta)
        muon4.Boost(beta)
        mass1 = (muon1 + muon2 + muon3 + muon4).M() 

    elif mode == 'gammagamma' :
        # get properties of gammas in eta rest frame
        p = 0.5*Meta
        px, py, pz = getPxPyPz(p)
        gamma1 = TLorentzVector(px, py, pz, p)
        gamma2 = TLorentzVector(-px, -py, -pz, p)

        # boost the gammas to the lab
        beta = pEta.BoostVector()
        gamma1.Boost(beta)
        gamma2.Boost(beta)
        if gamma1.E() < 4.*Mmu or gamma2.E() < 4.*Mmu : continue 

        mass1 = (gamma1 + gamma2).M()
        hMass1.Fill(mass1)

        muon1, muon2 = photConFun.photConFun(gamma1,"Si")
        photConWeight = photConFun.conversionProbability(gamma1,"Si")
        muon3, muon4 = photConFun.photConFun(gamma2,"Si")
        photConWeight *= photConFun.conversionProbability(gamma2,"Si")

    elif mode == 'pipipi0' :
        # get properties of pi+pi- system in eta rest frame
        M2pi = random.uniform(2.*Mpi,Meta-Mpi0)

        p = twoBody(Meta,M2pi,Mpi0)
        px, py, pz = getPxPyPz(p)
        E2pi = sqrt(M2pi*M2pi + p*p)
        Epi0 = sqrt(Mpi0*Mpi0 + p*p)
        p2pi = TLorentzVector(px, py, pz, E2pi)
        pPi0 = TLorentzVector(-px, -py, -pz, Epi0) 

        # get properties of pions in 2Pi system rest frame
        p = twoBody(M2pi,Mpi,Mpi)
        px, py, pz = getPxPyPz(p)
        Epi = sqrt(Mpi*Mpi + p*p)
        pion1 = TLorentzVector(px, py, pz, Epi)
        pion2 = TLorentzVector(-px, -py, -pz, Epi)

        # Boost the pions to the eta rest frame
        beta = p2pi.BoostVector()
        pion1.Boost(beta) 
        pion2.Boost(beta) 

        # gammas in the pi0 rest frame
        p =  0.5*Mpi0 
        px, py, pz = getPxPyPz(p)
        gamma1 = TLorentzVector(px, py, pz, p)
        gamma2 = TLorentzVector(-px, -py, -pz, p) 

       # boost the gammas to the eta rest frame
        beta = pPi0.BoostVector()
        gamma1.Boost(beta) 
        gamma2.Boost(beta) 

        # boost all three particles to the lab
        beta = pEta.BoostVector()
        pion1.Boost(beta)
        pion2.Boost(beta)
        gamma1.Boost(beta)
        gamma2.Boost(beta)
   
       # for mass1, we assign correct particle ID
        mass1 = (pion1+pion2+gamma1+gamma2).M()

        # for mass2, we assume that both pions are misidentified as muons
        # and also one of the gammas converts to a mu pair 
        muon1 = TLorentzVector(pion1.Px(),pion1.Py(),pion1.Pz(),sqrt(pion1.E()**2 - Mpi**2 + Mmu**2))
        muon2 = TLorentzVector(pion2.Px(),pion2.Py(),pion2.Pz(),sqrt(pion2.E()**2 - Mpi**2 + Mmu**2))
        if gamma1.E() < 4.*Mmu : continue   
        muon3, muon4 = photConFun.photConFun(gamma1,"Si")
        # we add a factor of two, since either photon can convert
        photConWeight = 2.*photConFun.conversionProbability(gamma1,"Si")

    elif mode == '3pi0' :
        # create a list of gammas
        gammas = []

        # get properties of pi+pi- system in eta rest frame
        M2pi = random.uniform(2.*Mpi0,Meta-Mpi0)

        p = twoBody(Meta,M2pi,Mpi0)
        px, py, pz = getPxPyPz(p)
        E2pi = sqrt(M2pi*M2pi + p*p)
        Epi0 = sqrt(Mpi0*Mpi0 + p*p)
        p2pi = TLorentzVector(px, py, pz, E2pi)
        pi0_3 = TLorentzVector(-px, -py, -pz, Epi0) 

        # get properties of pions in 2Pi system rest frame
        p = twoBody(M2pi,Mpi0,Mpi0)
        px, py, pz = getPxPyPz(p)
        Epi = sqrt(Mpi0*Mpi0 + p*p)
        pi0_1 = TLorentzVector(px, py, pz, Epi)
        pi0_2 = TLorentzVector(-px, -py, -pz, Epi)

        # Boost the pions to the eta rest frame
        beta = p2pi.BoostVector()
        pi0_1.Boost(beta) 
        pi0_2.Boost(beta) 

        # gammas in the pi0_1 rest frame
        p =  0.5*Mpi0 
        px, py, pz = getPxPyPz(p)
        gammas.append(TLorentzVector(px, py, pz, p))
        gammas.append(TLorentzVector(-px, -py, -pz, p))

        # boost the gammas to the eta rest frame
        beta = pi0_1.BoostVector()
        gammas[0].Boost(beta) 
        gammas[1].Boost(beta) 

        # gammas in the pi0_2 rest frame
        px, py, pz = getPxPyPz(p)
        gammas.append(TLorentzVector(px, py, pz, p))
        gammas.append(TLorentzVector(-px, -py, -pz, p))

        # boost the gammas to the eta rest frame
        beta = pi0_2.BoostVector()
        gammas[2].Boost(beta) 
        gammas[3].Boost(beta)

        # gammas in the pi0_3 rest frame 
        px, py, pz = getPxPyPz(p)
        gammas.append(TLorentzVector(px, py, pz, p))
        gammas.append(TLorentzVector(-px, -py, -pz, p))

        # boost the gammas to the eta rest frame
        beta = pi0_3.BoostVector()
        gammas[4].Boost(beta) 
        gammas[5].Boost(beta)

        # boost all six gammas to the lab
        beta = pEta.BoostVector()
        for i in range(6) : 
            gammas[i].Boost(beta)

        # for mass1, we assign correct particle ID

        mass1 = (gammas[0]+gammas[1]+gammas[2]+gammas[3]+gammas[4]+gammas[5]).M()

        # for mass2, we assume that two of the gammas convert to mu pairs 

        j1, j2 = random.sample(range(6),2) 
        if gammas[j1].E() < 4.*Mmu or gammas[j2].E() < 4.*Mmu : continue 
        muon1, muon2 = photConFun.photConFun(gammas[j1],"Si")
        muon3, muon4 = photConFun.photConFun(gammas[j2],"Si")
        photConWeight = photConFun.conversionProbability(gammas[j1],"Si")
        photConWeight *= photConFun.conversionProbability(gammas[j2],"Si")
        # we add a factor of 15 to take into account that there are 6 choose 2 combinations
        photConWeight *= 15. 

    elif mode == 'pipigamma' :
        # get properties of pi+pi- system in eta rest frame
        M2pi = random.uniform(2.*Mpi,Meta)

        p = twoBody(Meta,M2pi,0.)
        px, py, pz = getPxPyPz(p)
        E2pi = sqrt(M2pi*M2pi + p*p)
        p2pi = TLorentzVector(px, py, pz, E2pi)
        gamma = TLorentzVector(-px, -py, -pz, p) 

        # get properties of pions in 2Pi system rest frame
        p = twoBody(M2pi,Mpi,Mpi)
        px, py, pz = getPxPyPz(p)
        Epi = sqrt(Mpi*Mpi + p*p)
        pion1 = TLorentzVector(px, py, pz, Epi)
        pion2 = TLorentzVector(-px, -py, -pz, Epi)

        # Boost the pions to the eta rest frame
        beta = p2pi.BoostVector()
        pion1.Boost(beta) 
        pion2.Boost(beta) 
   
        # boost all three particles to the lab
        beta = pEta.BoostVector()
        pion1.Boost(beta)
        pion2.Boost(beta)
        gamma.Boost(beta)
   
        # for mass1, we assign correct particle ID
        mass1 = (pion1+pion2+gamma).M()

        # for mass2, we assume that both pions are misidentified as muons
        # and the gamma converts to a mu pair 
        muon1 = TLorentzVector(pion1.Px(),pion1.Py(),pion1.Pz(),sqrt(pion1.E()**2 - Mpi**2 + Mmu**2))
        muon2 = TLorentzVector(pion2.Px(),pion2.Py(),pion2.Pz(),sqrt(pion2.E()**2 - Mpi**2 + Mmu**2))
        if gamma.E() < 4.*Mmu : continue    
        muon3, muon4 = photConFun.photConFun(gamma,"Si")
        photConWeight = photConFun.conversionProbability(gamma,"Si")
    
    elif mode == 'mumugamma' :
        # get properties of 2Mu system in eta rest frame
        M2mu = random.uniform(2.*Mmu,Meta)
        p = twoBody(Meta,M2mu,0.)
        px, py, pz = getPxPyPz(p)
        E2Mu = sqrt(M2mu*M2mu + p*p)
        p2Mu_4 = TLorentzVector(px, py, pz, E2Mu)
        pGamma = TLorentzVector(-px, -py, -pz, p)
    
        # get properties of muons in 2M system rest frame
        p = twoBody(M2mu,Mmu,Mmu)
        px, py, pz = getPxPyPz(p)
        EMu = sqrt(Mmu*Mmu + p*p)
        muon1 = TLorentzVector(px, py, pz, EMu)
        muon2 = TLorentzVector(-px, -py, -pz, EMu)
    
        # Boost the muons to the eta rest frame
        beta = p2Mu_4.BoostVector()
        muon1.Boost(beta) 
        muon2.Boost(beta)  

        # boost all three particles to the lab
        beta = pEta.BoostVector()
        muon1.Boost(beta)
        muon2.Boost(beta)
        pGamma.Boost(beta)
        if pGamma.E() < 4.*Mmu : continue 

        mass1 = (muon1+muon2+pGamma).M()
        muon3, muon4 = photConFun.photConFun(pGamma,"Si")
        photConWeight = photConFun.conversionProbability(pGamma,"Si")

    elif mode == 'pipimumu' :
        # get properties of 2pi system in eta rest frame
        M2pi = random.uniform(2.*Mpi,Meta-2.*Mmu)
        # get properties of 2Mu system in eta rest frame
        M2mu = random.uniform(2.*Mmu,Meta-M2pi)
        
        p = twoBody(Meta,M2pi,M2mu)
        px, py, pz = getPxPyPz(p)
        E2pi = sqrt(M2pi*M2pi + p*p)
        E2mu = sqrt(M2mu*M2mu + p*p)
        p2pi = TLorentzVector(px, py, pz, E2pi)
        p2mu = TLorentzVector(-px, -py, -pz, E2mu) 

        # get properties of pions in 2Pi system rest frame
        p = twoBody(M2pi,Mpi,Mpi)
        px, py, pz = getPxPyPz(p)
        Epi = sqrt(Mpi*Mpi + p*p)
        pion1 = TLorentzVector(px, py, pz, Epi)
        pion2 = TLorentzVector(-px, -py, -pz, Epi)

        # get properties of muons in 2Mu system rest frame
        p = twoBody(M2mu,Mmu,Mmu)
        px, py, pz = getPxPyPz(p)
        Emu = sqrt(Mmu*Mmu + p*p)
        muon1 = TLorentzVector(px, py, pz, Emu)
        muon2 = TLorentzVector(-px, -py, -pz, Emu)
        
        # Boost the pions to the eta rest frame
        beta = p2pi.BoostVector()
        pion1.Boost(beta) 
        pion2.Boost(beta) 

        # Boost the muons to the eta rest frame
        beta = p2mu.BoostVector()
        muon1.Boost(beta) 
        muon2.Boost(beta) 

        # boost all four particles to the lab
        beta = pEta.BoostVector()
        pion1.Boost(beta)
        pion2.Boost(beta)
        muon1.Boost(beta)
        muon2.Boost(beta)
    
        # for mass1, we assign correct particle ID
        mass1 = (muon1+muon2+pion1+pion2).M()

        # for mass2, we assume that both pions are misidentified as muons
        # we further assume that the misidentification takes place after
        # their momentum was measured

        muon3 = TLorentzVector(pion1.Px(),pion1.Py(),pion1.Pz(),sqrt(pion1.E()**2 - Mpi**2 + Mmu**2))
        muon4 = TLorentzVector(pion2.Px(),pion2.Py(),pion2.Pz(),sqrt(pion2.E()**2 - Mpi**2 + Mmu**2))

    else :
        print("*** Unrecognized mode.  Exiting.")
        exit() 

    mass2 = (muon1 + muon2 + muon3 + muon4).M()
    
    weight = weight0*etaWeight*photConWeight 
    hMass1.Fill(mass1,weight)
    hWeight.Fill(1.0e8*photConWeight)
    mass2 = random.gauss(mass2,0.011*mass2)
    hMass2.Fill(mass2,weight)
    hPt.Fill(pEta.Pt(),weight)
    hMass2VsPt.Fill(pEta.Pt(),mass2,weight)

    if mass2 > 0.54 and mass2 < 0.56 : totalWeight += weight  
    
    if nPrint > 0 and pEta.Pt() > 20. :
        nPrint -= 1
        print("\n*** mode={0:s}".format(mode))
        print("weight0={0:e} etaWeight={1:e} photConWeight={2:e} weight={3:e}".format(weight0,etaWeight,photConWeight,weight))
        print("            px      py      pz      ")
        print("Muon1 : {0:8.3f} {1:8.3f} {2:8.3f}".format(muon1.Px(), muon1.Py(), muon1.Pz()))
        print("Muon2 : {0:8.3f} {1:8.3f} {2:8.3f}".format(muon2.Px(), muon2.Py(), muon2.Pz()))
        print("Muon3 : {0:8.3f} {1:8.3f} {2:8.3f}".format(muon3.Px(), muon3.Py(), muon3.Pz()))
        print("Muon4 : {0:8.3f} {1:8.3f} {2:8.3f}".format(muon4.Px(), muon4.Py(), muon4.Pz()))
        print("eta   : {0:8.3f} {1:8.3f} {2:8.3f}".format(pEta.Px(), pEta.Py(), pEta.Pz()))
        print("Mass1={0:8.3f} mass2={1:8.3f}".format(mass1,mass2))
        print("etaWeight={0:e} photConWeight={1:e} pMuonMisID={2:e} weight={3:e} totalWeight={4:e}".format(
                etaWeight, photConWeight, pMuonMisID[mode],weight,totalWeight))

print("Total weight in signal mass window={0:e}".format(totalWeight))
outputFile = TFile("{0:s}.root".format(mode),"recreate")
outputFile.cd()
hMass1.Write() 
hMass2.Write()
hWeight.Write()
hPt.Write() 
hMass2VsPt.Write()
outputFile.Close()

