#!/bin/bash

for mode in 'gammagamma' 'pipipi0' '3pi0' 'pipigamma' 'mumugamma' '2mu2e' 'pipiee' 'eegamma'
do
    echo "python eta_2mu2e.py $mode"
    python eta_2mu2e.py $mode
done
