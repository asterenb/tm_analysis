# simulate various eta decays -- and assume misconstructed as eta->2mu2e (not eta->4mu) 

from ROOT import TLorentzVector, TH1D, TH2D, TFile, TH1F
from math import sqrt, cos, sin, pi, exp
import random
import photConFun
import sys

mode = '2mu2e'
if len(sys.argv) > 1 : mode = sys.argv[1]

# calculate outgoing momentum in CM for M -> m1 + m2
def twoBody(M,m1,m2) :
    Msq, mp, mm = M*M, (m1+m2), (m1-m2)
    return sqrt((Msq - mp*mp)*(Msq - mm*mm))/(2.*M)

def getPxPyPz(p) :
    cs = random.uniform(-1.,1.)
    sn = sqrt(1. - cs*cs)
    phi = random.uniform(0.,2*pi)
    return p*sn*cos(phi), p*sn*sin(phi), p*cs

# get a four vector for the eta and an associated weight
def getEta() :
    val = random.random()
    ## generate pt according to a logarithmic distribution with two slopes 
    #if val > 0.022 : 
    #    pt = 9999.
    #    while pt > 16. :
    #        pt = 6. + random.expovariate(1./2.27)
    #else :
    #    pt = 16. + random.expovariate(1./5.08)
    #weight = 0.162/(1. + exp(-0.337*(pt-25.1)))
    pt = val*60.0 + 5.0
    weight = 1.0
    pEta = TLorentzVector()
    pEta.SetPtEtaPhiM(pt,random.uniform(-2.3,2.3),random.uniform(0.,2.*pi),Meta)
    return pEta, weight

#probability of misIDing a pion as a muon
pIDmu = 0.0015
#probability of misIDing a pion as an electron is practically 0.

# branching ratios and misID rates 
BR = {'gammagamma':0.3941, 'pipipi0':0.2292, '3pi0':0.327, 'pipigamma':0.0422,'mumugamma':3.1e-4, 
    '2mu2e':2.0e-6, 'pipiee':2.7e-4, 'eegamma':6.9e-3}
pMuonMisID = {'gammagamma':1., 'pipipi0':pIDmu**2, '3pi0':1., 'pipigamma':pIDmu**2,'mumugamma':1.,
        '2mu2e':1.0, 'pipiee':pIDmu**2, 'eegamma':1.0}
#branching factor for pi0 to decay to 2 photons
pi02gBR = .988

hMass1 = TH1D("hMass1","hMass1",100,0.4,0.6)
hMass1.GetXaxis().SetTitle("mu mu pi pi (GeV)")
#hMass2 = TH1D("hMass2","hMass2",120,0.4,1.0)
#hMass2 = TH1D("hMass2","hMass2",40,0.25,0.9)
hMass2 = TH1D("hMass2","hMass2",70,0.45,0.8)
hMass2.GetXaxis().SetTitle("M_{2#mu2e} (GeV)")
#hist for the dielectron invar. mass
hMee = TH1D("hMee","hMee",500,0.,0.5)
hMee.GetXaxis().SetTitle("M2e (GeV)")
hWeight = TH1D("hWeight","hWeight",100,0.,25.)
hWeight.GetXaxis().SetTitle("Weight x 10**8")
hEgamma = TH1D("hEgamma","hEgamma",100,0.,20.)
hEgamma.GetXaxis().SetTitle("E gamma (GeV)")
#make the pt histogram directly comparable to xsec one
newbins = [i*1.0 for i in range(6, 31)]
for i in range(32, 41, 2): 
    newbins.append(1.0*i)
for i in range(45, 56, 5): 
    newbins.append(1.0*i)
newbins.append(70.0)
newbins.append(100.0)
newnptbins = len(newbins)-1
from array import array
hPt = TH1F("hPt", "hPt", newnptbins, array('d', newbins))
hPt.GetXaxis().SetTitle("pt of eta (GeV)")
hMass2VsPt = TH2D("hMassVsPt","hMassVsPt",100,0.,100.,100,0.,1.)
hMass2VsPt.GetXaxis().SetTitle("pt of eta (GeV)")
hMass2VsPt.GetYaxis().SetTitle("Mass 2 (GeV)")

nEvents, nPrint = 1000000, 20
hEtaWeight = TH1F("hEtaWeight", "etaWeight", 1000, 0., .01) 

#open an old hPt to get the approximate number of events in the pT bin
fPtOld = TFile.Open("2mu2e_2mu2e.root", "read")
#fPtOld = TFile.Open("3pi0_2mu2e.root", "read")
hPtOld = fPtOld.Get("hPt")
hPtOld.SetName("hPtOld")
hPtOld.SetTitle("hPtOld")
#hPt = TH1D("hPt","hPt",100,0.,100.)

#nEvents, nPrint = 10000, 10 
Meta, Mmu, Mpi, Mpi0, Me = 0.5479, 0.1057, 0.1395, 0.1349, 0.000511
totalWeight, nEvent = 0., 0 

#xsec is just an overall weight so doesn't really matter for comparisons
xSec = 9.89e10    # total eta cross section above 6 GeV in fb
lumi = 38.48 #146.91 # 101.       # total lumi in fb-1 
acceptance = .00289365 #.00169 #0.00109 #approximately (accounting for weights) ###actually use pt-dependent acceptance!!
#triggerAcc = .055 # 0.24  #approximately (accounting for weights)
#nEta = xSec*lumi*acceptance*triggerAcc  # number of etas
#nEta = xSec*lumi #*acceptance  # number of etas
#weight0 = pMuonMisID[mode]*nEta*BR[mode]/nEvents

#get efficiency histograms for correction purposes
f2mu = TFile.Open("root://cmseos.fnal.gov//store/user/bgreenbe/BParking2022/ultraskimmed/bparking_sigMCtest335.root")
ptGen = f2mu.Get("hpTGenAll")
ptGen.Rebin(5)
ptAcc = f2mu.Get("hpTGenAccmmelel")
#bins should be 1 GeV wide!!!
ptAcc.Rebin(5)
ptAcc.Sumw2()
ptAcc.Divide(ptGen)
#get differential xsec measurement for accurate weighting
fxs = TFile.Open("xsec2022.root", "read")
hxs = fxs.Get("hXsecCor")

photConWeight = 1.0  
nGoodEvents = 0
for i in range(nEvents) :
    if i % 10000 == 0 : print("i={0:d}".format(i))
    # get a four vector for the eta and an associated weight    
    pEta, etaWeight = getEta() 
    #need to get the xSec as a function of pT for more accuracy
    ptbin = hxs.FindBin( pEta.Pt() )
    xSecPt = hxs.GetBinContent( ptbin ) / hxs.GetBinWidth( ptbin )
    nEvPt = hPtOld.GetBinContent( ptbin )
    if nEvPt == 0: continue
    nEta = xSecPt*lumi #*acceptance  # number of etas
    #nEta = xSec*lumi #*acceptance  # number of etas
    weight0 = pMuonMisID[mode]*nEta*BR[mode]/nEvPt
    #weight0 = pMuonMisID[mode]*nEta*BR[mode]/nEvents
    
    if mode == '2mu2e' :
        # get properties of the mu-mu systems in eta rest frame
        M2mu = random.uniform(2.*Mmu,Meta-2.*Me)
        M2lep = random.uniform(2.*Me,Meta-M2mu)
        p = twoBody(Meta,M2mu,M2lep)
        px, py, pz = getPxPyPz(p)
        E2mu = sqrt(M2mu**2 + p**2)
        E2lep = sqrt(M2lep**2 + p**2)
        p2mu = TLorentzVector(px, py, pz, E2mu)
        p2lep = TLorentzVector(-px, -py, -pz, E2lep) 

        # get properties of mu pair in M2mu rest frame 
        p = twoBody(M2mu,Mmu,Mmu)
        px, py, pz = getPxPyPz(p)
        Emu = sqrt(Mmu**2 + p**2)
        muon1 = TLorentzVector(px, py, pz, Emu)
        muon2 = TLorentzVector(-px, -py, -pz, Emu)

        # get properties of elec pair in M2lep rest frame 
        p = twoBody(M2lep, Me, Me)
        px, py, pz = getPxPyPz(p)
        Eel = sqrt(Me**2 + p**2)
        lep3 = TLorentzVector(px, py, pz, Eel)
        lep4 = TLorentzVector(-px, -py, -pz, Eel)

        # Boost the muons to the eta rest frame
        beta = p2mu.BoostVector()
        muon1.Boost(beta) 
        muon2.Boost(beta) 
        beta = p2lep.BoostVector()
        lep3.Boost(beta) 
        lep4.Boost(beta) 
   
        # boost all four particles to the lab
        beta = pEta.BoostVector()
        muon1.Boost(beta)
        muon2.Boost(beta)
        lep3.Boost(beta)
        lep4.Boost(beta)
        mass1 = (muon1 + muon2 + lep3 + lep4).M() 

    elif mode == 'gammagamma' :
        # get properties of gammas in eta rest frame
        p = 0.5*Meta
        px, py, pz = getPxPyPz(p)
        gamma1 = TLorentzVector(px, py, pz, p)
        gamma2 = TLorentzVector(-px, -py, -pz, p)

        # boost the gammas to the lab
        beta = pEta.BoostVector()
        gamma1.Boost(beta)
        gamma2.Boost(beta)
        #4Mmu because this is min for gamma->mumu approximations to work (xs is ~0 down there anyway).
        # No such issue for gamma->ee, so can use 2Me
        if gamma1.E() < 4.*Mmu or gamma2.E() < 2.*Me : continue 

        mass1 = (gamma1 + gamma2).M()
        hMass1.Fill(mass1)

        #TODO: test these functions thoroughly using GEANT and photon gun    
        muon1, muon2 = photConFun.photConFun(gamma1,"Si")
        photConWeight = photConFun.conversionProbability(gamma1,"Si")
        lep3, lep4 = photConFun.photConFun_ee(gamma2,"Si")
        photConWeight *= photConFun.conversionProbability_ee(gamma2,"Si")

    elif mode == 'pipipi0' :
        # get properties of pi+pi- system in eta rest frame
        M2pi = random.uniform(2.*Mpi,Meta-Mpi0)

        p = twoBody(Meta,M2pi,Mpi0)
        px, py, pz = getPxPyPz(p)
        E2pi = sqrt(M2pi*M2pi + p*p)
        Epi0 = sqrt(Mpi0*Mpi0 + p*p)
        p2pi = TLorentzVector(px, py, pz, E2pi)
        pPi0 = TLorentzVector(-px, -py, -pz, Epi0) 

        # get properties of pions in 2Pi system rest frame
        p = twoBody(M2pi,Mpi,Mpi)
        px, py, pz = getPxPyPz(p)
        Epi = sqrt(Mpi*Mpi + p*p)
        pion1 = TLorentzVector(px, py, pz, Epi)
        pion2 = TLorentzVector(-px, -py, -pz, Epi)

        # Boost the pions to the eta rest frame
        beta = p2pi.BoostVector()
        pion1.Boost(beta) 
        pion2.Boost(beta) 

        #With 98.8% probability, the pi0 decays to gamma gamma
        ggdecay = random.random() < pi02gBR
        if ggdecay:
            # gammas in the pi0 rest frame
            p =  0.5*Mpi0 
            px, py, pz = getPxPyPz(p)
            gamma1 = TLorentzVector(px, py, pz, p)
            gamma2 = TLorentzVector(-px, -py, -pz, p) 

           # boost the gammas to the eta rest frame
            beta = pPi0.BoostVector()
            gamma1.Boost(beta) 
            gamma2.Boost(beta) 

            # boost all three particles to the lab
            beta = pEta.BoostVector()
            pion1.Boost(beta)
            pion2.Boost(beta)
            gamma1.Boost(beta)
            gamma2.Boost(beta)
       
           # for mass1, we assign correct particle ID
            mass1 = (pion1+pion2+gamma1+gamma2).M()

            # for mass2, we assume that 
            # both pions are misidentified as muons
            #     and also one of the gammas converts to an electron pair 

            #not possible if energy is less than 2Me
            if gamma1.E() < 2.*Me :  
                continue   

            lep3, lep4 = photConFun.photConFun_ee(gamma1, "Si")
            # we add a factor of two, since either photon can convert
            photConWeight = 2.*photConFun.conversionProbability_ee(gamma1,"Si")
        #otherwise, the pi0 decays to ee gamma
        else:
            #get the decay products: 2-lepton and photon
            M2e = random.uniform(2.*Me,Mpi0)
            p = twoBody(Mpi0, M2e, 0) 
            px, py, pz = getPxPyPz(p)
            E2e = sqrt(M2e*M2e + p*p)
            p2e_4 = TLorentzVector(px, py, pz, E2e)
            pGamma = TLorentzVector(-px, -py, -pz, p)
        
            # get properties of electrons in 2e system rest frame
            p = twoBody(M2e,Me,Me)
            px, py, pz = getPxPyPz(p)
            Ee = sqrt(Me*Me + p*p)
            elec1 = TLorentzVector(px, py, pz, Ee)
            elec2 = TLorentzVector(-px, -py, -pz, Ee)
        
            # Boost the electrons to the pi0 rest frame
            beta = p2e_4.BoostVector()
            elec1.Boost(beta) 
            elec2.Boost(beta)  

            # boost all three particles to the eta rest frame
            beta = pPi0.BoostVector()
            elec1.Boost(beta)
            elec2.Boost(beta)
            pGamma.Boost(beta)  

            # lastly, boost all particles to the lab
            beta = pEta.BoostVector()
            pion1.Boost(beta)
            pion2.Boost(beta)
            elec1.Boost(beta)
            elec2.Boost(beta)
            pGamma.Boost(beta)

            mass1 = (pion1+pion2+elec1+elec2+pGamma).M()

            lep3 = elec1
            lep4 = elec2

        muon1 = TLorentzVector(pion1.Px(),pion1.Py(),pion1.Pz(),sqrt(pion1.E()**2 - Mpi**2 + Mmu**2))
        muon2 = TLorentzVector(pion2.Px(),pion2.Py(),pion2.Pz(),sqrt(pion2.E()**2 - Mpi**2 + Mmu**2))

        
    elif mode == '3pi0' :
        # create a list of gammas
        gammas = []

        # get properties of pi0pi0 system in eta rest frame
        M2pi = random.uniform(2.*Mpi0,Meta-Mpi0)

        p = twoBody(Meta,M2pi,Mpi0)
        px, py, pz = getPxPyPz(p)
        E2pi = sqrt(M2pi*M2pi + p*p)
        Epi0 = sqrt(Mpi0*Mpi0 + p*p)
        p2pi = TLorentzVector(px, py, pz, E2pi)
        pi0_3 = TLorentzVector(-px, -py, -pz, Epi0) 

        # get properties of pions in 2Pi system rest frame
        p = twoBody(M2pi,Mpi0,Mpi0)
        px, py, pz = getPxPyPz(p)
        Epi = sqrt(Mpi0*Mpi0 + p*p)
        pi0_1 = TLorentzVector(px, py, pz, Epi)
        pi0_2 = TLorentzVector(-px, -py, -pz, Epi)

        #with 96.4% prob, all 3 pi0s decay to gamma gamma
        ggdecay = random.random() > (1.0 - pi02gBR**3) 
        #if not pi0->gamma gamma, then pi0->ee gamma
        if not ggdecay:
            #get the decay products: 2-lepton and photon
            M2e = random.uniform(2.*Me,Mpi0)
            p = twoBody(Mpi0, M2e, 0) 
            px, py, pz = getPxPyPz(p)
            E2e = sqrt(M2e*M2e + p*p)
            p2e_4 = TLorentzVector(px, py, pz, E2e)
            pGamma = TLorentzVector(-px, -py, -pz, p)
        
            # get properties of electrons in 2e system rest frame
            p = twoBody(M2e,Me,Me)
            px, py, pz = getPxPyPz(p)
            Ee = sqrt(Me*Me + p*p)
            elec1 = TLorentzVector(px, py, pz, Ee)
            elec2 = TLorentzVector(-px, -py, -pz, Ee)
        
            # Boost the electrons to the pi0 rest frame
            beta = p2e_4.BoostVector()
            elec1.Boost(beta)
            elec2.Boost(beta)

            # boost all three particles to the eta rest frame
            #randomly choose one of the three pions to have undergone this decay
            whichpi = int(random.random()*3)
            if whichpi == 2:
                beta = pi0_3.BoostVector()
            elif whichpi == 1:
                #first need to boost to the rest frame of the 2pi0 mass
                beta = p2pi.BoostVector()
                elec1.Boost(beta)
                elec2.Boost(beta)
                pGamma.Boost(beta)
                beta = pi0_2.BoostVector()
            else:
                beta = p2pi.BoostVector()
                elec1.Boost(beta)
                elec2.Boost(beta)
                pGamma.Boost(beta)
                beta = pi0_1.BoostVector()
            elec1.Boost(beta)
            elec2.Boost(beta)
            pGamma.Boost(beta)

            # lastly, boost all particles to the lab
            beta = pEta.BoostVector()
            elec1.Boost(beta)
            elec2.Boost(beta)
            pGamma.Boost(beta)
            lep3 = elec1
            lep4 = elec2

        # Boost the pions to the eta rest frame
        beta = p2pi.BoostVector()
        pi0_1.Boost(beta) 
        pi0_2.Boost(beta) 

        # gammas in the pi0_1 rest frame
        p =  0.5*Mpi0 
        px, py, pz = getPxPyPz(p)
        gammas.append(TLorentzVector(px, py, pz, p))
        gammas.append(TLorentzVector(-px, -py, -pz, p))

        # boost the gammas to the eta rest frame
        beta = pi0_1.BoostVector()
        gammas[0].Boost(beta) 
        gammas[1].Boost(beta) 

        # gammas in the pi0_2 rest frame
        px, py, pz = getPxPyPz(p)
        gammas.append(TLorentzVector(px, py, pz, p))
        gammas.append(TLorentzVector(-px, -py, -pz, p))

        # boost the gammas to the eta rest frame
        beta = pi0_2.BoostVector()
        gammas[2].Boost(beta) 
        gammas[3].Boost(beta)

        # gammas in the pi0_3 rest frame 
        px, py, pz = getPxPyPz(p)
        gammas.append(TLorentzVector(px, py, pz, p))
        gammas.append(TLorentzVector(-px, -py, -pz, p))

        # boost the gammas to the eta rest frame
        beta = pi0_3.BoostVector()
        gammas[4].Boost(beta) 
        gammas[5].Boost(beta)

        # boost all six gammas to the lab
        beta = pEta.BoostVector()
        for i in range(6) : 
            gammas[i].Boost(beta)

        j1, j2 = random.sample(range(6),2) 
        #debugging -- is this block the source of the problem??
        if (not ggdecay) and (int(j1/2) == whichpi):
            #replace j1 by the photon from the eeg decay
            gammas[j1] = pGamma
            #replace the other j (either one above or one below) by the 2 electrons
            gammas[ j1 + (-1)**(j1%2)] = lep3 + lep4
        elif not ggdecay:
            gammas[whichpi*2] = pGamma
            gammas[whichpi*2+1] = lep3 + lep4

        # for mass1, we assign correct particle ID
        #if ggdecay:
        mass1 = (gammas[0]+gammas[1]+gammas[2]+gammas[3]+gammas[4]+gammas[5]).M()

        # for mass2, we assume that two of the gammas convert to elec pairs 
        if gammas[j1].E() < 4.*Mmu : continue 
        if ggdecay and gammas[j2].E() < 2.*Me : continue 
        muon1, muon2 = photConFun.photConFun(gammas[j1],"Si")
        if ggdecay:
            lep3, lep4 = photConFun.photConFun_ee(gammas[j2],"Si")
        photConWeight = photConFun.conversionProbability(gammas[j1],"Si")
        if ggdecay:
            photConWeight *= photConFun.conversionProbability_ee(gammas[j2],"Si")
            # we add a factor of 15 to take into account that there are 6 choose 2 combinations
            photConWeight *= 15. 
        else:
            # if the pi0 decayed to eeg, then 5 photons to choose one from
            photConWeight *= 5

    elif mode == 'pipigamma' :
        # get properties of pi+pi- system in eta rest frame
        M2pi = random.uniform(2.*Mpi,Meta)

        p = twoBody(Meta,M2pi,0.)
        px, py, pz = getPxPyPz(p)
        E2pi = sqrt(M2pi*M2pi + p*p)
        p2pi = TLorentzVector(px, py, pz, E2pi)
        gamma = TLorentzVector(-px, -py, -pz, p) 

        # get properties of pions in 2Pi system rest frame
        p = twoBody(M2pi,Mpi,Mpi)
        px, py, pz = getPxPyPz(p)
        Epi = sqrt(Mpi*Mpi + p*p)
        pion1 = TLorentzVector(px, py, pz, Epi)
        pion2 = TLorentzVector(-px, -py, -pz, Epi)

        # Boost the pions to the eta rest frame
        beta = p2pi.BoostVector()
        pion1.Boost(beta) 
        pion2.Boost(beta) 
   
        # boost all three particles to the lab
        beta = pEta.BoostVector()
        pion1.Boost(beta)
        pion2.Boost(beta)
        gamma.Boost(beta)
   
        # for mass2, we assume that both pions are misidentified as muons
        # and the gamma converts to an elec pair 
        muon1 = TLorentzVector(pion1.Px(),pion1.Py(),pion1.Pz(),sqrt(pion1.E()**2 - Mpi**2 + Mmu**2))
        muon2 = TLorentzVector(pion2.Px(),pion2.Py(),pion2.Pz(),sqrt(pion2.E()**2 - Mpi**2 + Mmu**2))
        if gamma.E() < 2.*Me : continue    

        # for mass1, we assign correct particle ID
        mass1 = (pion1+pion2+gamma).M()

        lep3, lep4 = photConFun.photConFun_ee(gamma,"Si")
        photConWeight = photConFun.conversionProbability_ee(gamma,"Si")
    
    elif mode == 'mumugamma' :
        # get properties of 2Mu system in eta rest frame
        M2mu = random.uniform(2.*Mmu,Meta)
        p = twoBody(Meta,M2mu,0.)
        px, py, pz = getPxPyPz(p)
        E2Mu = sqrt(M2mu*M2mu + p*p)
        p2Mu_4 = TLorentzVector(px, py, pz, E2Mu)
        pGamma = TLorentzVector(-px, -py, -pz, p)
    
        # get properties of muons in 2M system rest frame
        p = twoBody(M2mu,Mmu,Mmu)
        px, py, pz = getPxPyPz(p)
        EMu = sqrt(Mmu*Mmu + p*p)
        muon1 = TLorentzVector(px, py, pz, EMu)
        muon2 = TLorentzVector(-px, -py, -pz, EMu)
    
        # Boost the muons to the eta rest frame
        beta = p2Mu_4.BoostVector()
        muon1.Boost(beta) 
        muon2.Boost(beta)  

        # boost all three particles to the lab
        beta = pEta.BoostVector()
        muon1.Boost(beta)
        muon2.Boost(beta)
        pGamma.Boost(beta)

        #now assume that the photon converted into 2 electrons
        if pGamma.E() < 2.*Me : continue 

        mass1 = (muon1+muon2+pGamma).M()

        lep3, lep4 = photConFun.photConFun_ee(pGamma,"Si")
        photConWeight = photConFun.conversionProbability_ee(pGamma,"Si")

    elif mode == 'eegamma' :
        # get properties of 2El system in eta rest frame
        M2el = random.uniform(2.*Me, Meta)
        p = twoBody(Meta,M2el,0.)
        px, py, pz = getPxPyPz(p)
        E2El = sqrt(M2el**2 + p**2)
        p2El_4 = TLorentzVector(px, py, pz, E2El)
        pGamma = TLorentzVector(-px, -py, -pz, p)
    
        # get properties of electrons in 2El system rest frame
        p = twoBody(M2el,Me,Me)
        px, py, pz = getPxPyPz(p)
        Eel = sqrt(Me**2 + p**2)
        lep3 = TLorentzVector(px, py, pz, Eel)
        lep4 = TLorentzVector(-px, -py, -pz, Eel)
    
        # Boost the electrons to the eta rest frame
        beta = p2El_4.BoostVector()
        lep3.Boost(beta) 
        lep4.Boost(beta)  

        # boost all three particles to the lab
        beta = pEta.BoostVector()
        lep3.Boost(beta)
        lep4.Boost(beta)
        pGamma.Boost(beta)

        #now assume the photon converts to muons
        if pGamma.E() < 4.*Mmu : continue 

        mass1 = (lep3+lep4+pGamma).M()

        muon1, muon2 = photConFun.photConFun(pGamma,"Si")
        photConWeight = photConFun.conversionProbability(pGamma,"Si")

    elif mode == 'pipiee' :
        # get properties of 2pi system in eta rest frame
        M2pi = random.uniform(2.*Mpi,Meta-2.*Mmu)
        # get properties of 2El system in eta rest frame
        M2el = random.uniform(2.*Me,Meta-M2pi)
        
        p = twoBody(Meta,M2pi,M2el)
        px, py, pz = getPxPyPz(p)
        E2pi = sqrt(M2pi*M2pi + p*p)
        E2el = sqrt(M2el*M2el + p*p)
        p2pi = TLorentzVector(px, py, pz, E2pi)
        p2el = TLorentzVector(-px, -py, -pz, E2el) 

        # get properties of pions in 2Pi system rest frame
        p = twoBody(M2pi,Mpi,Mpi)
        px, py, pz = getPxPyPz(p)
        Epi = sqrt(Mpi*Mpi + p*p)
        pion1 = TLorentzVector(px, py, pz, Epi)
        pion2 = TLorentzVector(-px, -py, -pz, Epi)

        # get properties of electrons in 2El system rest frame
        p = twoBody(M2el,Me,Me)
        px, py, pz = getPxPyPz(p)
        Eel = sqrt(Me**2 + p**2)
        lep3 = TLorentzVector(px, py, pz, Eel)
        lep4 = TLorentzVector(-px, -py, -pz, Eel)
        
        # Boost the pions to the eta rest frame
        beta = p2pi.BoostVector()
        pion1.Boost(beta) 
        pion2.Boost(beta) 

        # Boost the muons to the eta rest frame
        beta = p2el.BoostVector()
        lep3.Boost(beta) 
        lep4.Boost(beta) 

        # boost all four particles to the lab
        beta = pEta.BoostVector()
        pion1.Boost(beta)
        pion2.Boost(beta)
        lep3.Boost(beta)
        lep4.Boost(beta)
    
        # for mass1, we assign correct particle ID
        mass1 = (lep3+lep4+pion1+pion2).M()

        # for mass2, we assume that both pions are misidentified as muons
        # we further assume that the misidentification takes place after
        # their momentum was measured

        muon1 = TLorentzVector(pion1.Px(),pion1.Py(),pion1.Pz(),sqrt(pion1.E()**2 - Mpi**2 + Mmu**2))
        muon2 = TLorentzVector(pion2.Px(),pion2.Py(),pion2.Pz(),sqrt(pion2.E()**2 - Mpi**2 + Mmu**2))

    else :
        print("*** Unrecognized mode.  Exiting.")
        exit() 

    mass2 = (muon1 + muon2 + lep3 + lep4).M()

    #look at just the ee invariant mass to see if it can be used for sig-bkg discrimination
    Mee = (lep3 + lep4).M()
    
    acceptance = ptAcc.GetBinContent( ptAcc.FindBin(pEta.Pt()) ) 
    weight = weight0*etaWeight*photConWeight*acceptance 
    hMass1.Fill(mass1,weight)
    hWeight.Fill(1.0e8*photConWeight)
    #mass2 = random.gauss(mass2,0.011*mass2)
    #smearing also included
    mass2 = random.gauss(mass2,0.022*mass2)
    hMass2.Fill(mass2,weight)
    #hPt.Fill(pEta.Pt(),weight)
    #actually want the hPt hist unweighted now so can use for normalization
    hPt.Fill(pEta.Pt())
    hMass2VsPt.Fill(pEta.Pt(),mass2,weight)
    hEtaWeight.Fill(etaWeight)
    nGoodEvents += 1

    Mee = random.gauss(Mee,0.011*Mee)
    hMee.Fill(Mee, weight)

    #if mass2 > 0.54 and mass2 < 0.56 : totalWeight += weight  
    #use 2mu2e peak range instead
    if mass2 > 0.51 and mass2 < 0.63 : totalWeight += weight  
    
    if nPrint > 0: # and pEta.Pt() > 20. :
        nPrint -= 1
        print("\n*** mode={0:s}".format(mode))
        print("weight0={0:e} etaWeight={1:e} BR={4:e} photConWeight={2:e} weight={3:e}".format(weight0,etaWeight,photConWeight,weight,BR[mode]))
        print("            px      py      pz      ")
        print("Muon1 : {0:8.3f} {1:8.3f} {2:8.3f}".format(muon1.Px(), muon1.Py(), muon1.Pz()))
        print("Muon2 : {0:8.3f} {1:8.3f} {2:8.3f}".format(muon2.Px(), muon2.Py(), muon2.Pz()))
        print("Muon3 : {0:8.3f} {1:8.3f} {2:8.3f}".format(lep3.Px(), lep3.Py(), lep3.Pz()))
        print("Muon4 : {0:8.3f} {1:8.3f} {2:8.3f}".format(lep4.Px(), lep4.Py(), lep4.Pz()))
        print("eta   : {0:8.3f} {1:8.3f} {2:8.3f}".format(pEta.Px(), pEta.Py(), pEta.Pz()))
        print("Mass1={0:8.3f} mass2={1:8.3f}".format(mass1,mass2))
        print("etaWeight={0:e} photConWeight={1:e} pMuonMisID={2:e} weight={3:e} totalWeight={4:e}".format(
                etaWeight, photConWeight, pMuonMisID[mode],weight,totalWeight))
        print("etaWeight %f"%(etaWeight)) 

fPtOld.Close()
fxs.Close()
print("Total weight in signal mass window={0:e}".format(totalWeight))
outputFile = TFile("{0:s}_2mu2e.root".format(mode),"recreate")
outputFile.cd()
hMass1.Write() 
hMass2.Write()
hWeight.Write()
hPt.Write() 
hMass2VsPt.Write()
hMee.Write()
hEtaWeight.Write()
outputFile.Close()
print("nGoodEvents: %d out of %d"%(nGoodEvents, nEvents)) 

