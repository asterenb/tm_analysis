import photConFun
from ROOT import TLorentzVector, TH1D, TFile, TGraph
from math import pi, cos, sin, sqrt, acos, degrees
import random 
from array import array
nPrint = 10 

nEnergies = 18
x_values = [0.0 for j in range(nEnergies)]
y_values = [0.0 for j in range(nEnergies)]

##probability of conversion
hprob = TH1D("hprob", "hprob", 1000, 0, 1) 

#p = 10. #10.
navg = 100000
for j in range(0, nEnergies):
    p = 2**(j-nEnergies/2)
    print("j=%d, p=%f GeV"%(j, p)) 
    x_values[j] = p
    ##hAngle = TH1D("hAngle","hAngle",1800,0.,180.)
    #hAngle = TH1D("hAngle","hAngle",1000,0.,.0001)
    ##pt of the first electron
    #hpt0 = TH1D("hpt0", "hpt0", 100, 0, 10.)
    ##pt of the second electron
    #hpt1 = TH1D("hpt1", "hpt1", 100, 0, 10.)
    ##pt of the sum
    #hptsum = TH1D("hptsum", "hptsum", 100, 0, 10.)
    ##opening angle betwixt electron and positron
    #hOpening = TH1D("hOpening", "hOpening", 1000, 0., 180.) 
    y_avg = 0.0
    for i in range(navg) :
    #for i in range(10) :
        cs = random.uniform(-1.,1.)
        sn = sqrt(1. - cs*cs)
        phi = random.uniform(0.,2.*pi)
        gamma_4 = TLorentzVector(p*sn*cos(phi),p*sn*sin(phi),p*cs,p)
        elec0, elec1 = photConFun.photConFun_ee(gamma_4,'Si')
        prob = photConFun.conversionProbability_ee(gamma_4, 'Si')
        y_avg += prob
        hprob.Fill(prob)
        ##elec0, elec1 = photConFun.photConFun(gamma_4,'Si')
        #sum = elec0 + elec1
        #dot = (gamma_4.Px()*sum.Px() + gamma_4.Py()*sum.Py() + gamma_4.Pz()*sum.Pz())/(gamma_4.P()*sum.P())
        #dotee = (elec0.Px()*elec1.Px() + elec0.Py()*elec1.Py() + elec0.Pz()*elec1.Pz())/(elec0.P()*elec1.P())
        ##rounding error can cause dot to be very slightly over 1. but if it's more than a rounding error, something is wrong.
        #if dot > 1.0001 :
        #   print("****i={0:d} Bad dot product: {1:f}".format(i, dot))
        #   print("        px      py       pz       E       P") 
        #   print("Gam  {0:8.3f} {1:8.3f} {2:8.3f} {3:8.3f} {4:8.3f}".format(gamma_4.Px(),gamma_4.Py(),gamma_4.Pz(),gamma_4.E(), gamma_4.P()))
        #   print("El1  {0:8.3f} {1:8.3f} {2:8.3f} {3:8.3f} {4:8.3f}".format(elec0.Px(),elec0.Py(),elec0.Pz(),elec0.E(), elec0.P()))
        #   print("El2  {0:8.3f} {1:8.3f} {2:8.3f} {3:8.3f} {4:8.3f}".format(elec1.Px(),elec1.Py(),elec1.Pz(),elec1.E(), elec1.P()))
        #   print("Sum  {0:8.3f} {1:8.3f} {2:8.3f} {3:8.3f} {4:8.3f} M={4:8.3f}".format(sum.Px(),sum.Py(),sum.Pz(),sum.E(),sum.P(),sum.M()))  
        #   continue
        #elif dot > 1. : 
        #    #print("dot too high! setting to 1.")
        #    dot = 1.
        #try :
        #    angle = degrees(acos(dot))
        #    hAngle.Fill(angle) 
        #    #print(angle)
        #except ValueError :
        #    continue 

        #if dotee > 1.:
        #    print("dotee too high!! {}".format(dotee)) 
        #    dotee = 1. 
        #opening = degrees(acos(dotee)) 
        #hOpening.Fill(opening)
        #hpt0.Fill(elec0.Pt())
        #hpt1.Fill(elec1.Pt())
        #hptsum.Fill(sum.Pt()) 
        #if nPrint > 0 :
        #   nPrint -= 1
        #   print("\n        px      py       pz       E   ") 
        #   print("Gam  {0:8.3f} {1:8.3f} {2:8.3f} {3:8.3f}".format(gamma_4.Px(),gamma_4.Py(),gamma_4.Pz(),gamma_4.E()))
        #   print("El1  {0:8.3f} {1:8.3f} {2:8.3f} {3:8.3f}".format(elec0.Px(),elec0.Py(),elec0.Pz(),elec0.E()))
        #   print("El2  {0:8.3f} {1:8.3f} {2:8.3f} {3:8.3f}".format(elec1.Px(),elec1.Py(),elec1.Pz(),elec1.E()))
        #   print("Sum  {0:8.3f} {1:8.3f} {2:8.3f} {3:8.3f} M={4:8.3f}".format(sum.Px(),sum.Py(),sum.Pz(),sum.E(),sum.M()))

    y_avg /= navg
    y_values[j] = y_avg

tg = TGraph(nEnergies, array('d', x_values), array('d', y_values))

outputFile = TFile("photConVsE_ee.root","recreate")
outputFile.cd()
#hAngle.Write() 
#hpt0.Write()
#hpt1.Write()
#hptsum.Write()
hprob.Write()
tg.Write()
#hOpening.Write()
#tg.Draw()
#input("")
outputFile.Close()
