import ROOT
import sys, time
import numpy as np
import math
from array import array
from math import sqrt, sin, cos, log


def getArgs():
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("-E", "--energy", default=100, type=float, help="Energy of incoming photon (GeV)")
    parser.add_argument("-Z", "--Z", default=14, type=int, help="Atomic number of barrier element")
    parser.add_argument("-p", "--printing", action='store_true', help="Print parameters")
    parser.add_argument("-n", "--nEvents", default=100000, type=int, help="Number of MC events generated")
    
    return parser.parse_args()





# ------- Execution begins here -------
ROOT.gROOT.SetBatch()
args = getArgs()
E_gamma, Z, nEvents = args.energy, args.Z, args.nEvents            # E_gamma (GeV), delta_z (mm)

atomic_mass = {1:1.007, 4:9.012, 14:28.0855, 26:55.847, 82:207.2}
elements    = {1:"H", 4:"Be", 14:"Si", 26:"Fe", 82:"Pb"}
A           = atomic_mass.get(Z)
element     = elements.get(Z)
print("Working on : E = {0:.0f}, element = {1} ...".format(E_gamma,element))
dirr        = "outputs/E={0:.0f},Z={1:d}/".format(E_gamma,Z) 
dirr        = "outputs/"

outFile     = ROOT.TFile(dirr + "hists_full.root","recreate")
hXPlus      = ROOT.TH1F("hXPlus","x_plus",200,0,1)
hT          = ROOT.TH1F("hT","t",200,0,1)
hPsi        = ROOT.TH1F("hPsi","psi",200,0,2*math.pi)
hRho        = ROOT.TH1F("hRho","rho",200,0,1)
hThetaPlus  = ROOT.TH1F("hThetaPlus","theta_plus",200,0,0.1)
hAngle      = ROOT.TH1F("hAngle","angle",200,0,0.1)
hBalance    = ROOT.TH1F("hBalance","balance",200,-0.4,0.4)

tree        = ROOT.TTree("Events","Events")
angle       = array('f', [0])
balance     = array('f', [0])
tree.Branch('angle',       angle,      'angle/F')
tree.Branch('balance',     balance,    'balance/F')


# ------- constants -------
m_mu, m_e       = 0.1057, 0.0005110
D_n, B          = 1.54*(A**0.27) if (Z!=1) else 1.49, 183. if (Z!=1) else 202.4
x_min, x_max    = 1./2-sqrt(1./4-m_mu/E_gamma), 1./2+sqrt(1./4-m_mu/E_gamma)

W_infty         = B*Z**(-1./3)/D_n * m_mu/m_e
delta_max       = m_mu**2 / (2*E_gamma*0.5*0.5)
W_max           = W_infty * (1+(D_n*sqrt(math.e)-2)*delta_max/m_mu) / (1+B*Z**(-1./3)*sqrt(math.e)*delta_max/m_e)



n0, n1, n2, n3, n5 = 0,0,0,0,0
while True:
    R = np.random.rand(8)
    n0 += 1


    ## ------- Step 1 -------
    x_plus      = x_min + R[0]*(x_max-x_min)
    x_minus     = 1-x_plus
    delta       = m_mu**2 / (2*E_gamma*x_plus*x_minus)
    W           = W_infty * (1+(D_n*sqrt(math.e)-2)*delta/m_mu) / (1+B*Z**(-1./3)*sqrt(math.e)*delta/m_e)
    #print("W={0:f} W_infty={1:f} W_max={2:f}".format(W,W_infty,W_max))

    test_var = (1-(4./3)*x_plus*x_minus)*log(W)/log(W_max)
    #print("test_var={0:f}".format(test_var))
    if ( (1-(4./3)*x_plus*x_minus)*log(W)/log(W_max) < R[1] ): continue
    hXPlus.Fill(x_plus)
    n1 += 1

    ## ------- Step 2 -------
    C1          = (0.35*A**0.27)**2 / (x_plus*x_minus*E_gamma/m_mu)
    f1_max      = (1-x_plus*x_minus) / (1+C1)
    t           = R[2]
    
    if ( (1-2*x_plus*x_minus+4*x_plus*x_minus*t*(1-t))/(1+C1/t/t)/f1_max < R[3] ): continue
    hT.Fill(t)
    hXPlus.Fill(x_plus)
    n2 += 1


    ## ------- Step 3 -------
    f2_max      = 1-2*x_plus*x_minus*(1-4*t*(1-t))
    psi         = R[4] * 2*math.pi
    
    if ( (1-2*x_plus*x_minus+4*x_plus*x_minus*t*(1-t)*(1+cos(2*psi)))/f2_max < R[5] ) : continue
    hPsi.Fill(psi)
    n3 += 1


    ## ------- Step 4 -------
    rho_max_sq  = 1.9/(A**0.27) * (1./t-1)
    C2          = 4/sqrt(x_plus*x_minus) * ((m_mu/(2*E_gamma*x_plus*x_minus*t))**2 + (m_e/(183.*Z**(-1./3)*m_mu))**2)**2
    beta        = log((C2+rho_max_sq**2)/C2)
    rho         = (C2*(math.exp(beta*R[6])-1))**(0.25)

    hRho.Fill(rho)



    ## ------- Step 5 -------
    gamma_plus, gamma_minus = x_plus*E_gamma/m_mu, x_minus*E_gamma/m_mu
    u           = sqrt(1./t-1)
    theta_plus, theta_minus = 1./gamma_plus*(u+rho/2*cos(psi)), 1./gamma_minus*(u-rho/2*cos(psi))
    phi         = rho/u*sin(psi)

    if (theta_plus > math.pi or theta_minus > math.pi): continue
    hThetaPlus.Fill(theta_plus)
    n5 += 1
    hXPlus.Fill(x_plus)

    ## ------- Vectors -------
    muon1, muon2        = ROOT.TLorentzVector(), ROOT.TLorentzVector()
    p_plus, p_minus     = sqrt((x_plus*E_gamma)**2-m_mu**2), sqrt((x_minus*E_gamma)**2-m_mu**2)
    phi0        = R[7] * 2*math.pi
    muon1.SetXYZM(p_plus*sin(theta_plus)*cos(phi0+phi/2)       , p_plus*sin(theta_plus)*sin(phi0+phi/2)    , p_plus*cos(theta_plus),   m_mu)
    muon2.SetXYZM(-p_minus*sin(theta_minus)*cos(phi0-phi/2)    , -p_minus*sin(theta_minus)*sin(phi0-phi/2) , p_minus*cos(theta_minus), m_mu)

    angle[0]    = muon1.Angle(muon2.Vect())
    hAngle.Fill(angle[0])

    p1, p2      = muon1.Pt(), muon2.Pt()
    balance[0]  = 2 * (p1-p2) / (p1+p2)
    hBalance.Fill(balance[0])
    tree.Fill()

    if (n5 % 50000 == 0): print("Generated Events: {0:7d}".format(n5))
    if n5 == nEvents: break


## ------- Drawing -------
c = ROOT.TCanvas("","",1800,800)
c.Divide(3,2,0.05,0.05)
c.cd(1)
hXPlus.Draw()
c.cd(2)
hT.Draw()
c.cd(3)
hPsi.Draw()
c.cd(4)
hRho.Draw()
c.cd(5)
hThetaPlus.Draw()
c.SaveAs(dirr + "pars.png")

c = ROOT.TCanvas("","")
hAngle.Draw()
c.SaveAs(dirr + "angle.png")

c = ROOT.TCanvas("","")
hBalance.Draw()
c.SaveAs(dirr + "balance.png")


## ------- Saving Files -------
outFile.cd()
tree.Write()
hAngle.Write()
hBalance.Write()
hXPlus.Write()
hT.Write()
hPsi.Write()
hRho.Write()
hThetaPlus.Write()

outFile2 = ROOT.TFile(dirr + "hists.root","recreate")
hAngle.Write()
hBalance.Write()
outFile2.Close()
outFile.Close()
