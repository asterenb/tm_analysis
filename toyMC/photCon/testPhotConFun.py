import photConFun
from ROOT import TLorentzVector, TH1D, TFile 
from math import pi, cos, sin, sqrt, acos, degrees
import random 
nPrint = 10 
p = 10.
hAngle = TH1D("hAngle","hAngle",180,0.,180.)
for i in range(10000) :
    cs = random.uniform(-1.,1.)
    sn = sqrt(1. - cs*cs)
    phi = random.uniform(0.,2.*pi)
    gamma_4 = TLorentzVector(p*sn*cos(phi),p*sn*sin(phi),p*cs,p)
    muon1, muon2 = photConFun.photConFun(gamma_4,'Si')
    sum = muon1 + muon2
    dot = (gamma_4.Px()*sum.Px() + gamma_4.Py()*sum.Py() + gamma_4.Pz()*sum.Pz())/(gamma_4.P()*sum.P())
    if dot > 1. :
       print("****i={0:d} Bad dot product".format(i))
       print("        px      py       pz       E   ") 
       print("Gam  {0:8.3f} {1:8.3f} {2:8.3f} {3:8.3f}".format(gamma_4.Px(),gamma_4.Py(),gamma_4.Pz(),gamma_4.E()))
       print("Mu1  {0:8.3f} {1:8.3f} {2:8.3f} {3:8.3f}".format(muon1.Px(),muon1.Py(),muon1.Pz(),muon1.E()))
       print("Mu2  {0:8.3f} {1:8.3f} {2:8.3f} {3:8.3f}".format(muon2.Px(),muon2.Py(),muon2.Pz(),muon2.E()))
       print("Sum  {0:8.3f} {1:8.3f} {2:8.3f} {3:8.3f} M={4:8.3f}".format(sum.Px(),sum.Py(),sum.Pz(),sum.E(),sum.M()))  
    try :
        angle = degrees(acos(dot))
        hAngle.Fill(angle) 
    except ValueError :
        continue 

    if nPrint > 0 :
       nPrint -= 1
       print("\n        px      py       pz       E   ") 
       print("Gam  {0:8.3f} {1:8.3f} {2:8.3f} {3:8.3f}".format(gamma_4.Px(),gamma_4.Py(),gamma_4.Pz(),gamma_4.E()))
       print("Mu1  {0:8.3f} {1:8.3f} {2:8.3f} {3:8.3f}".format(muon1.Px(),muon1.Py(),muon1.Pz(),muon1.E()))
       print("Mu2  {0:8.3f} {1:8.3f} {2:8.3f} {3:8.3f}".format(muon2.Px(),muon2.Py(),muon2.Pz(),muon2.E()))
       print("Sum  {0:8.3f} {1:8.3f} {2:8.3f} {3:8.3f} M={4:8.3f}".format(sum.Px(),sum.Py(),sum.Pz(),sum.E(),sum.M()))

outputFile = TFile("testPhotCon.root","recreate")
outputFile.cd()
hAngle.Write() 
outputFile.Close()
