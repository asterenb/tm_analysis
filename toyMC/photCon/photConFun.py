# program to generate muon conversions
# code is based on algorithm and physics equations 
# defined in the GEANT Physics Manual.

# Original by Zeyu (Steven) Wang
# Adaptation to callable function, but Dan Marlow

import ROOT
import sys, time
import numpy as np
import math
from array import array
from math import sqrt, sin, cos, log, acos, atan2
import random

#1/137
alpha		= 7.297e-3
#muon mass, electron mass (GeV)
m_mu, m_e       = 0.1057, 0.0005110

def photConFun(gamma_4,element) :
    E_gamma = gamma_4.E() 
    atomic_mass = {"H":1.007, "Be":9.012, "Si":28.0855, "Cu":63.5, "Pb":207.2}
    atomic_number = { "H":1., "Be":4., "Si":14., "Cu": 29., "Pb":82. }
    try :
        A = atomic_mass[element]
        Z = atomic_number[element]
    except IndexError :
        print("ERROR:  Element {0:s} not supported.  Exiting.")
        exit()

    D_n, B          = 1.54*(A**0.27) if (Z!=1) else 1.49, 183. if (Z!=1) else 202.4
    x_min, x_max    = 1./2-sqrt(1./4-m_mu/E_gamma), 1./2+sqrt(1./4-m_mu/E_gamma)

    W_infty         = B*Z**(-1./3)/D_n * m_mu/m_e
    delta_max       = m_mu**2 / (2*E_gamma*0.5*0.5)
    W_max           = W_infty * (1+(D_n*sqrt(math.e)-2)*delta_max/m_mu) / (1+B*Z**(-1./3)*sqrt(math.e)*delta_max/m_e)
    
    while True :
        R = np.random.rand(8)
         ## ------- Step 1 -------
        x_plus      = x_min + R[0]*(x_max-x_min)
        x_minus     = 1-x_plus
        delta       = m_mu**2 / (2*E_gamma*x_plus*x_minus)
        W           = W_infty * (1+(D_n*sqrt(math.e)-2)*delta/m_mu) / (1+B*Z**(-1./3)*sqrt(math.e)*delta/m_e)
        if ( (1-(4./3)*x_plus*x_minus)*log(W)/log(W_max) < R[1] ) : continue

        ## ------- Step 2 -------
        C1          = (0.35*A**0.27)**2 / (x_plus*x_minus*E_gamma/m_mu)
        f1_max      = (1-x_plus*x_minus) / (1+C1)
        t           = R[2]
        if ( (1-2*x_plus*x_minus+4*x_plus*x_minus*t*(1-t))/(1+C1/t/t)/f1_max < R[3] ): continue

        ## ------- Step 3 -------
        f2_max      = 1-2*x_plus*x_minus*(1-4*t*(1-t))
        psi         = R[4] * 2*math.pi
        if ( (1-2*x_plus*x_minus+4*x_plus*x_minus*t*(1-t)*(1+cos(2*psi)))/f2_max < R[5] ) : continue

        ## ------- Step 4 -------
        rho_max_sq  = 1.9/(A**0.27) * (1./t-1)
        C2          = 4/sqrt(x_plus*x_minus) * ((m_mu/(2*E_gamma*x_plus*x_minus*t))**2 + (m_e/(183.*Z**(-1./3)*m_mu))**2)**2
        beta        = log((C2+rho_max_sq**2)/C2)
        rho         = (C2*(math.exp(beta*R[6])-1))**(0.25)

        ## ------- Step 5 -------
        gamma_plus, gamma_minus = x_plus*E_gamma/m_mu, x_minus*E_gamma/m_mu
        u           = sqrt(1./t-1)
        theta_plus, theta_minus = 1./gamma_plus*(u+rho/2*cos(psi)), 1./gamma_minus*(u-rho/2*cos(psi))
        phi         = rho/u*sin(psi)
        if (theta_plus > math.pi or theta_minus > math.pi): continue

        break 
        
    ## ------- Vectors -------
    muon1, muon2        = ROOT.TLorentzVector(), ROOT.TLorentzVector()
    p_plus, p_minus     = sqrt((x_plus*E_gamma)**2-m_mu**2), sqrt((x_minus*E_gamma)**2-m_mu**2)
    phi0        = R[7] * 2*math.pi
    muon1.SetXYZM(p_plus*sin(theta_plus)*cos(phi0+phi/2)       , p_plus*sin(theta_plus)*sin(phi0+phi/2)    , p_plus*cos(theta_plus),   m_mu)
    muon2.SetXYZM(-p_minus*sin(theta_minus)*cos(phi0-phi/2)    , -p_minus*sin(theta_minus)*sin(phi0-phi/2) , p_minus*cos(theta_minus), m_mu)

    # rotate muons into direction of gamma_4 
    theta = acos(gamma_4.Pz()/gamma_4.P())
    phi = atan2(gamma_4.Px(),gamma_4.Py()) 
    muon1.RotateX(theta)
    muon1.RotateZ(phi)
    muon1.SetPy(-muon1.Py())
    muon2.RotateX(theta)
    muon2.RotateZ(phi)
    muon2.SetPy(-muon2.Py())
    return muon1, muon2 

# calculate the probability that a photon converts in a cylindrical shell
# determine the path length by using the direction of the photon
#  
def conversionProbability(gamma_4,element) :

    E_gamma = gamma_4.E()
    atomic_mass = {"H":1.007, "Be":9.012, "Si":28.0855, "Cu":63.5, "Pb":207.2}
    atomic_number = { "H":1., "Be":4., "Si":14., "Cu": 29., "Pb":82. }
    density = { "H":0.071, "Be":1.848, "Si":2.329, "Cu": 8.96, "Pb":11.35 }
 
    N_A = 6.022e23

    try :
        A = atomic_mass[element]
        Z = atomic_number[element]
        number_density = N_A*(density[element]/A)
    except IndexError :
        print("ERROR:  Element {0:s} not supported.  Exiting.")
        exit()

    D_n     = 1.54*(A**0.27) if (Z!=1) else 1.49
    B 		= 183. if (Z!=1) else 202.4
    sqrt_e 	= math.sqrt(math.e)
    t, s 	= 1.479 + 0.00799*D_n, -0.88

    E_c 		= -18+4347./(B*Z**(-1./3))	# (GeV)
    r_c 		= 1.362e-15    #  (classical radius of the muon in cm)

    W_sat 	= B*Z**(-1./3.)*4*sqrt_e*m_mu**2/m_e

    W_M 		= 1/(4*D_n*sqrt_e*m_mu)
    C_f 		= 1+0.04*math.log(1+E_c/E_gamma)
    E_g 		= (1-4*m_mu/E_gamma)**t * (W_sat**s+E_gamma**s)**(1/s)

    sigma		= 28./9. * alpha * Z**2 * r_c**2 * math.log(1+W_M*C_f*E_g)					# in cm**2

    # use the value of sigma, the density, and the psuedorapidity (eta)
    # to calculate the conversion probability, which can be viewed as 
    # an event weight.
  
    # for now we make the simple assumption of a cylindrical shell

    shellThickness = 0.03  # 300 um
    thickness = shellThickness/sin(gamma_4.Theta())
    prob		= sigma*number_density*thickness
    #print("thickness={0:f} sigma={1:e} number_density={2:e} prob={3:e}".format(thickness,sigma,number_density,prob))
    return prob

#photConFun, but for electrons instead of muons!
#  WRONG: using heavy lepton formula but with electron mass instead!
def photConFun_elec(gamma_4,element) :
    E_gamma = gamma_4.E() 
    atomic_mass = {"H":1.007, "Be":9.012, "Si":28.0855, "Cu":63.5, "Pb":207.2}
    atomic_number = { "H":1., "Be":4., "Si":14., "Cu": 29., "Pb":82. }
    try :
        A = atomic_mass[element]
        Z = atomic_number[element]
    except IndexError :
        print("ERROR:  Element {0:s} not supported.  Exiting.")
        exit()

    D_n, B          = 1.54*(A**0.27) if (Z!=1) else 1.49, 183. if (Z!=1) else 202.4
    x_min, x_max    = 1./2-sqrt(1./4-m_e/E_gamma), 1./2+sqrt(1./4-m_e/E_gamma)

    W_infty         = B*Z**(-1./3)/D_n * m_e/m_e
    delta_max       = m_e**2 / (2*E_gamma*0.5*0.5)
    W_max           = W_infty * (1+(D_n*sqrt(math.e)-2)*delta_max/m_e) / (1+B*Z**(-1./3)*sqrt(math.e)*delta_max/m_e)
    
    while True :
        R = np.random.rand(8)
         ## ------- Step 1 -------
        x_plus      = x_min + R[0]*(x_max-x_min)
        x_minus     = 1-x_plus
        delta       = m_e**2 / (2*E_gamma*x_plus*x_minus)
        W           = W_infty * (1+(D_n*sqrt(math.e)-2)*delta/m_e) / (1+B*Z**(-1./3)*sqrt(math.e)*delta/m_e)
        if ( (1-(4./3)*x_plus*x_minus)*log(W)/log(W_max) < R[1] ) : continue

        ## ------- Step 2 -------
        C1          = (0.35*A**0.27)**2 / (x_plus*x_minus*E_gamma/m_e)
        f1_max      = (1-x_plus*x_minus) / (1+C1)
        t           = R[2]
        if ( (1-2*x_plus*x_minus+4*x_plus*x_minus*t*(1-t))/(1+C1/t/t)/f1_max < R[3] ): continue

        ## ------- Step 3 -------
        f2_max      = 1-2*x_plus*x_minus*(1-4*t*(1-t))
        psi         = R[4] * 2*math.pi
        if ( (1-2*x_plus*x_minus+4*x_plus*x_minus*t*(1-t)*(1+cos(2*psi)))/f2_max < R[5] ) : continue

        ## ------- Step 4 -------
        rho_max_sq  = 1.9/(A**0.27) * (1./t-1)
        C2          = 4/sqrt(x_plus*x_minus) * ((m_e/(2*E_gamma*x_plus*x_minus*t))**2 + (m_e/(183.*Z**(-1./3)*m_e))**2)**2
        beta        = log((C2+rho_max_sq**2)/C2)
        rho         = (C2*(math.exp(beta*R[6])-1))**(0.25)

        ## ------- Step 5 -------
        gamma_plus, gamma_minus = x_plus*E_gamma/m_e, x_minus*E_gamma/m_e
        u           = sqrt(1./t-1)
        theta_plus, theta_minus = 1./gamma_plus*(u+rho/2*cos(psi)), 1./gamma_minus*(u-rho/2*cos(psi))
        phi         = rho/u*sin(psi)
        if (theta_plus > math.pi or theta_minus > math.pi): continue

        break 
        
    ## ------- Vectors -------
    elec1, elec2        = ROOT.TLorentzVector(), ROOT.TLorentzVector()
    p_plus, p_minus     = sqrt((x_plus*E_gamma)**2-m_e**2), sqrt((x_minus*E_gamma)**2-m_e**2)
    phi0        = R[7] * 2*math.pi
    elec1.SetXYZM(p_plus*sin(theta_plus)*cos(phi0+phi/2)       , p_plus*sin(theta_plus)*sin(phi0+phi/2)    , p_plus*cos(theta_plus),   m_e)
    elec2.SetXYZM(-p_minus*sin(theta_minus)*cos(phi0-phi/2)    , -p_minus*sin(theta_minus)*sin(phi0-phi/2) , p_minus*cos(theta_minus), m_e)

    # rotate elecs into direction of gamma_4 
    theta = acos(gamma_4.Pz()/gamma_4.P())
    phi = atan2(gamma_4.Px(),gamma_4.Py()) 
    elec1.RotateX(theta)
    elec1.RotateZ(phi)
    elec1.SetPy(-elec1.Py())
    elec2.RotateX(theta)
    elec2.RotateZ(phi)
    elec2.SetPy(-elec2.Py())
    return elec1, elec2 

# calculate the probability that a photon converts in a cylindrical shell
# determine the path length by using the direction of the photon
#  WRONG: using heavy lepton formula but with electron mass instead!
#  
def conversionProbability_elec(gamma_4,element) :

    E_gamma = gamma_4.E()
    atomic_mass = {"H":1.007, "Be":9.012, "Si":28.0855, "Cu":63.5, "Pb":207.2}
    atomic_number = { "H":1., "Be":4., "Si":14., "Cu": 29., "Pb":82. }
    density = { "H":0.071, "Be":1.848, "Si":2.329, "Cu": 8.96, "Pb":11.35 }
 
    N_A = 6.022e23

    try :
        A = atomic_mass[element]
        Z = atomic_number[element]
        number_density = N_A*(density[element]/A)
    except IndexError :
        print("ERROR:  Element {0:s} not supported.  Exiting.")
        exit()

    D_n     = 1.54*(A**0.27) if (Z!=1) else 1.49
    B 		= 183. if (Z!=1) else 202.4
    sqrt_e 	= math.sqrt(math.e)
    t, s 	= 1.479 + 0.00799*D_n, -0.88

    E_c 		= -18+4347./(B*Z**(-1./3))	# (GeV)
    r_c 		= 2.8179e-13  #  (classical radius of the electron in cm)

    W_sat 	= B*Z**(-1./3.)*4*sqrt_e*m_e**2/m_e

    W_M 		= 1/(4*D_n*sqrt_e*m_e)
    C_f 		= 1+0.04*math.log(1+E_c/E_gamma)
    E_g 		= (1-4*m_e/E_gamma)**t * (W_sat**s+E_gamma**s)**(1/s)

    sigma		= 28./9. * alpha * Z**2 * r_c**2 * math.log(1+W_M*C_f*E_g)					# in cm**2

    # use the value of sigma, the density, and the psuedorapidity (eta)
    # to calculate the conversion probability, which can be viewed as 
    # an event weight.
  
    # for now we make the simple assumption of a cylindrical shell

    shellThickness = 0.03  # 300 um
    thickness = shellThickness/sin(gamma_4.Theta())
    prob		= sigma*number_density*thickness
    #print("thickness={0:f} sigma={1:e} number_density={2:e} prob={3:e}".format(thickness,sigma,number_density,prob))
    return prob

#4-vectors for ee pair that converted from a photon
# using correct model for electrons (not heavy leptons)
def photConFun_ee(gamma_4,element) :
    E_gamma = gamma_4.E() 
    atomic_mass = {"H":1.007, "Be":9.012, "Si":28.0855, "Cu":63.5, "Pb":207.2}
    atomic_number = { "H":1., "Be":4., "Si":14., "Cu": 29., "Pb":82. }
    try :
        A = atomic_mass[element]
        Z = atomic_number[element]
    except IndexError :
        print("ERROR:  Element {0:s} not supported.  Exiting.")
        exit()

    epsilon0 = m_e / E_gamma
    #need special treatment for energy less than 2 MeV
    if E_gamma > .002:
        def delta(epsilon):
            return 136.0 / Z**(1.0/3) * epsilon0/(epsilon*(1-epsilon)) 
        deltamin = delta(0.5)
        F = 8.0/3 * log(Z)
        #for Egamma higher than 50 MeV, need to add additional correction
        if E_gamma > .05:
            F += 8*(alpha*Z)**2 *(1.0/(1+(alpha*Z)**2) + 0.20206 - 0.0369*(alpha*Z)**2 + 0.0083*(alpha*Z)**4 - 0.0020*(alpha*Z)**6) 
        deltamax = math.exp( (42.24 - F)/8.368 ) - 0.952
        def Phi1(dlt):
            if dlt <= 1:
                return 20.867 - 3.242*dlt + 0.625*dlt**2
            else:
                return 21.12 - 4.184*log(dlt + 0.952) 
        def Phi2(dlt):
            if dlt <= 1:
                return 20.209 - 1.930*dlt - 0.086*dlt**2
            else:
                return Phi1(dlt)
        def F1(delt):
            return 3*Phi1(delt) - Phi2(delt) - F 
        def F2(delt):
            return 1.5*Phi1(delt) - 0.5*Phi2(delt) - F
        #print("deltamin: %f, deltamax: %f, epsilon0: %f"%(deltamin, deltamax, epsilon0))
        epsilon1 = 0.5 - 0.5*sqrt( 1 - deltamin/deltamax)
        emin = max(epsilon0, epsilon1)
        F10 = F1(deltamin)
        F20 = F2(deltamin)
        N1 = (.5 - emin)**2 * F10
        N2 = 1.5 * F20
        #sample the energy
        engy_frac = 0
        #First, get the energy of the pair.
        while True:
            ra, rb, rc = random.random(), random.random(), random.random()
            #print("random numbers: {}, {}, {}".format(ra, rb, rc))
            if ra < N1 / (N1 + N2):
                engy_frac = .5 - (.5 - emin)*rb**(1.0/3)
                greject = F1(delta(engy_frac)) / F10
            else:
                engy_frac = emin + (.5 - emin)*rb 
                greject = F2(delta(engy_frac)) / F20
            #reject if greject < rc, ie accept if >=.
            if greject >= rc: break
            #print("energy fraction REJECTED: %f "%engy_frac)
    else:
        engy_frac = epsilon0 + (0.5 - epsilon0)*random.random()
    
    #print("energy fraction sampled: %f"%engy_frac)
    energy0 = engy_frac * E_gamma
    energy1 = E_gamma - energy0
    #randomly assign charge
    if random.random() > .5:
        energy0, energy1 = energy1, energy0
    #charge of each electron is irrelevant.
    #Finally, get the polar angle.
    a = 5.0/8
    d = 27.0
    #get a triplet of random numbers r1, r2, r3 to generate the random theta
    r1, r2, r3 = random.random(), random.random(), random.random()
    #print("random numbers: {}".format(r1, r2, r3))
    if r1 < 9. / (9. + d):
        u = -1*log(r2*r3)/a
    else:
        u = -1*log(r2*r3)/(3*a)
    #print("u: {}".format(u)) 
    theta_plus = m_e / energy0 * u 
    theta_minus = m_e / energy1 * u

    #print("theta+: {}, theta-: {}".format(theta_plus, theta_minus)) 
    ## ------- Vectors -------
    elec1, elec2        = ROOT.TLorentzVector(), ROOT.TLorentzVector()
    p_plus, p_minus     = sqrt(energy0**2 - m_e**2), sqrt(energy1**2 - m_e**2)

    #print("p+: {}, p-: {}".format(p_plus, p_minus)) 
    
    phi        = random.random() * 2*math.pi
    #print("px:")
    #print(p_plus*sin(theta_plus)*cos(phi))
    elec1.SetXYZM(p_plus*sin(theta_plus)*cos(phi)       , p_plus*sin(theta_plus)*sin(phi)    , p_plus*cos(theta_plus),   m_e)
    elec2.SetXYZM(-p_minus*sin(theta_minus)*cos(phi)    , -p_minus*sin(theta_minus)*sin(phi) , p_minus*cos(theta_minus), m_e)

    # rotate elecs into direction of gamma_4 
    theta = acos(gamma_4.Pz()/gamma_4.P())
    phi = atan2(gamma_4.Px(),gamma_4.Py()) 
    elec1.RotateX(theta)
    elec1.RotateZ(phi)
    elec1.SetPy(-elec1.Py())
    elec2.RotateX(theta)
    elec2.RotateZ(phi)
    elec2.SetPy(-elec2.Py())
    return elec1, elec2 

# calculate the probability that a photon converts in a cylindrical shell
# determine the path length by using the direction of the photon
#  Using correct electron treatment!
#   from GEANT user manual:
# https://geant4-userdoc.web.cern.ch/UsersGuides/PhysicsReferenceManual/html/electromagnetic/gamma_incident/gammaconversion/conv.html
#  
def conversionProbability_ee(gamma_4,element) :

    E_gamma = gamma_4.E()
    atomic_mass = {"H":1.007, "Be":9.012, "Si":28.0855, "Cu":63.5, "Pb":207.2}
    atomic_number = { "H":1., "Be":4., "Si":14., "Cu": 29., "Pb":82. }
    density = { "H":0.071, "Be":1.848, "Si":2.329, "Cu": 8.96, "Pb":11.35 }
 
    N_A = 6.022e23

    try :
        A = atomic_mass[element]
        Z = atomic_number[element]
        number_density = N_A*(density[element]/A)
    except IndexError :
        print("ERROR:  Element {0:s} not supported.  Exiting.")
        exit()

    D_n     = 1.54*(A**0.27) if (Z!=1) else 1.49
    B 		= 183. if (Z!=1) else 202.4
    sqrt_e 	= math.sqrt(math.e)
    t, s 	= 1.479 + 0.00799*D_n, -0.88

    E_c 		= -18+4347./(B*Z**(-1./3))	# (GeV)
    r_c 		= 2.8179e-13  #  (classical radius of the electron in cm)

   # W_sat 	= B*Z**(-1./3.)*4*sqrt_e*m_e**2/m_e

   # W_M 		= 1/(4*D_n*sqrt_e*m_e)
   # C_f 		= 1+0.04*math.log(1+E_c/E_gamma)
   # E_g 		= (1-4*m_e/E_gamma)**t * (W_sat**s+E_gamma**s)**(1/s)

    #sigma		= 28./9. * alpha * Z**2 * r_c**2 * math.log(1+W_M*C_f*E_g)					# in cm**2
    X = log(E_gamma / m_e)
    #constant values from GEANT4 code: 
    # https://gitlab.cern.ch/geant4/geant4/-/blob/master/source/processes/electromagnetic/standard/src/G4BetheHeitlerModel.cc#L118-137
    microbarn = 1.e-6 * 1.e-24 #in sq cm 
    a0 =  8.7842e+2 * microbarn
    a1 = -1.9625e+3 * microbarn
    a2 =  1.2949e+3 * microbarn
    a3 = -2.0028e+2 * microbarn
    a4 =  1.2575e+1 * microbarn
    a5 = -2.8333e-1 * microbarn
                               
    b0 = -1.0342e+1 * microbarn
    b1 =  1.7692e+1 * microbarn
    b2 = -8.2381    * microbarn
    b3 =  1.3063    * microbarn
    b4 = -9.0815e-2 * microbarn
    b5 =  2.3586e-3 * microbarn
  
    c0 = -4.5263e+2 * microbarn
    c1 =  1.1161e+3 * microbarn
    c2 = -8.6749e+2 * microbarn
    c3 =  2.1773e+2 * microbarn
    c4 = -2.0467e+1 * microbarn
    c5 =  6.5372e-1 * microbarn

    F_1 = a0 + a1*X + a2*X**2 + a3*X**3 + a4*X**4 + a5*X**5
    F_2 = b0 + b1*X + b2*X**2 + b3*X**3 + b4*X**4 + b5*X**5
    F_3 = c0 + c1*X + c2*X**2 + c3*X**3 + c4*X**4 + c5*X**5

    #this formula has accuracy of at worst 5%
    sigma = Z*(Z + 1) * ( F_1 + F_2*Z + F_3/Z )
    #if Egamma is less than 1.5 MeV, need to adjust
    E_low = .0015
    if E_gamma < E_low:
        sigma *= ((E_gamma - 2*m_e) / (E_low - 2*m_e))**2
    # use the value of sigma, the density, and the psuedorapidity (eta)
    # to calculate the conversion probability, which can be viewed as 
    # an event weight.
  
    # for now we make the simple assumption of a cylindrical shell

    shellThickness = 0.03  # 300 um = .03 cm
    thickness = shellThickness/sin(gamma_4.Theta())
    prob		= sigma*number_density*thickness
    #print("thickness={0:f} sigma={1:e} number_density={2:e} prob={3:e}".format(thickness,sigma,number_density,prob))
    return prob
