from typing import OrderedDict
import ROOT as rt
import numpy as np
import csv, pprint

n_mass_bins = 50

f = rt.TFile.Open("../data_in/twoMuon_data.root")
t = f.Get("Events")

# Create bins
pt_edges = np.arange(30)
pt_edges = np.append(pt_edges, np.arange(30,40,2))
pt_edges = np.append(pt_edges, np.arange(40,55,5))
pt_edges = np.append(pt_edges, np.arange(55,70,15))
pt_edges = np.append(pt_edges, np.arange(70,101,30))

pt_centers = [(pt_edges[i]+pt_edges[i+1])/2 for i in range(len(pt_edges)-1)]

# Draw the 2D pT vs mass plot into the histogram
h_mass_pt = rt.TH2D("h_mass_pt", "h_mass_pt", n_mass_bins, 0.5, 0.6, len(pt_edges)-1, pt_edges.astype(float))
t.Draw("p_t:mass>>h_mass_pt", "mass>0.1 && nVtx>0 && nMuons==2 && nGood>1", "COLZ GOFF")

# Declare observable mass
mass = rt.RooRealVar("mass", "M_{#mu#mu} [GeV]", 0.52, 0.57)

# Create two Gaussian PDFs g1(x,mean1,sigma) anf g2(x,mean2,sigma)
mean = rt.RooRealVar("mean", "mean of gaussians", 0.5478)
sigma1 = rt.RooRealVar("sigma1", "width of gaussians", 0.01)
sigma2 = rt.RooRealVar("sigma2", "width of gaussians", 0.005)
sig1 = rt.RooGaussian("sig1", "Signal component 1", mass, mean, sigma1)
sig2 = rt.RooGaussian("sig2", "Signal component 2", mass, mean, sigma2)

# alpha = rt.RooRealVar("alpha", "alpha", 1, 0.5, 10)
# n = rt.RooRealVar("n", "n", 1, 0.5, 10)
# sig = rt.RooCBShape("sig", "Cystal Ball Function", mass, mean, sigma1, alpha, n)

# gamma = rt.RooRealVar("gamma", "gamma", 2, -5, 5)
# delta = rt.RooRealVar("delta", "delta", 2, -5, 5)
# sig = rt.RooJohnson("sig", "Johnson distribution", mass, mean, sigma1, gamma, delta)

# Build Chebychev pdf for lower pT
a0 = rt.RooRealVar("a0", "a0", 0.5, 0., 1.)
a1 = rt.RooRealVar("a1", "a1", 0.2, 0., 1.)
# a2 = rt.RooRealVar("a2", "a2", 0.1, 0., 1.)
# RooChebychev bkgHigh("bkgHigh", "Background for high-pT", mass, RooArgSet(a0, a1, a2))
bkg = rt.RooChebychev("bkg", "Background", mass, rt.RooArgSet(a0, a1))

# Build polynomial pdf for higher pT
p0 = rt.RooRealVar("p0", "p0", 5500, 1e3, 1e7)
p1 = rt.RooRealVar("p1", "p1", -1000., -1e4, 0.0)
# p2 = rt.RooRealVar("p2", "p2", 0.01, -1e2, 1e2)
bkgHigh = rt.RooPolynomial("bkgHigh", "Background for high-pT", mass, rt.RooArgSet(p0, p1))

# Sum the signal components into a composite signal pdf
sig1frac = rt.RooRealVar("sig1frac", "fraction of component 1 in signal", 0.8, 0., 1.)
sig = rt.RooAddPdf("sig", "Signal", rt.RooArgList(sig1, sig2), sig1frac)
# nsig1 = rt.RooRealVar("nsig1", "number of component 1 in signal", 1000, 0., 10000.)
# nsig2 = rt.RooRealVar("nsig2", "number of component 2 in signal", 1000, 0., 10000.)
# sig = rt.RooAddPdf("sig", "Signal", rt.RooArgList(sig1, sig2), rt.RooArgList(nsig1, nsig2))

# Sum the composite signal and background
# RooRealVar bkgfrac("bkgfrac", "fraction of background", 0.9, 0., 1.)
# RooAddPdf model("model", "(g1+g2)+a", RooArgList(bkg, sig), bkgfrac)

# Sum the composite signal and background into an extended pdf nsig*sig+nbkg*bkg
nsig = rt.RooRealVar("nsig", "number of signal events", 1e5, 0., 1e9)
nbkg = rt.RooRealVar("nbkg", "number of background events", 1e7, 0, 1e9)
nbkgHigh = rt.RooRealVar("nbkgHigh", "number of background events at high-pT", 1e7, 0, 1e9)
model = rt.RooAddPdf("model", "(g1+g2)+a", rt.RooArgList(bkg, sig), rt.RooArgList(nbkg, nsig))
modelHigh = rt.RooAddPdf("modelHigh", "(g1+g2)+a", rt.RooArgList(bkgHigh, sig), rt.RooArgList(nbkgHigh, nsig))

# Open file to store all the fits
f_out = rt.TFile.Open("../data_out/fits.root", "RECREATE")

# For each pT bin, fit the invMass and add the yield and errors to the histos
tot_yields = 0.
yields, errors = {}, {}
for i in range(1, h_mass_pt.GetNbinsY()+1):
    h_projX = h_mass_pt.ProjectionX(f"{i}", i, i)
    massframe = mass.frame(rt.RooFit.Title("Two gaussian signal peaks plus Chebychev 2nd or 3rd order background"))
    dh = rt.RooDataHist("dh", "dh", mass, rt.RooFit.Import(h_projX))
    dh.plotOn(massframe, rt.RooFit.DrawOption("E"), rt.RooFit.LineColor(rt.kRed), rt.RooFit.MarkerSize(0.3), rt.RooFit.MarkerColor(rt.kRed))
    if h_mass_pt.GetYaxis().GetBinCenter(i) < 5:
        yld, error = 0., 0.
        yields[i] = yld
        errors[i] = error
        continue
    elif h_mass_pt.GetYaxis().GetBinCenter(i) > 30:
        modelHigh.fitTo(dh, rt.RooFit.PrintLevel(-1))
        modelHigh.plotOn(massframe)
    else:
        model.fitTo(dh, rt.RooFit.PrintLevel(-1))
        model.plotOn(massframe)
    
    # model.Print("t")
    # massframe.Draw()
    f_out.mkdir(f"bin{i}")
    f_out.cd(f"bin{i}")
    w = rt.RooWorkspace("w","workspace")
    if h_mass_pt.GetYaxis().GetBinCenter(i) > 30:
        w.Import(modelHigh)
    else:
        w.Import(model)
    w.Import(dh)
    w.Write()
    massframe.Write()

    # Extract the signal yield and error from the fit
    yld = nsig.getVal()
    yields[i] = yld
    tot_yields += yld
    error = nsig.getError()
    errors[i] = error

f_out.Close()

f_acc_nominal = rt.TFile.Open("../data_out/acceptance_EtaTo2Mu.root")
# g_acc = f_acc_nominal.Get("acceptance_EtaTo2Mu")
acc_fit_nominal = f_acc_nominal.Get("accFit")

f_acc_plus = rt.TFile.Open("../data_out/acceptance_EtaTo2Mu_plus10percent.root")
# g_acc = f_acc_nominal.Get("acceptance_EtaTo2Mu")
acc_fit_plus = f_acc_plus.Get("accFit")

all_data = []
for i, pt in enumerate(pt_centers):
    i_bin = i + 1
    acc = acc_fit_nominal.Eval(pt)
    acc_error = abs(acc_fit_plus.Eval(pt) - acc)
    d = OrderedDict({
        'bin': i_bin, 
        'pt_center': pt,
        'pt_low': pt_edges[i],
        'pt_high': pt_edges[i+1],
        'A_2mu': acc,
        'A_2mu_error': acc_error,
        'N_2mu': yields[i_bin],
        'N_2mu_error': errors[i_bin]
    })
    all_data.append(d)
    if acc < 0.:
        continue

# f_out2 = rt.TFile.Open("../data_out/corrected_xsec.root", "RECREATE")
# g_corrXsec.Write()
# f_out2.Close()

# Write out information in csv format rather than histos
# (Use histo facility in utils for the histograms)
with open('../data_out/two_mu_yields.csv', 'w') as f:
    writer = csv.DictWriter(f, fieldnames=all_data[0].keys())
    writer.writeheader()
    writer.writerows(all_data)

pprint.pprint(all_data)
print("total signal yield: ", tot_yields)