#! /bin/bash

# Check linux first, then Mac OS gnu grealpath
if realpath $(dirname -- ${BASH_SOURCE}) > /dev/null; then
	SCRIPTPATH="$(realpath $(dirname -- ${BASH_SOURCE}))"
elif grealpath $(dirname -- ${BASH_SOURCE}) > /dev/null; then
	SCRIPTPATH="$(grealpath $(dirname -- ${BASH_SOURCE}))"
else
	echo "Warning! Could not determine absolute path of repo! Setting relative path..."
	SCRIPTPATH="$(dirname -- ${BASH_SOURCE}))"
fi

export ANALYSIS_BASE_DIR=$SCRIPTPATH

export PYTHONPATH=$PYTHONPATH:$ANALYSIS_BASE_DIR/python

if root-config --version > /dev/null ; then
	echo "Identified ROOT version: `root-config --version`"
else
	echo "ROOT not found! Please make sure it is sourced."
fi
