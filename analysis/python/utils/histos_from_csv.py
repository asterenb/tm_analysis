import ROOT as rt
import csv
import numpy as np
import math
import utils.blinding as blinding
import utils.configuration as configuration
cfg = configuration.Configuration()

def read_csv(filename):
    header, data = [], []
    with open(filename) as f:
        reader = csv.reader(f)
        for i, row in enumerate(reader):
            if i == 0:
                header = row
            else:
                data.append(row)
    return data, header

def write_csv(filename, data, header=None):
    if isinstance(data[0], list):
        with open(filename, 'w') as f:
            writer = csv.writer(f)
            writer.writerow(header)
            for row in data:
                writer.writerow(row)
    elif isinstance(data[0], dict):
        with open(filename, 'w') as f:
            writer = csv.DictWriter(f, fieldnames=data[0].keys())
            writer.writeheader()
            writer.writerows(data)

def read_two_mu_csv():
    return read_csv(cfg.fns['two_mu_csv'])

def read_four_mu_csv():
    return read_csv(cfg.fns['four_mu_csv'])

def read_two_mu_acc():
    return read_csv(cfg.fns['two_mu_acc_csv'])

def read_four_mu_acc():
    f = rt.TFile.Open(cfg.fns['four_mu_acc'])
    g = f.Get("acceptance_EtaTo4Mu")
    return g

def get_uncorrected_xsec():
    B_2mu, B_2mu_err = 5.8e-6, 0.8e-6
    lumi, lumi_err = 101.3, 101.3 * 0.03
    # two_mu_data, _ = read_two_mu_csv()
    two_mu_data, _ = read_csv(cfg.fns['two_mu_csv_with_syst'])
    nBins = len(two_mu_data)
    edges, widths = np.array([]), np.array([])
    N_2mu, N_2mu_err = np.array([]), np.array([])
    N_2mu_syst_err = np.array([])
    for row in two_mu_data:
        edges = np.append(edges, float(row[2]))
        widths = np.append(widths, float(row[3]) - float(row[2]))
        N_2mu = np.append(N_2mu, float(row[4]))
        N_2mu_err = np.append(N_2mu_err, float(row[5]))
        if len(row) > 6:
            N_2mu_syst_err = np.append(N_2mu_syst_err, float(row[6]))
        else:
            N_2mu_syst_err = np.append(N_2mu_syst_err, 0.)
    edges = np.append(edges, float(row[3]))
    histo = rt.TH1D("uncorr_xsec", "Uncorrected cross-section", nBins, edges.astype(float))
    for i in range(1, nBins+1):
        if N_2mu[i-1] > 0.:
            dPt = widths[i-1]
            content = N_2mu[i-1] / (B_2mu * lumi * dPt)
            vals = [N_2mu[i-1], N_2mu[i-1], B_2mu, lumi]
            errs = [N_2mu_syst_err[i-1], N_2mu_err[i-1], B_2mu_err, lumi_err]
            error = sum(k*k/(j*j) for k,j in zip(errs, vals))
            error = math.sqrt(error)
        else:
            content = 0.
            error = 0.
        histo.SetBinContent(i, content)
        histo.SetBinError(i, error * content)
    return histo

def get_corrected_xsec():
    B_2mu, B_2mu_err = 5.8e-6, 0.8e-6
    lumi, lumi_err = 101.3, 101.3 * 0.03
    # two_mu_data, _ = read_two_mu_csv()
    two_mu_data, _ = read_csv(cfg.fns['two_mu_csv_with_syst'])
    two_mu_acc, _ = read_two_mu_acc()
    nBins = len(two_mu_data)
    edges, widths = np.array([]), np.array([])
    N_2mu, N_2mu_err = np.array([]), np.array([])
    N_2mu_syst_err = np.array([])
    A_2mu, A_2mu_err = np.array([]), np.array([])
    for row0, row1 in zip(two_mu_data, two_mu_acc):
        edges = np.append(edges, float(row0[2]))
        widths = np.append(widths, float(row0[3]) - float(row0[2]))
        N_2mu = np.append(N_2mu, float(row0[4]))
        N_2mu_err = np.append(N_2mu_err, float(row0[5]))
        if len(row0) > 6:
            N_2mu_syst_err = np.append(N_2mu_syst_err, float(row0[6]))
        else:
            N_2mu_syst_err = np.append(N_2mu_syst_err, 0.)
        A_2mu = np.append(A_2mu, float(row1[4]))
        A_2mu_err = np.append(A_2mu_err, float(row1[5]))
    edges = np.append(edges, float(row0[3]))
    histo = rt.TH1D("corr_xsec", "Acceptance-corrected cross-section", nBins, edges.astype(float))
    for i in range(1, nBins+1):
        if N_2mu[i-1] > 0. and A_2mu[i-1] > 0.:
            dPt = widths[i-1]
            content = N_2mu[i-1] / (A_2mu[i-1] * B_2mu * lumi * dPt)
            vals = [N_2mu[i-1], N_2mu[i-1], A_2mu[i-1], B_2mu, lumi]
            errs = [N_2mu_syst_err[i-1], N_2mu_err[i-1], A_2mu_err[i-1], B_2mu_err, lumi_err]
            error = sum(k*k/(j*j) for k,j in zip(errs, vals))
            error = math.sqrt(error)
        else:
            content = 0.
            error = 0.
        histo.SetBinContent(i, content)
        histo.SetBinError(i, content * error)
    return histo

def get_ALICE_uncorrected_xsec():
    histo = get_uncorrected_xsec()
    for i in range(1, histo.GetNbinsX()+1):
        pT = histo.GetBinCenter(i)
        ALICE_factor = 0.001 / (2 * 3.14 * 4.8 * pT)
        histo.SetBinContent(i, histo.GetBinContent(i) * ALICE_factor)
        histo.SetBinError(i, histo.GetBinError(i) * ALICE_factor)
    return histo

def get_ALICE_corrected_xsec():
    histo = get_corrected_xsec()
    for i in range(1, histo.GetNbinsX()+1):
        pT = histo.GetBinCenter(i)
        ALICE_factor = 0.001 / (2 * 3.14 * 4.8 * pT)
        histo.SetBinContent(i, histo.GetBinContent(i) * ALICE_factor)
        histo.SetBinError(i, histo.GetBinError(i) * ALICE_factor)
    return histo

def get_4mu_predictions(include_acc=False, bin_edges=[]):
    if include_acc is True:
        acc_str = 'after'
        g_A_4mu = read_four_mu_acc()
    else:
        acc_str = 'before'
    h_corr_xsec = get_corrected_xsec()
    lumi = 101.3 # 1/fb
    multipliers = [0.5, 1.0, 2.0]
    labels = [f'{x} #times #bar{{B}}' for x in multipliers]
    BRs = [x * blinding.get_blinded_BR() for x in multipliers]
    h_N_4mu_MC_BRs = []
    for i, BR in enumerate(BRs):
        if len(bin_edges) == 0:
            bin_edges = np.linspace(0, 100, 11)
        h = rt.TH1D(f'h_N_4mu_MC_{acc_str}_{i}', '', len(bin_edges)-1, bin_edges)
        h.SetTitle(f'Exp. #eta #rightarrow 4#mu yield {acc_str} acc. ({labels[i]})')
        for bin in range(1, h.GetNbinsX()+1):
            xsec_bin = h_corr_xsec.FindBin(h.GetBinCenter(bin))
            content = h_corr_xsec.GetBinContent(xsec_bin) * h.GetBinWidth(bin)
            error = h_corr_xsec.GetBinError(xsec_bin) * h.GetBinWidth(bin)
            if include_acc is True:
                acc = g_A_4mu.Eval(h.GetBinCenter(bin))
                content *= acc
                error *= acc
            h.SetBinContent(bin, content)
            h.SetBinError(bin, error)
        h.Scale(BR * lumi)
        h_N_4mu_MC_BRs.append(h)
    return h_N_4mu_MC_BRs


def test_unit():
    uncorr_xsec = get_uncorrected_xsec()
    corr_xsec = get_corrected_xsec()
    c1 = rt.TCanvas()
    rt.SetOwnership(uncorr_xsec, False)
    rt.SetOwnership(corr_xsec, False)
    rt.SetOwnership(c1, False)
    corr_xsec.Draw()
    uncorr_xsec.Draw("SAME")
    c1.Update()

    uncorr_ALICE_xsec = get_ALICE_uncorrected_xsec()
    corr_ALICE_xsec = get_ALICE_corrected_xsec()
    c2 = rt.TCanvas()
    rt.SetOwnership(uncorr_ALICE_xsec, False)
    rt.SetOwnership(corr_ALICE_xsec, False)
    rt.SetOwnership(c2, False)
    corr_ALICE_xsec.Draw()
    uncorr_ALICE_xsec.Draw("SAME")
    c2.Update()

if __name__ == "__main__":
    test_unit()