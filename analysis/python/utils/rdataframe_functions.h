#include "ROOT/RDataFrame.hxx"
#include "ROOT/RVec.hxx"
#include "TCanvas.h"
#include "TH1D.h"
#include "TLatex.h"
#include "Math/Vector4D.h"
#include "TStyle.h"
 
using namespace ROOT::VecOps;
using rvec_f = const RVec<float>;
using rvec_i = const RVec<int>;

rvec_f compute_M1_M2_min(rvec_f pt, rvec_f eta, rvec_f phi, rvec_f charge) {
    auto idx_comb = Combinations(pt, 2);
    auto min_idx1 = -1, min_idx2 = -1;
    float M1 = 1000.0;
    for (auto i = 0; i < idx_comb[0].size(); i++) {
        const auto i1 = idx_comb[0][i];
        const auto i2 = idx_comb[1][i];
        if (charge[i1] != charge[i2]) {
            ROOT::Math::PtEtaPhiMVector p1(pt[i1], eta[i1], phi[i1], 0.1056);
            ROOT::Math::PtEtaPhiMVector p2(pt[i2], eta[i2], phi[i2], 0.1056);
            float this_mass = (p1 + p2).M();
            if (this_mass < M1) {
                M1 = this_mass;
                min_idx1 = i1;
                min_idx2 = i2;
            }
        }
    }
    int other_idx1[4][4] = {
        { -1, 2, 1, 1 },
        { 2, -1, 0, 0 },
        { 1, 0, -1, 0 },
        { 1, 0, 0, -1 }
    };
    int other_idx2[4][4] = {
        { -1, 3, 3, 2 },
        { 3, -1, 3, 2 },
        { 3, 3, -1, 1 },
        { 2, 2, 1, -1 }
    };
    auto i3 = other_idx1[min_idx1][min_idx2];
    auto i4 = other_idx2[min_idx1][min_idx2];
    ROOT::Math::PtEtaPhiMVector p3(pt[i3], eta[i3], phi[i3], 0.1056);
    ROOT::Math::PtEtaPhiMVector p4(pt[i4], eta[i4], phi[i4], 0.1056);
    float M2 = (p3 + p4).M();
    rvec_f result = {M1, M2};
    return result;
}

rvec_f compute_M1_M2_max(rvec_f pt, rvec_f eta, rvec_f phi, rvec_f charge) {
    auto idx_comb = Combinations(pt, 2);
    auto max_idx1 = -1, max_idx2 = -1;
    float M1 = 0.0;
    for (auto i = 0; i < idx_comb[0].size(); i++) {
        const auto i1 = idx_comb[0][i];
        const auto i2 = idx_comb[1][i];
        if (charge[i1] != charge[i2]) {
            ROOT::Math::PtEtaPhiMVector p1(pt[i1], eta[i1], phi[i1], 0.1056);
            ROOT::Math::PtEtaPhiMVector p2(pt[i2], eta[i2], phi[i2], 0.1056);
            float this_mass = (p1 + p2).M();
            if (this_mass > M1) {
                M1 = this_mass;
                max_idx1 = i1;
                max_idx2 = i2;
            }
        }
    }
    int other_idx1[4][4] = {
        { -1, 2, 1, 1 },
        { 2, -1, 0, 0 },
        { 1, 0, -1, 0 },
        { 1, 0, 0, -1 }
    };
    int other_idx2[4][4] = {
        { -1, 3, 3, 2 },
        { 3, -1, 3, 2 },
        { 3, 3, -1, 1 },
        { 2, 2, 1, -1 }
    };
    auto i3 = other_idx1[max_idx1][max_idx2];
    auto i4 = other_idx2[max_idx1][max_idx2];
    ROOT::Math::PtEtaPhiMVector p3(pt[i3], eta[i3], phi[i3], 0.1056);
    ROOT::Math::PtEtaPhiMVector p4(pt[i4], eta[i4], phi[i4], 0.1056);
    float M2 = (p3 + p4).M();
    rvec_f result = {M1, M2};
    return result;
}