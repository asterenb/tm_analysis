import urllib.request

def get_blind_coeff(mumuee=False):
    url = 'https://asterenb.web.cern.ch/asterenb/EtaTo4Mu/'
    blind_c1, blind_c2 = None, None
    c1 = "c1_2mu2e.txt" if mumuee else "c1.txt"
    c2 = "c2_2mu2e.txt" if mumuee else "c2.txt"
    print("Using %s, %s for blinding"%(c1, c2)) 
    for line in urllib.request.urlopen(url + c1):
        blind_c1 = float(line)
    for line in urllib.request.urlopen(url + c2):
        blind_c2 = float(line)
    if blind_c1 is None or blind_c2 is None:
        print("Error fetching blinding coefficients from the internet!")
        print("Returning 0.0 to avoid accidental unblinding..")
        return 0.0
    return blind_c1 * blind_c2


def get_blind_coeffs():
    url = 'https://asterenb.web.cern.ch/asterenb/EtaTo4Mu/'
    blind_c1, blind_c2 = 1.0, 1.0
    for line in urllib.request.urlopen(url + 'c1.txt'):
        blind_c1 = float(line)
    for line in urllib.request.urlopen(url + 'c2.txt'):
        blind_c2 = float(line)
    if blind_c1 is None or blind_c2 is None:
        print("Error fetching blinding coefficients from the internet!")
        print("Returning 0.0 to avoid accidental unblinding..")
        return 0.0
    return blind_c1, blind_c2

def get_blinded_BR():
    return 4e-9 * get_blind_coeff()

def get_blinded_BR_EtaTo2Mu2E():
    return 2.4e-6 * get_blind_coeff(True)
    #return 2.4e-6 * get_blind_coeff(False)
