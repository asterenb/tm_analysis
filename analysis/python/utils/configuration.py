from collections import namedtuple
from unicodedata import name
import numpy as np
import utils.fit_function_library as lib
import os

class TwoMuFitParams:
    def __init__(self):
        self.sig_model = {
            # '7': lib.CB_p(0.54646076, 0.0076024167, -3.2836237, 16.656078),
            # '8': CB(0.5465, 0.005, -0.01, 10),
            # '9': CB(0.5471, 0.007, -1.14, 49.9),
            # '12': TwoGauss(0.54727148, 0.011148402, 0.0054840053, 0.37594806),
            # '38': Voigt(5.4580e-01,1.0250e-03,1.0835e-02),
            'all': lib.DoubleGauss_p(0.54709530, 0.0082174922, 0.0044861213, 0.77462665)
            # 'all': lib.CB_p(0.54646076, 0.0076024167, -3.2836237, 16.656078)
        }
        self.bkg_model = {
            # '38': Cheb3(-4.7856e-03, 7.5439e-03, -4.8834e-03, -8.2942e-03),
            '7': lib.Cheb3_p(-0.01917, -0.00448, -0.000099),
            'all': lib.Cheb3_p(-0.01917, -0.00448, -0.000099)
            # 'all': lib.Cheb2_p(0.0098572910, 0.00021053080)
        }
        
class Configuration:
    def __init__(self):
        try:
            self.base_dir = os.environ['ANALYSIS_BASE_DIR']
        except KeyError:
            print("$ANALYSIS_BASE_DIR not found! Please source env.sh first.")
            raise

        # Filenames/paths
        self.fns = {
            'four_mu_ntuple_data': 'data_in/fourMuon_data_v4.root',
            'four_mu_ntuple_MC': 'data_in/fourMuon_MC_v2.root',
            'two_mu_ntuple_data': 'data_in/twoMuon_data.root',
            'two_mu_ntuple_MC': 'data_in/twoMuon_MC_v2.root',
            'two_mu_csv': 'data_out/two_mu_yields.csv',
            'two_mu_csv_with_syst': 'data_out/two_mu_yields_errors.csv',
            'four_mu_csv': 'data_out/four_mu_acc.csv',
            'two_mu_acc': 'data_out/acceptance_EtaTo2Mu.root',
            'two_mu_acc_csv': 'data_out/two_mu_acc.csv',
            'four_mu_acc': 'data_out/acceptance_EtaTo4Mu.root',
            'two_mu_fits': 'data_out/fits.root',
            'fit_variations_p': 'data_out/fit_variations',
            'fit_variations_f': 'data_out/fit_variations/fits_AltSigBkg.root',
            'xsecs': 'data_out/xsecs.root',
            'gen_weights': 'data_out/gen_weights.root',
            'm2mu_vs_pt2mu': 'data_out/m2mu_vs_pt2mu.root',
            'fit_masses': 'data_out/fit_masses.root',
            'fit_systs': {
                'Voigtian_Cheb4': 'data_out/fit_variations/fits_Voigtian_Cheb4.csv',
                'DoubleGauss_Cheb4': 'data_out/fit_variations/fits_DoubleGauss_Cheb4.csv',
                'Voigtian_Cheb3': 'data_out/fit_variations/fits_Voigtian_Cheb3.csv',
                'Voigtian_Cheb2': 'data_out/fit_variations/fits_Voigtian_Cheb2.csv'
            }
        }
        for k, v in self.fns.items():
            if isinstance(v, dict):
                for k2, v2 in v.items():
                    self.fns[k][k2] = '/'.join([self.base_dir, v2])
            else:
                self.fns[k] = '/'.join([self.base_dir, v])

        # Fit params
        self.two_mu_fit_params = TwoMuFitParams()
        self.low_high_pt_split = 30.0

        # pT and mass histogram params
        self.pt_edges = np.arange(0.0, self.low_high_pt_split, 1.0)
        self.pt_edges = np.append(self.pt_edges, np.arange(self.low_high_pt_split,40.0,2.0))
        self.pt_edges = np.append(self.pt_edges, np.arange(40.0,55.0,5.0))
        self.pt_edges = np.append(self.pt_edges, np.arange(55.0,70.0,15.0))
        self.pt_edges = np.append(self.pt_edges, np.arange(70.0,101.0,30.0))

        self.n_mass_bins = 100
