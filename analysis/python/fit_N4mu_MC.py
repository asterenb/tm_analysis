import ROOT as rt
import utils.CMSStyle as CMSStyle
from utils.fitter import fitter_4mu as fitter
import utils.configuration as configuration
cfg = configuration.Configuration()

CMSStyle.setTDRStyle()

f = rt.TFile.Open(cfg.fns['four_mu_ntuple_MC'])
tt = f.Get("Events")
h = rt.TH1D("h", "h", 70, 0.492, 0.608)
tt.Draw("mass>>h", "mass<0.6 && mass>0.5 && nGood>3 && nVtx>0 && nMuons==4 && trigFired==1", "HIST GOFF")
h.Rebin(2)

# Construct observable
mass = rt.RooRealVar("m_{4#mu}","Reconstructed m_{4#mu} [GeV]", 0.51, 0.59) 

# Construct fit model
sig_model = 'CB_Gauss'
fit = fitter(mass=mass, bkg_model='', sig_model=sig_model)

data = rt.RooDataHist("data", "data", mass, rt.RooFit.Import(h))

res = fit.model.fitTo(data, rt.RooFit.Save())

frame = mass.frame()
frame.SetAxisRange(0, 2e3, "Y")
data.plotOn(frame, Name="Data")
fit.model.plotOn(frame, Name="signal", LineColor=rt.kCyan)

# Draw frame on canvas
c1 = rt.TCanvas()
# gPad.SetLeftMargin(0.15)
frame.GetYaxis().SetTitleOffset(1.5)
frame.GetXaxis().SetNdivisions(510)
frame.Draw()

leg = rt.TLegend(0.58, 0.7, 0.88, 0.9)
leg.AddEntry("signal", f"{sig_model} fit", "l")
leg.AddEntry("Data", "Simulated events", "lep")
leg.Draw()

# Print CMS logo and lumi
CMSStyle.setCMSLumiStyle(c1, 10, era='scouting')

c1.RedrawAxis()
c1.Update()

res.Print("v")
print(frame.chiSquare(res.floatParsFinal().getSize()))