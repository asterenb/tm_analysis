from ROOT import gPad, TCanvas, TFile, TH1D, TGraphErrors, TLegend, TF1, TLatex, TText
from ROOT import kBlack, kBlue, kGreen, kRed, kDashed, kDashDotted
import tdrstyle, CMS_lumi 
from array import array 

tdrstyle.setTDRStyle()

f = TFile.Open("./fourMuon_v4.root")

t = f.Get("Events")

h = TH1D("hFourMuons", "hFourMuons", 100, 0.46, 0.9)
h.SetMarkerColor(kBlack)
h.SetMarkerStyle(20)
h.SetMarkerSize(0.6)
h.SetLineColor(kBlack)

H_ref, W_ref = 600, 800 
W, H = W_ref, H_ref

# references for T, B, L, R
T, B, L, R = 0.08*H_ref, 0.12*H_ref, 0.12*W_ref, 0.04*W_ref

c = TCanvas("c","c",50,50,W,H)
c.SetLeftMargin( L/W )
c.SetRightMargin( R/W )
c.SetTopMargin( T/H )
c.SetBottomMargin( B/H )
c.SetTicks()
frame = c.DrawFrame(0.5, 0, 0.9, 80)
frame.GetYaxis().SetTitle("Events")
frame.GetXaxis().SetTitle("m_{4#mu} [GeV]")

t.Project("hFourMuons","mass", "mass < 1.0 && nGood > 3 && abs(charge_4mu) < 0.5")

h.Draw("PE SAME") 


fit = TF1("fit", "pol2(0)+landau(3)", 0.5, 0.9)
fit.SetParameters(49.8, -193.4, 192.1, 154.5, 0.548, 0.00024)
fit.SetLineColor(kBlue)
fit.SetLineWidth(3)

fit_sig = TF1("fit_sig", "landau", 0.54, 0.57)
fit_sig.SetLineColor(kGreen+1)
fit_sig.SetLineStyle(kDashDotted)
fit_sig.SetLineWidth(3)

fit_bkg = TF1("fit_bkg", "pol2", 0.5, 0.9)
fit_bkg.SetLineColor(kRed)
fit_bkg.SetLineWidth(3)
fit_bkg.SetLineStyle(kDashed)

h.Fit(fit, "R")
fit.Draw("SAME")
h.Fit(fit_sig, "R")
fit_sig.Draw("SAME")
h.Fit(fit_bkg, "R")
fit_bkg.Draw("SAME")

integral_bkg = fit_bkg.Integral(0.53, 0.57)/h.GetBinWidth(10)
integral_sig = fit_sig.Integral(0.53, 0.57)/h.GetBinWidth(10)
integral = fit.Integral(0.53, 0.57)/h.GetBinWidth(10)
total = h.Integral(h.FindBin(0.53), h.FindBin(0.57))

leg = TLegend(0.2, 0.54, 0.95, 0.84)
leg.SetBorderSize(0)
leg.SetLineWidth(0)
leg.AddEntry(h, "Scouting dataset: 101.3 fb^{-1} (2017+2018)", "lep")
leg.AddEntry(fit_sig, "Landau fit", "l")
txt1 = "Quadratic fit: m_{4#mu}#in [0.53,0.57]GeV: N=" + "{0:.1f}".format(integral_bkg)
leg.AddEntry(fit_bkg, txt1, "l")
txt2 = "Quad.+Landau: m_{4#mu}#in [0.53, 0.57]GeV: N=" + "{0:.1f}".format(integral)
leg.AddEntry(fit, txt2, "l")
leg.AddEntry("", "Tot. obs. signal peak [0.53, 0.57]GeV: N = {0:d}".format(int(total)), "")
leg.Draw()

#draw the lumi text on the canvas
CMS_lumi.writeExtraText = 0
CMS_lumi.extraText = "Preliminary"
CMS_lumi.lumi_13TeV = "101 fb^{-1}"
CMS_lumi.lumi_sqrtS = "13 TeV"
iPeriod, iPos = 4, 11
#CMS_lumi.CMS_lumi(c, iPeriod, iPos)

if False :
    latex = TLatex()
    latex.SetNDC()
    latex.SetTextAngle(0)
    latex.SetTextColor(kBlack)    
    latex.SetTextFont(42)
    #latex.SetTextAlign(31) 
    #latex.SetTextSize(lumiTextSize*t)  
    latex.SetTextSize(1.0) 
    #latex.DrawLatex(1-r,1-t+lumiTextOffset*t,lumiText)
    t = c.GetTopMargin()
    r = c.GetRightMargin()
    lumiTextOffset   = 0.2
    latex.DrawLatex(1-r,1-t+lumiTextOffset*t,"CMS Preliminary")
    #latex.DrawLatex(0.5,0.5,"CMS Preliminary")

txt1 = TLatex(0.51,81.,'CMS')
txt1.Draw() 
txt2 = TLatex(0.55,81.,'Preliminary')
txt2.SetTextFont(52)
txt2.Draw() 
txt3 = TLatex(0.75,81.,'2017+2018 101.3 fb^{-1}')
txt3.SetTextFont(52)
txt3.Draw() 

c.Update()

raw_input()

#CMS_lumi.CMS_lumi(c, 3, 10, "101 fb^{-1} (13 TeV, 2017+2018)")

print("Integral quadratic fit under peak [0.53, 0.57] GeV: {0:f} ".format(integral_bkg))
print("Integral landau fit under peak [0.53, 0.57] GeV: {0:f}".format(integral_sig))
print("Integral quadratic+landau fit under peak [0.53, 0.57] GeV: {0:f}".format(integral)) 
print("Total observed under peak [0.53, 0.57] GeV: {0:f}".format(total))
