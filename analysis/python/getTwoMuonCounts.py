from ROOT import TFile, TF1, TTree, TH1D, TH2D, TLorentzVector, TCanvas, TLegend, TGraphErrors
from ROOT import kBlue, kRed, kFALSE
from array import array
from math import sqrt, exp 
import numpy as np
import utils.configuration as configuration
config = configuration.Configuration()

def getN_2mu() :
    inFile1 = TFile.Open(config.corrected_xsec)
    h_xsec = inFile1.Get("h_corrXsec")

    inFile2 = TFile.Open(config.two_mu_acc)
    g_acc = inFile2.Get("acceptance_EtaTo2Mu")

    eps = 1.e-4
    lumi, B_2mu = 101., 5.8e-6    # lumi in fb^-1 
    p_t,  N_2mu = array('d'), array('d')
    for i in range(5,h_xsec.GetNbinsX()) :
        low = h_xsec.GetBinLowEdge(i+1)
        high = low + h_xsec.GetBinWidth(i+1)
        accPoints =[]
        for j in range(g_acc.GetN()) :
            x = g_acc.GetPointX(j) 
            if x > low-eps and x < high+eps : 
                accPoints.append(g_acc.GetPointY(j))
        if len(accPoints) < 1 : continue 
        acc = np.mean(np.array(accPoints))
        xSec = h_xsec.GetBinContent(i)
        p_t.append(0.5*(low+high))
        N_2mu.append(lumi*B_2mu*acc*xSec)

        #print("i={0:d} low={1:.1f} high={2:.1f} points={3:s} average={4:f}".format(i,low,high,str(accPoints),acc))
        #print("i={0:d} low={1:.1f} xSec={2:.2e} N={3:.2e}".format(i,low,xSec,Nmumu))
    inFile1.Close()
    inFile2.Close()
    return p_t, N_2mu 

# execution begins here

p_t, N_2mu = getN_2mu()



