import ROOT as rt
import utils.blinding
import utils.histos_from_csv as csv_utils
import utils.CMSStyle as CMSStyle
import utils.configuration as configuration
cfg = configuration.Configuration()

B_2mu = 5.8e-6 # uncertainty: 0.8e-6
N_4mu = 55.0 # TODO read directly from fit
# N_2mu, _ = csv_utils.read_csv(config.two_mu_csv_with_syst)
N_2mu, _ = csv_utils.read_csv(cfg.fns['two_mu_csv'])
A_2mu, _ = csv_utils.read_csv(cfg.fns['two_mu_acc_csv'])
A_4mu, _ = csv_utils.read_csv(cfg.fns['four_mu_csv'])

multipliers = [0.5, 1.0, 2.0]
labels = [f'{x} #times #bar{{B}}' for x in multipliers]
BRs = [x * utils.blinding.get_blinded_BR() for x in multipliers]

denominator = 0.0
for line0, line1, line2 in zip(N_2mu, A_2mu, A_4mu):
    N_2mu_i = float(line0[4]) # - float(line0[6]) # for fit syst variations
    A_2mu_i = float(line1[4])
    A_4mu_i = float(line2[4])
    if A_2mu_i > 0.:
        denominator += N_2mu_i * A_4mu_i / A_2mu_i

obs_B_4mu = utils.blinding.get_blind_coeff() * N_4mu * B_2mu / denominator

print(f"Measured B_4mu (blinded): {obs_B_4mu}")