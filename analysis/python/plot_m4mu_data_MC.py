from audioop import mul
import ROOT as rt
import utils.CMSStyle as CMSStyle
import utils.configuration as configuration
cfg = configuration.Configuration()

CMSStyle.setTDRStyle()

minX = 0.46+0.00275
maxX = 0.9+0.00275
upperXRange = 0.9+0.00275
upperYRange = 100
nBins = 80
# 80, 0.46+0.00275, 0.9+0.00275

# Load the data and MC events
f_data = rt.TFile.Open(cfg.fns['four_mu_ntuple_data'])
t_data = f_data.Get("Events")

f_MC = rt.TFile.Open(cfg.fns['four_mu_ntuple_MC'])
t_MC = f_MC.Get("Events")

f_wgts = rt.TFile.Open(cfg.fns['gen_weights'])
t_wgts = f_wgts.Get("weights_tt")
t_MC.AddFriend(t_wgts, "weights_tt")

# String to select events (note the different variable name for mass)
selection_data = "mass > 0.46275 && mass < 0.90275 && nGood > 3 && charge_4mu==0"
selection_MC = "mass > 0.46275 && mass < 0.90275 && nGood > 3 && charge_4mu==0"

# Create histo and draw data into it from tree
h_data = rt.TH1D("h_data", "h_data", nBins, minX, maxX)
t_data.Draw("mass>>h_data", selection_data, "GOFF")

# Make histos for the different BR predictions
multipliers = [1.0] #[0.5, 1.0, 2.0]
labels = [f'{x} #times #bar{{B}}' for x in multipliers]
h_MCs = []
for i in range(len(multipliers)):
    h = rt.TH1D(f"h_MC{i}", f"h_MC{i}", nBins, minX, maxX)
    h_MCs.append(h)
    t_MC.Draw(f"mass>>h_MC{i}", f"(weight*{multipliers[i]})*({selection_MC})", "GOFF")

# Make canvas
c1 = rt.TCanvas()
c1.SetTicks()

# Draw frame
frame = c1.DrawFrame(minX, 0, maxX, 60)
frame.GetXaxis().SetTitle("m_{4#mu} [GeV]")
frame.GetYaxis().SetTitle("Events / bin")

# Draw MC predictions
for i, h in enumerate(h_MCs):
    h.SetMarkerColor(rt.kOrange + i)
    h.SetLineColor(rt.kOrange + i)
    h.SetLineWidth(3)
    h.Draw("HIST SAME")
    # h.Draw("E0 SAME")

# Draw data
h_data.SetMarkerStyle(20)
h_data.SetMarkerSize(0.7)
h_data.SetMarkerColor(rt.kBlack)
h_data.SetLineColor(rt.kBlack)
h_data.Draw("EP E0 SAME")

# Make and draw legend
leg = rt.TLegend(0.38, 0.68, 0.73, 0.88)
leg.AddEntry(h_data, "Scouting dataset", "lep")
for i, h in enumerate(h_MCs):
    leg.AddEntry(h, f"Simulation ({labels[i]})", "l")
leg.AddEntry("", "#bar{B} #in [1.2, 12] #times 10^{-9}", "")
leg.Draw()

# Draw CMS logo and lumi
CMSStyle.setCMSLumiStyle(c1, 10, era='scouting')

# Redraw axis and update canvas
c1.RedrawAxis()
c1.Update()