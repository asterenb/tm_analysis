import ROOT as rt
from utils.CMSStyle import get_canvas_and_frame, setTDRStyle, setCMSLumiStyle
setTDRStyle()

header_path = 'utils/rdataframe_functions.h'
rt.gInterpreter.Declare(f'#include "{header_path}"')

f_data = rt.TFile.Open("../data_in/fourMuon_data_v4.root")
tt_data = f_data.Get("Events")
df_raw_data = rt.RDataFrame(tt_data)
df_data = df_raw_data.Filter("nMuons==4 && nGood>3 && nVtx>0 && charge_4mu==0 && mass<0.57 && mass>0.53")

df_data = df_data.Define("M1_M2_min", "compute_M1_M2_min(pt, eta, phi, charge)").\
    Define("M1_min", "M1_M2_min[0]").\
    Define("M2_min", "M1_M2_min[1]").\
    Define("M1_M2_max", "compute_M1_M2_max(pt, eta, phi, charge)").\
    Define("M1_max", "M1_M2_max[0]").\
    Define("M2_max", "M1_M2_max[1]")

c1, frame1 = get_canvas_and_frame(0.11, 0.11, 0.4, 0.4, "M_{1}^{min} [GeV]", "M_{2}^{min} [GeV]")
frame1.GetYaxis().SetTitleOffset(1.5)
h_2D_Min_data = df_data.Histo2D(("M1_M2_min", "M1_M2_min", 100, 0.11, 0.4, 100, 0.11, 0.4), "M1_min", "M2_min")
h_2D_Min_data.Draw("SAME")
c1.Update()

c2, frame2 = get_canvas_and_frame(0.11, 0.11, 0.4, 0.4, "M_{1}^{max} [GeV]", "M_{2}^{max} [GeV]")
frame1.GetYaxis().SetTitleOffset(1.5)
h_2D_Max_data = df_data.Histo2D(("M1_M2_max", "M1_M2_max", 100, 0.11, 0.4, 100, 0.11, 0.4), "M1_max", "M2_max")
h_2D_Max_data.Draw("SAME")
c2.Update()

f_MC = rt.TFile.Open("../data_in/fourMuon_MC_v2.root")
f_wgts = rt.TFile.Open("../data_out/gen_weights.root")
tt_MC = f_MC.Get("Events")
tt_MC.AddFriend("weights_tt", f_wgts)
df_raw_MC = rt.RDataFrame(tt_MC)
df_MC = df_raw_MC.Filter("nMuons==4 && nGood>3 && nVtx>0 && charge_4mu==0 && mass<0.57 && mass>0.53")

df_MC = df_MC.Define("M1_M2_min", "compute_M1_M2_min(pt, eta, phi, charge)").\
    Define("M1_min", "M1_M2_min[0]").\
    Define("M2_min", "M1_M2_min[1]").\
    Define("M1_M2_max", "compute_M1_M2_max(pt, eta, phi, charge)").\
    Define("M1_max", "M1_M2_max[0]").\
    Define("M2_max", "M1_M2_max[1]")

nBins = 8

c3, _ = get_canvas_and_frame(0.15, 0.1, 0.40, 100, "M_{i}^{min} [GeV]", "Events")
h_1D_M1_Min_data = df_data.Histo1D(("M1_min", "M1_min", 8, 0.15, 0.40), "M1_min")
h_1D_M1_Min_data.SetLineColor(rt.kBlue)
h_1D_M1_Min_data.SetMarkerColor(rt.kBlue)
h_1D_M1_Min_data.Draw("SAME EP E0")
h_1D_M2_Min_data = df_data.Histo1D(("M2_min", "M2_min", 8, 0.15, 0.40), "M2_min")
h_1D_M2_Min_data.SetLineColor(rt.kMagenta)
h_1D_M2_Min_data.SetMarkerColor(rt.kMagenta)
h_1D_M2_Min_data.Draw("SAME EP E0")
h_1D_M1_Min_MC = df_MC.Histo1D(("M1_min", "M1_min", 8, 0.15, 0.40), "M1_min", "weight")
h_1D_M1_Min_MC.SetLineWidth(2)
h_1D_M1_Min_MC.SetLineColor(rt.kBlue)
h_1D_M1_Min_MC.SetMarkerColor(rt.kBlue)
h_1D_M1_Min_MC.Draw("SAME HIST")
h_1D_M2_Min_MC = df_MC.Histo1D(("M2_min", "M2_min", 8, 0.15, 0.40), "M2_min", "weight")
h_1D_M2_Min_MC.SetLineWidth(2)
h_1D_M2_Min_MC.SetLineColor(rt.kMagenta)
h_1D_M2_Min_MC.SetMarkerColor(rt.kMagenta)
h_1D_M2_Min_MC.Draw("SAME HIST")

leg = rt.TLegend(0.5, 0.7, 0.85, 0.85)
leg.AddEntry(h_1D_M1_Min_data.GetName(), "M_{1}^{min} data", "lep")
leg.AddEntry(h_1D_M2_Min_data.GetName(), "M_{2}^{min} data", "lep")
leg.AddEntry(h_1D_M1_Min_MC.GetName(), "M_{1}^{min} signal MC", "l")
leg.AddEntry(h_1D_M2_Min_MC.GetName(), "M_{2}^{min} signal MC", "l")
leg.Draw()

c3.Update()

c4, _ = get_canvas_and_frame(0.15, 0.1, 0.40, 100, "M_{i}^{max} [GeV]", "Events")
h_1D_M1_Max_data = df_data.Histo1D(("M1_max", "M1_max", 8, 0.15, 0.40), "M1_max")
h_1D_M1_Max_data.SetLineColor(rt.kBlue)
h_1D_M1_Max_data.SetMarkerColor(rt.kBlue)
h_1D_M1_Max_data.Draw("SAME EP E0")
h_1D_M2_Max_data = df_data.Histo1D(("M2_max", "M2_max", 8, 0.15, 0.40), "M2_max")
h_1D_M2_Max_data.SetLineColor(rt.kMagenta)
h_1D_M2_Max_data.SetMarkerColor(rt.kMagenta)
h_1D_M2_Max_data.Draw("SAME EP E0")
h_1D_M1_Max_MC = df_MC.Histo1D(("M1_max", "M1_max", 8, 0.15, 0.40), "M1_max", "weight")
h_1D_M1_Max_MC.SetLineWidth(2)
h_1D_M1_Max_MC.SetLineColor(rt.kBlue)
h_1D_M1_Max_MC.SetMarkerColor(rt.kBlue)
h_1D_M1_Max_MC.Draw("SAME HIST")
h_1D_M2_Max_MC = df_MC.Histo1D(("M2_max", "M2_max", 8, 0.15, 0.40), "M2_max", "weight")
h_1D_M2_Max_MC.SetLineWidth(2)
h_1D_M2_Max_MC.SetLineColor(rt.kMagenta)
h_1D_M2_Max_MC.SetMarkerColor(rt.kMagenta)
h_1D_M2_Max_MC.Draw("SAME HIST")

leg = rt.TLegend(0.5, 0.7, 0.85, 0.85)
leg.AddEntry(h_1D_M1_Max_data.GetName(), "M_{1}^{max} data", "lep")
leg.AddEntry(h_1D_M2_Max_data.GetName(), "M_{2}^{max} data", "lep")
leg.AddEntry(h_1D_M1_Max_MC.GetName(), "M_{1}^{max} signal MC", "l")
leg.AddEntry(h_1D_M2_Max_MC.GetName(), "M_{2}^{max} signal MC", "l")
leg.Draw()

c4.Update()
