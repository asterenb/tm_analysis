import ROOT as rt
import utils.CMSStyle as CMSStyle
import utils.histos_from_csv as histos
import utils.configuration as configuration
cfg = configuration.Configuration()

CMSStyle.setTDRStyle()

# Retrieve different BR predictions from file
h_exp_BRs = histos.get_4mu_predictions(include_acc=True)
for i, h in enumerate(h_exp_BRs):
    h.SetLineColor(rt.kOrange + i - 1)
    h.SetLineWidth(3)
    n_exp = h.Integral()
    multipliers = [0.5, 1.0, 2.0]
    labels = [f'{x} #times #bar{{B}}' for x in multipliers]
    h.SetTitle(f"#eta #rightarrow 4#mu MC ({labels[i]}), N = {n_exp:.1f}")

f_obs = rt.TFile.Open(cfg.fns['four_mu_ntuple_data'])
tt = f_obs.Get("Events")

# Clone the predicted histogram then reset to get the same shape for data
h_obs = h_exp_BRs[0].Clone("h_obs")
h_obs.Reset()
h_obs.SetMarkerStyle(rt.kFullCircle)
h_obs.SetMarkerSize(0.8)
h_obs.SetLineColor(rt.kBlack)
h_obs.SetLineWidth(1)
h_obs.SetMarkerColor(rt.kBlack)
h_obs.SetStats(0)

h_sideband = h_obs.Clone("h_sideband")
h_sideband.Reset()
h_sideband.SetMarkerStyle(rt.kFullCircle)
h_sideband.SetMarkerSize(0.8)
h_sideband.SetLineColor(rt.kBlue+1)
h_sideband.SetLineWidth(3)
h_sideband.SetMarkerColor(rt.kBlue+1)
h_sideband.SetStats(0)

# Fill the histogram with the data (but don't draw yet)
sel_signal = "mass > 0.53 && mass < 0.57 && nGood > 3 && nVtx > 0 && charge_4mu == 0"
sel_sideband = "mass > 0.6 && mass < 0.9 && nGood > 3 && nVtx > 0 && charge_4mu == 0"
tt.Draw("pt_4mu>>h_obs", sel_signal, "GOFF")
tt.Draw("pt_4mu>>h_sideband", sel_sideband, "GOFF")

n_obs = h_obs.Integral()
h_obs.SetTitle(f"Data, N = {n_obs:n}")

# Normalize sideband to signal yield
h_sideband.Scale((n_obs-h_exp_BRs[1].Integral())/h_sideband.Integral())

# Make canvas
c1 = rt.TCanvas()
c1.SetTicks()

# Make frame
frame = c1.DrawFrame(0, 0, 80, 100)
frame.GetXaxis().SetTitle("p_{T}^{4#mu} [GeV]")
frame.GetYaxis().SetTitle("Events / bin")

# for h in h_exp_BRs:
#     for bin in range(1, h.GetNbinsX()+1):
#         h.SetBinContent(bin, h.GetBinContent(bin)*h.GetBinWidth(bin))

# Draw data and predictions
h_obs.Draw("SAME E")
h_sideband.Draw("SAME HIST")
for h in h_exp_BRs:
    # h.Draw("SAME")
    h.Draw("SAME HIST")

# Make and draw legend
leg = rt.TLegend(0.34, 0.5, 0.83, 0.82)
leg.SetHeader("N: m_{4#mu} #in [0.53, 0.57] GeV")
for h in h_exp_BRs:
    leg.AddEntry(h, h.GetTitle(), "l")
leg.AddEntry(h_sideband, "Bkg from sideband (1.0 #times #bar{B})", "l")
leg.AddEntry(h_obs, h_obs.GetTitle(), "lep")
leg.AddEntry("", "#bar{B} #in [1.2, 12] #times 10^{-9}", "")
leg.SetLineWidth(0)
leg.Draw()

# Print the total integral of the middle BR prediction and data
print(f"N exp BR0 {h_exp_BRs[0].Integral():.1f}")
print(f"N exp BR1 {h_exp_BRs[1].Integral():.1f}")
print(f"N exp BR2 {h_exp_BRs[2].Integral():.1f}")
print(f"N obs {h_obs.Integral():n}")

# Draw CMS logo and lumi
CMSStyle.setCMSLumiStyle(c1, 10, era='scouting')

# Redraw axis on top and update canvas
c1.RedrawAxis()
c1.Update()