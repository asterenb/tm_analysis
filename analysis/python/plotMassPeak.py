#! /usr/bin/env python
from ROOT import TFile, TH1D, TCanvas, gPad
import sys
from math import sqrt
import numpy as np

def getArgs() :
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("-v","--verbose",default=0,type=int,help="Print level.")
    parser.add_argument("-f","--inFileName",default='data1.root',help="File to be analyzed.")
    parser.add_argument("-m","--maxPrint",default=0,type=int,help="Maximum number of events to print.")
    return parser.parse_args()

def goodVertex(e,j) :
    sum, div = 0, 2**j
    for k in range(4) :
        if (e.vtxList[k]/div % 2 == 1) : sum += int(e.charge[k])   
    return sum == 0 
         
# begin execution here 

args = getArgs()
nPrint = args.maxPrint 


inFile = TFile(args.inFileName) 
inFile.cd()
t = inFile.Get("Events")
nentries = t.GetEntries()
print("nEntries={0:d}".format(nentries))

hMass = TH1D("hMass","hMass",80,0.4,0.8)
t.Project("hMass","mass","mass < 0.8 && nGood > 3")

# Draw the histograms
c1 = TCanvas("c1","Four Muon Mass Distribution",1200,700)

hMass.Draw()
hMass.GetXaxis().SetTitle("M_4mu (GeV)")
hMass.GetYaxis().SetTitle("Counts/0.005 GeV")
c1.Update()
print("Enter <CR> to continue") 
raw_input()



