from typing import OrderedDict
import ROOT as rt
import numpy as np
import csv, pprint
from utils.CMSStyle import get_canvas_and_frame, setTDRStyle, setCMSLumiStyle
from utils.fitter import fitter_2mu as fitter

setTDRStyle()

f = rt.TFile.Open("m2mu_by_pT.root")
h = f.Get("binAll/h_m2mu")
h.Rebin(2)

mass = rt.RooRealVar("mass", "m_{#mu#mu} [GeV]", 0.5, 0.6)

massframe = mass.frame(rt.RooFit.Title(f"Signal + background fit for all p_{{T}}"))

dh = rt.RooDataHist("dh", "dh", mass, rt.RooFit.Import(h))
dh.plotOn(massframe, rt.RooFit.DrawOption("E"), rt.RooFit.LineColor(rt.kRed), rt.RooFit.MarkerSize(0.3), rt.RooFit.MarkerColor(rt.kRed))

fit = fitter(mass, bkg_model='Cheb2', signal_model='TwoGauss')

result = fit.fit(dh, rt.RooFit.Save())

fit.model.plotOn(massframe)

c1 = rt.TCanvas()
massframe.Draw()
c1.Update()

f_MC = rt.TFile.Open("../data_in/twoMuon_MC.root")
t_MC = f_MC.Get("Events")
h_MC = rt.TH1D("h_MC", "h_MC", 50, 0.5, 0.6)
t_MC.Draw("mass>>h_MC", "mass>0.5 && mass<0.6 && mass>0.1 && nVtx>0 && nMuons==2 && nGood>1", "NORM GOFF")

c2, frame2 = get_canvas_and_frame(0.5, 0, 0.6, 0.1, "m_{#mu#mu} [GeV]", "A. U.")
massframe2 = mass.frame()
fit.sig.plotOn(massframe2, rt.RooFit.Name("fit"))
massframe2.Draw("SAME")
h_MC.Draw("HIST SAME")
c2.SetTicks()

leg = rt.TLegend(0.6, 0.72, 0.81, 0.9)
leg.SetBorderSize(0)
leg.AddEntry(h_MC, "MC", "l")
leg.AddEntry("fit", "Signal fit", "l")
leg.Draw()

c2.Update()

tot_yield = fit.nsig.getVal()

print(result.Print())
print(f"Total yield: {tot_yield}")