from ROOT import TFile, TTree, TH1D, TH2D, TLorentzVector, TCanvas, TLegend
from ROOT import kBlue, kRed, kFALSE
import numpy as np 
from math import sqrt 

def SWtrig(nMuons,muon,charge,threshold) :
    if nMuons < 1 : return False 
    nMuons = min(nMuons,4)
    for i in range(nMuons) :
        if muon[i].Pt() < threshold : continue 
        for j in range(i+1,nMuons) :
            if muon[j].Pt() < threshold : continue
            if charge[i]*charge[j] > 0. : continue
            DR = muon[i].DeltaR(muon[j])
            if DR > 1.2 : continue 
            return True
    return False 

inFile = TFile.Open("twoMuon_000.root")
inTree= inFile.Get("Events")
genTree = inFile.Get("GenEvents")
inTree.AddFriend(genTree)

nentries = inTree.GetEntries()
print("nentries={0:d}".format(nentries))

# make a single histogram 
fOut = TFile('twoMuon_hist.root', 'recreate' )

hmuonPt_all = TH1D('hmuonPt_all','hmuonPt_all',100, 0., 50.) 
hmuonPt_all.SetLineColor(kBlue)
hmuonPt_all.GetXaxis().SetTitle("muon p_{t} (GeV)")
hmuonPt_good = TH1D('hmuonPt_good','hmuonPt_good',100, 0., 50.) 
hmuonPt_good.SetLineColor(kRed)

hDR_all = TH1D('hDR_all','hDR_all',100, 0., 0.1) 
hDR_all.SetLineColor(kBlue)
hDR_all.GetXaxis().SetTitle("track #DeltaR")
hDR_good = TH1D('hDR_good','hDR_good',100, 0., 0.1) 
hDR_good.SetLineColor(kRed)

hPt_eta_all = TH1D('hPt_eta_all','hPt_eta_all',100, 0., 100.) 
hPt_eta_all.SetLineColor(kBlue)
hPt_eta_all.GetXaxis().SetTitle("#eta p_{t} (GeV)")
hPt_eta_good = TH1D('hPt_eta_good','hPt_eta_good',100, 0., 100.) 
hPt_eta_good.SetLineColor(kRed)

hTrig_all = TH1D('hTrig_all','hTrig_all',100, 0., 100.) 
hTrig_all.SetLineColor(kBlue)
hTrig_all.GetXaxis().SetTitle("#eta p_{t} (GeV)")
hTrig_good = TH1D('hTrig_good','hTrig_good',100, 0., 100.) 
hTrig_good.SetLineColor(kRed)

heta_eta_all = TH1D('heta_eta_all','heta_eta_all',100, -3., 3.) 
heta_eta_all.SetLineColor(kBlue)
heta_eta_all.GetXaxis().SetTitle("#eta pseudorapidity")
heta_eta_good = TH1D('heta_eta_good','heta_eta_good',100, -3., 3.) 
heta_eta_good.SetLineColor(kRed)

hnMuon = TH1D('hnMuon','hnMuon',10,0.,10.)
hnMuon.GetXaxis().SetTitle("Number of reconstructed muons")

hdPhi_all = TH1D("hdPhi_all","hdPhi_all",100,-0.1,0.1)
hdPhi_all.GetXaxis().SetTitle("#phi_{+} - #phi_{-}")
hdPhi_all.SetLineColor(kBlue)
hdPhi_good = TH1D("hdPhi_good","hdPhi_good",100,-0.1,0.1)
hdPhi_good.GetXaxis().SetTitle("#phi_{+} - #phi_{-}")
hdPhi_good.SetLineColor(kRed)

hTrigPt_all = TH1D('hTrigPt_all','hTrigPt_all',100,0.,40.)
hTrigPt_all.SetLineColor(kBlue)
hTrigPt_good = TH1D('hTrigPt_good','hTrigPt_good',100,0.,40.)
hTrigPt_good.SetLineColor(kRed)

hCompare = TH1D('hCompare','hCompare',4,0.,4.)

verbose = 0 
triggerThreshold = 5.0 
muon, muon_gen, charge, charge_gen = [], [], [], []
for i in range(4) : 
    muon.append(TLorentzVector())
    muon_gen.append(TLorentzVector())

printCount = 0 
for count, e in enumerate(inTree) :
    hnMuon.Fill(e.nMuons)
    good = e.nMuons > 1 
    #if e.nMuons == 0 : continue 

# fill the trigger histograms    
    pt_list = [e.pt_gen_1,e.pt_gen_2]
    if True :     # reco level 
        if e.nMuons > 0 : pt_list.append(e.pt_1)
        if e.nMuons > 1 : pt_list.append(e.pt_2) 
    if len(pt_list) > 1 : 
        pt_list.sort(reverse=True) 
        if pt_list[0] > 0. :
            hTrigPt_all.Fill(pt_list[1])
            if e.trigFired : hTrigPt_good.Fill(pt_list[1])
    
    #if not good : continue 
    muon[0].SetPtEtaPhiM(e.pt_1,e.eta_1,e.phi_1,0.107)
    muon[1].SetPtEtaPhiM(e.pt_2,e.eta_2,e.phi_2,0.107)
    charge.append(e.charge_1)
    charge.append(e.charge_2) 

    muon_gen[0].SetPtEtaPhiM(e.pt_gen_1,e.eta_gen_1,e.phi_gen_1,0.107)
    muon_gen[1].SetPtEtaPhiM(e.pt_gen_2,e.eta_gen_2,e.phi_gen_2,0.107)
    charge_gen.append(e.charge_gen_1)
    charge_gen.append(e.charge_gen_2)  

    goodSWtrig = SWtrig(e.nMuons,muon,charge,triggerThreshold) 
    if e.trigFired and e.nMuons > 1 :
        hTrig_all.Fill(e.pt_gen_eta)
        SW = 0.
        if goodSWtrig :
            hTrig_good.Fill(e.pt_gen_eta)
            SW = 2. 

    hCompare.Fill(e.trigFired+SW)
    dPhi = e.phi_gen_1 - e.phi_gen_2
    if e.charge_gen_1 < e.charge_gen_2 : dPhi *= -1. 
    if muon_gen[0].Pt() > 10. and muon_gen[1].Pt() > 10. :
        hdPhi_all.Fill(dPhi)
        if e.nMuons > 1 : hdPhi_good.Fill(dPhi)

    heta_eta_all.Fill(e.eta_gen_eta)
    if good : heta_eta_good.Fill(e.eta_gen_eta)

    hPt_eta_all.Fill(e.pt_gen_eta)
    if good : hPt_eta_good.Fill(e.pt_gen_eta)

    printFlag = False 
    for i in range(2) :
        dPmin = 9999.
        DRmin = 9999.
        for j in range(2):
            DR = muon_gen[i].DeltaR(muon[j])
            DRmin = min(DRmin,DR)
            dP = abs(muon_gen[i].Pt()-muon[j].Pt())/muon_gen[i].Pt()
            dPmin = min(dPmin,dP)

        goodTrack = DRmin < 0.005 and dPmin < 0.06
        hmuonPt_all.Fill(muon_gen[i].Pt())
        if goodTrack : hmuonPt_good.Fill(muon_gen[i].Pt())

        # calculate the DR between gen muons  
        DR = muon_gen[0].DeltaR(muon_gen[1])
        if muon_gen[0].Pt() > 10. and muon_gen[1].Pt() > 10. :
            hDR_all.Fill(DR)
            if goodTrack : hDR_good.Fill(DR)

    printFlag = not goodSWtrig   
    printFlag = (e.trigFired > 0 and not goodSWtrig) or (e.trigFired == 0 and goodSWtrig) 
    if printFlag and printCount < 20 :
        printCount += 1 
        print("\nGen Muons: count={0:d} nMuons={1:d} DR={2:f} dPhi={3:f} trigFired={4} SWtrig={5}".format(
            count,e.nMuons,DR,dPhi,e.trigFired,goodSWtrig))
        print("    pt      eta     phi  jBest    DR     dP")
        for i in range(2) :
            # get the track that matches  
            dPmin, DR, jBest = 99., 99., -1
            for j in range(2):
                DR = min(DR,muon_gen[i].DeltaR(muon[j]))
                if DR < 0.005 :
                    dP = abs(muon_gen[i].Pt()-muon[j].Pt())/muon_gen[i].Pt()
                    if dP < dPmin : 
                        jBest = j
                        dPmin = dP
                        DRmin = DR
            st0 = " +"
            if charge_gen[i] < 0. : st0 = ' -'
            st1 = ""
            if jBest > -1 : st1 = "{0:6d}{1:8.4f}{2:8.4f}".format(jBest,DRmin,dPmin)
            print("{0:2s}{1:7.2f}{2:8.4f}{3:8.4f}{4:s}".format(st0,muon_gen[i].Pt(),muon_gen[i].Eta(),muon_gen[i].Phi(),st1))
            
        print("Reco Muons nMuons={0:d}: \n    pt      eta     phi ".format(e.nMuons))
        for i in range(2) :
            st0 = " +"
            if charge[i] < 0. : st0 = ' -'
            print("{0:2s}{1:7.2f}{2:8.4f}{3:8.4f}".format(st0,muon[i].Pt(),muon[i].Eta(),muon[i].Phi()))


fOut.cd()
fOut.Write()
fOut.Close()        
