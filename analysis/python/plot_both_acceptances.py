import ROOT as rt
import utils.CMSStyle as CMSStyle
import utils.configuration as configuration
cfg = configuration.Configuration()
CMSStyle.setTDRStyle()

file_2mu = rt.TFile.Open(cfg.fns['two_mu_acc'])
acc_2mu = file_2mu.Get("acceptance_EtaTo2Mu_forPlots")

file_4mu = rt.TFile.Open(cfg.fns['four_mu_acc'])
acc_4mu = file_4mu.Get("acceptance_EtaTo4Mu_forPlots")

c1 = rt.TCanvas()
frame = c1.DrawFrame(0, 0, 70, 1.1)
frame.GetXaxis().SetTitle("#eta candidate p_{T} [GeV]")
frame.GetYaxis().SetTitle("Acceptance")
c1.SetTicks()

acc_2mu.SetMarkerColor(rt.kRed)
acc_2mu.SetLineColor(rt.kRed)
acc_2mu.Draw("SAME Z P")
acc_4mu.SetMarkerColor(rt.kBlue)
acc_4mu.SetLineColor(rt.kBlue)
acc_4mu.Draw("SAME Z P")

leg = rt.TLegend(0.46, 0.76, 0.69, 0.87)
leg.AddEntry(acc_2mu, "#eta #rightarrow 2#mu MC", "lep")
leg.AddEntry(acc_4mu, "#eta #rightarrow 4#mu MC", "lep")
leg.Draw()

# Print CMS logo and lumi
CMSStyle.setCMSLumiStyle(c1, 10, era='scouting')

c1.RedrawAxis()
c1.Update()