import ROOT as rt
import utils.CMSStyle as CMSStyle
import utils.histos_from_csv as histos
from uncertainties import ufloat
import utils.configuration as configuration
cfg = configuration.Configuration()


CMSStyle.setTDRStyle()

# First get the histos from the utils library
h_corr_xsec = histos.get_ALICE_corrected_xsec()
h_uncorr_xsec = histos.get_ALICE_uncorrected_xsec()

# Plot the corrected and uncorrected histos
c1 = rt.TCanvas()
c1.SetLogy()
c1.SetLogx()
c1.SetTicks()
frame = c1.DrawFrame(5, 1e-2, 99, 1e8)
frame.GetXaxis().SetMoreLogLabels()
frame.GetXaxis().SetTitle("p_{T}^{#mu#mu} [GeV]")
frame.GetXaxis().SetTitleOffset(1.4)
frame.GetYaxis().SetTitle("E #frac{d^{3}#sigma}{dp^{3}} [pb GeV^{-2} c^{3}]")
frame.GetYaxis().SetTitleOffset(1.6)

h_corr_xsec.SetLineWidth(2)
h_corr_xsec.SetMarkerColor(rt.kGreen-1)
h_corr_xsec.SetLineColor(rt.kGreen-1)
h_corr_xsec.SetMarkerStyle(rt.kFullCircle)
h_corr_xsec.Draw("SAME E")
h_uncorr_xsec.SetLineWidth(2)
h_uncorr_xsec.SetMarkerColor(rt.kPink-9)
h_uncorr_xsec.SetLineColor(rt.kPink-9)
h_uncorr_xsec.SetMarkerStyle(rt.kFullCircle)
h_uncorr_xsec.Draw("SAME E")

power_fit = rt.TF1("power_fit", "[0]*x^[1]", 6, 100)
power_fit.SetParameters(6.22e7, -5)
power_fit.SetParameter(1, -6)
h_corr_xsec.Fit(power_fit, "R")
fit_result0 = ufloat(power_fit.GetParameter(0), power_fit.GetParError(0))
fit_result1 = ufloat(power_fit.GetParameter(1), power_fit.GetParError(1))
power_fit.SetLineColor(rt.kRed+1)
power_fit.SetLineWidth(3)
power_fit.Draw("SAME")

TCM_fit = rt.TF1("TCM_fit", "[0]*exp(-sqrt(x^2 + 0.548^2)/[1])+[2]*(1+x^2/([3]^2*[4]))^(-[4])", 6, 100)
TCM_fit.FixParameter(0, 1.62e9)
TCM_fit.FixParameter(1, 0.229)
TCM_fit.FixParameter(2, 2.89e9)
TCM_fit.FixParameter(3, 0.810)
TCM_fit.FixParameter(4, 3.043)
TCM_fit.SetLineColor(rt.kOrange+1)
TCM_fit.SetLineWidth(3)
TCM_fit.Draw("SAME")

Tsallis_fit = rt.TF1("Tsallis_fit", "([0]/(2*3.14))*(([2]-1)*([2]-2)/([2]*[1]*([2]*[1] + 0.548*([2]-2))))*(1+(sqrt(0.548^2+x^2)-0.548)/([2]*[1]))^(-[2])", 6, 100)
Tsallis_fit.FixParameter(0, 1.56e10)
Tsallis_fit.FixParameter(1, 0.221)
Tsallis_fit.FixParameter(2, 6.560)
Tsallis_fit.SetLineColor(rt.kBlue)
Tsallis_fit.SetLineWidth(3)
Tsallis_fit.Draw("SAME")

leg = rt.TLegend(0.35, 0.63, 0.85, 0.87)
leg.AddEntry(h_corr_xsec, h_corr_xsec.GetTitle(), "lep")
leg.AddEntry(h_uncorr_xsec, h_uncorr_xsec.GetTitle(), "lep")
leg.AddEntry(Tsallis_fit, "Tsallis fit", "l")
leg.AddEntry(TCM_fit, "TCM fit", "l")
leg.AddEntry(power_fit, "Fit: p_{0} #times p_{T}^{p_{1}}", "l")
leg.AddEntry("", f"   p_{{0}}: {fit_result0}", "")
leg.AddEntry("", f"   p_{{1}}: {fit_result1}", "")
leg.SetLineWidth(0)
leg.Draw()

# Print CMS logo and lumi
CMSStyle.setCMSLumiStyle(c1, 10, era='scouting')

# Redraw axis on top and update canvas
c1.RedrawAxis()
c1.Update()