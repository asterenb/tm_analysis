from posixpath import devnull
from ROOT import TFile, TTree, TH1D, TH2D, TLorentzVector, TCanvas, TLegend, TGraphErrors
from ROOT import kBlue, kRed, kFALSE
from array import array
from math import sqrt
import sys

fileName = "fourMuon_hist.root"
if len(sys.argv) > 1 : fileName = sys.argv[1] 

inFile = TFile.Open(fileName)
hDR_all = inFile.Get("hDR_all")
hDR_good = inFile.Get("hDR_good")

c1 = TCanvas("c1")
c1.cd()
#c1.SetLogy()
hDR_all.Draw()
hDR_good.Draw("SAME") 
c1.Update()
input("<CR> to continue")  

# compute the efficiency curve
nBins = hDR_all.GetNbinsX()
xx, yy, ex, ey = array('d'), array('d'),  array('d'), array('d') 
for i in range(nBins-1)  :
    num, den = hDR_good.GetBinContent(i+1), hDR_all.GetBinContent(i+1)
    if den > 0. :
        eff = num/den
        err = sqrt(eff*(1.-eff)/den)
        x, y, e = hDR_all.GetBinCenter(i+1), eff, err
        if y > 0. :
            xx.append(x)
            yy.append(y)
            ex.append(0.) 
            ey.append(e)

g = TGraphErrors(len(xx),xx,yy,ex,ey)
g.SetTitle("Track Reconstruction Efficiency vs. DR")
g.GetXaxis().SetTitle("#DeltaR")
g.GetYaxis().SetTitle("Track Efficiency")
g.SetMarkerStyle(20)
g.SetMarkerColor(kRed)
g.Draw("AP") 
c1.Update()
input("<CR> to continue")  





