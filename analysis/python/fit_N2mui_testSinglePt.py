import ROOT as rt
from utils.fitter import fitter_2mu as fitter
import utils.configuration as configuration
cfg = configuration.Configuration()

low_high_pt_split = cfg.low_high_pt_split
pt_edges = cfg.pt_edges
n_mass_bins = cfg.n_mass_bins

bin = 14
f = rt.TFile.Open(cfg.fns['m2mu_vs_pt2mu'])
h = f.Get(f"bin{bin}/h_m2mu")

# Declare observable mass
mass = rt.RooRealVar("mass", "M_{#mu#mu} [GeV]", 0.5, 0.6)

fit_low = fitter(mass, bkg_model='Cheb3', sig_model='DoubleGauss')
fit_signal_low = fitter(mass, bkg_model='Cheb3', sig_model='Voigtian')
fit_high = fitter(mass, bkg_model='Pol1', sig_model='DoubleGauss')

# For each pT bin, fit the mass and add the yield and errors to the histos
tot_yields = 0.
yields, errors = {}, {}
massframe = mass.frame(rt.RooFit.Title(f"Signal + background fit for p_{{T}} in [{pt_edges[bin-1]}, {pt_edges[bin]}] GeV"))
dh = rt.RooDataHist("dh", "dh", mass, rt.RooFit.Import(h))
fit = fit_low
result = fit.model.fitTo(dh, rt.RooFit.Save())
dh.plotOn(massframe, DrawOption="E", LineColor=rt.kRed, MarkerSize=0.3, MarkerColor=rt.kRed)
fit.model.plotOn(massframe)
fit.model.plotOn(massframe, Components={fit.bkg()}, LineColor=rt.kGreen-1, LineStyle='--')

# Open file to store all the fits
f_out = rt.TFile.Open(cfg.fns['two_mu_fits'], "RECREATE")
f_out.mkdir(f"bin{bin}")
f_out.cd(f"bin{bin}")
w = rt.RooWorkspace("w","workspace")
w.Import(fit.model)
w.Import(dh)
w.Write()
massframe.Write()
f_out.Close()

c1 = rt.TCanvas()
massframe.Draw()
c1.Update()

# Extract the signal yield and error from the fit
yld = fit.nsig.getVal()
yields[6] = yld
tot_yields += yld
error = fit.nsig.getError()
errors[6] = error

print(result.Print("v"))
print(massframe.chiSquare(result.floatParsFinal().getSize()))

print("total signal yield: ", tot_yields)