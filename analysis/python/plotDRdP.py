from ROOT import TFile, TF1, TTree, TH1D, TH2D, TLorentzVector, TCanvas, TGraph, TGraphErrors

hDR = TH1D("hDR","hDR",100,0.,0.02)
hdP = TH1D("hdP","hdP",100,0.,0.50)

N = 2
muon, muon_gen = [], []
for i in range(N) : 
    muon.append(TLorentzVector())
    muon_gen.append(TLorentzVector())

# analyze the two muon events
if N == 2 : inFile = TFile.Open("twoMuon_000.root")
else : inFile = TFile.Open("fourMuon_000.root")
inTree = inFile.Get("Events")
genTree = inFile.Get("GenEvents")
inTree.AddFriend(genTree)

for count, e in enumerate(inTree) :
    if e.nMuons < N : continue 
    weight = 1. 

    muon[0].SetPtEtaPhiM(e.pt_1,e.eta_1,e.phi_1,0.107)
    muon[1].SetPtEtaPhiM(e.pt_2,e.eta_2,e.phi_2,0.107)
    muon_gen[0].SetPtEtaPhiM(e.pt_gen_1,e.eta_gen_1,e.phi_gen_1,0.107)
    muon_gen[1].SetPtEtaPhiM(e.pt_gen_2,e.eta_gen_2,e.phi_gen_2,0.107)
    if N == 4 :
        muon[2].SetPtEtaPhiM(e.pt_3,e.eta_3,e.phi_3,0.107)
        muon[3].SetPtEtaPhiM(e.pt_4,e.eta_4,e.phi_4,0.107)
        muon_gen[2].SetPtEtaPhiM(e.pt_gen_3,e.eta_gen_3,e.phi_gen_3,0.107)
        muon_gen[3].SetPtEtaPhiM(e.pt_gen_4,e.eta_gen_4,e.phi_gen_4,0.107)

    for i in range(N) :
        dPmin, DRmin = 9999., 9999.
        for j in range(N):
            DRmin = min(DRmin, muon_gen[i].DeltaR(muon[j]))
            dPmin = min(dPmin, abs(muon_gen[i].Pt()-muon[j].Pt())/muon_gen[i].Pt())
        hDR.Fill(DRmin)
        hdP.Fill(dPmin)

c1 = TCanvas("c1")
c1.cd()
c1.SetLogy()
hDR.Draw()
c1.Update()
input("<CR> to continue")  

c2 = TCanvas("c2")
c2.cd()
c2.SetLogy()
hdP.Draw()
c2.Update()
input("<CR> to continue")  



