#! /usr/bin/env python
from ROOT import TFile, TH1D, TCanvas, gPad
import sys
from math import sqrt
import numpy as np

def getArgs() :
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("-v","--verbose",default=0,type=int,help="Print level.")
    parser.add_argument("-f","--inFileName",default='data1.root',help="File to be analyzed.")
    parser.add_argument("-m","--maxPrint",default=0,type=int,help="Maximum number of events to print.")
    return parser.parse_args()

def goodVertex(e,j) :
    sum, div = 0, 2**j
    for k in range(4) :
        if (e.vtxList[k]/div % 2 == 1) : sum += int(e.charge[k])   
    return sum == 0 
         
# begin execution here 

args = getArgs()
nPrint = args.maxPrint 

hvtx_x = TH1D("hxvtx_x","x vertex",100,-10.,10.)
hvtx_y = TH1D("hxvtx_y","y vertex",100,-10.,10.)
hvtx_z = TH1D("hxvtx_z","z vertex",100,-20.,20.)
hvtx_r = TH1D("hxvtx_r","Vertex radius",100,0.,10.)
hvtx_chi2 = TH1D("hvtx_chi2","Vertex chi**2",100,0.,20.)

inFile = TFile(args.inFileName) 
inFile.cd()
t = inFile.Get("Events")
nentries = t.GetEntries()
print("nEntries={0:d}".format(nentries))

# loop over ntuple to fill histograms  
for i, e in enumerate(t) :
    # Loop over vertices.   Plot only those from opposite sign pairs.
    if e.nVtx < 1 : continue 
    #if e.mass < 0.54 or e.mass > 0.56 : continue 
    #if e.nGood < 4 : continue 
    bestList = []
    printOn = False 
    for j in range(min(e.nVtx,4)) :
        hvtx_chi2.Fill(e.vtx_chi2[j])
        # plot (at most) the best two vertices sorted by chi2 or radius 
        if goodVertex(e,j) : 
            #bestList.append(sqrt(e.vtx_x[j]**2 + e.vtx_y[j]**2)) 
            bestList.append(e.vtx_chi2[j])
    sortIndex = np.argsort(np.array(bestList))
    for j in range(min(len(sortIndex),2)) :
        idx = sortIndex[j]
        hvtx_x.Fill(e.vtx_x[idx])
        hvtx_y.Fill(e.vtx_y[idx])
        hvtx_z.Fill(e.vtx_z[idx])
        rr = sqrt(e.vtx_x[idx]**2 + e.vtx_y[idx]**2)
        hvtx_r.Fill(rr)
        if rr > 2.0 : printOn = True

    if nPrint > 0 and printOn :
        nPrint -= 1
        print("\nMass={0:10.3f} nVtx={1:2d} nGood={2:3d}".format(e.mass,e.nVtx,e.nGood))
        print("Muons\n  #  Q    pt      eta     phi     vtx")
        for j in range(4) :
            print("{0:3d}{1:3d}{2:8.3f}{3:8.3f}{4:8.3f}{5:8b}".format(j,int(e.charge[j]),e.pt[j],e.eta[j],e.phi[j],e.vtxList[j])) 
    
        print("Vertices:\n  #     x       y       z      chi2  good".format(e.nVtx))
        for j in range(min(e.nVtx,4)) :
            print("{0:3d}{1:8.4f}{2:8.4f}{3:8.4f}{4:8.2f} {5}".format(j,e.vtx_x[j],e.vtx_y[j],e.vtx_z[j],e.vtx_chi2[j],goodVertex(e,j)))
    

# Draw the histograms
c1 = TCanvas("c1","Vertex Properties",1200,700)

c1.Divide(2,2)
c1.cd(1)
gPad.SetLogy() 
hvtx_x.Draw()
hvtx_x.GetXaxis().SetTitle("Vertex x (cm)")
c1.cd(2)
gPad.SetLogy() 
hvtx_y.Draw()
hvtx_y.GetXaxis().SetTitle("Vertex y (cm)")
c1.cd(3)
gPad.SetLogy() 
hvtx_z.Draw()
hvtx_z.GetXaxis().SetTitle("Vertex z (cm)")
c1.cd(4)
gPad.SetLogy() 
hvtx_r.Draw()
hvtx_r.GetXaxis().SetTitle("Vertex radius (x-y)")
	
c1.Update()
print("Enter <CR> to continue") 
raw_input()



