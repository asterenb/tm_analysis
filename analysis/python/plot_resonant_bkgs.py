import ROOT as rt
import utils.CMSStyle as CMSStyle

CMSStyle.setTDRStyle()

modes = ['4mu', 'pipimumu', 'mumugamma', 'pipipi0', '3pi0', \
    'pipigamma', 'gammagamma']

def lbl(*parts):
    greek = {'pp': '#pi^{+}', 'pm': '#pi^{-}', 'p': '#pi^{0}',
     'mp': '#mu^{+}', 'mm': '#mu^{-}', 'g': '#gamma'}
    label = '#eta #rightarrow'
    for p in parts:
        label += f' {greek[p]}'
    return label

titles = [lbl('mp', 'mm', 'mp', 'mm'), lbl('pp', 'pm', 'mp', 'mm'), \
    lbl('mp', 'mm', 'g', 'g'), lbl('pp', 'pm', 'p'), lbl('p', 'p', 'p'), \
    lbl('pp', 'pm', 'g', 'g'), lbl('g', 'g')] 

markers = [rt.kFullSquare, rt.kFullTriangleUp, rt.kFullStar, \
    rt.kFullCrossX, rt.kFullTriangleDown, \
    rt.kFullDoubleDiamond, rt.kFullCircle]

colors = [rt.kRed, rt.kCyan, rt.kBlue, rt.kYellow, \
     rt.kMagenta, rt.kGreen, rt.kOrange]

sizes = [0.8, 0.9, 1.0, 0.9, 0.9, 1.1, 0.8]

files = {}
for mode in modes:
    files[mode] = rt.TFile.Open(f'../../toyMC/eta/{mode}.root')

hists = {}
for m, f in files.items():
    hists[m] = f.Get("hMass2")

c1 = rt.TCanvas()
c1.SetLogy()
c1.SetTicks()
frame = c1.DrawFrame(0.4, 1e-7, 0.8, 1e3)
frame.GetXaxis().SetTitle("m_{4#mu} [GeV]")
frame.GetYaxis().SetTitle("Events / 0.01 GeV")

leg = rt.TLegend(0.55, 0.6, 0.79, 0.87)
# leg.SetBorderSize(1)
for i, h in enumerate(hists.values()):
    h.SetTitle(titles[i])
    h.Rebin()
    h.SetMarkerColor(colors[i]-3)
    h.SetLineColor(colors[i]-3)
    h.SetMarkerStyle(markers[i])
    h.SetMarkerSize(sizes[i]+0.2)
    h.Draw("SAME HIST P")
    leg.AddEntry(h, h.GetTitle(), "p")

leg.Draw()

# Print CMS logo and lumi
CMSStyle.setCMSLumiStyle(c1, 10, era='scouting')

c1.RedrawAxis()
c1.Update()