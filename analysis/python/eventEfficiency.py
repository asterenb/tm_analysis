from posixpath import devnull
from ROOT import TFile, TTree, TH1D, TH2D, TLorentzVector, TCanvas, TLegend, TGraphErrors
from ROOT import kBlue, kRed, kFALSE
from array import array
from math import sqrt
import sys

fileName = "fourMuon_hist.root"
if len(sys.argv) > 1 : fileName = sys.argv[1] 

inFile = TFile.Open(fileName)
hPt_eta_all = inFile.Get("hPt_eta_all")
hPt_eta_good = inFile.Get("hPt_eta_good")

c1 = TCanvas("c1")
c1.cd()
#c1.SetLogy()
hPt_eta_all.Draw()
hPt_eta_good.Draw("SAME") 
c1.Update()
input("<CR> to continue")  

# compute the efficiency curve
nBins = hPt_eta_all.GetNbinsX()
xx, yy, ex, ey = array('d'), array('d'),  array('d'), array('d') 
for i in range(nBins-1)  :
    num, den = hPt_eta_good.GetBinContent(i+1), hPt_eta_all.GetBinContent(i+1)
    if den > 0. :
        eff = num/den
        err = sqrt(eff*(1.-eff)/den)
        x, y, e = hPt_eta_all.GetBinCenter(i+1), eff, err
        if y > 0. :
            xx.append(x)
            yy.append(y)
            ex.append(0.) 
            ey.append(e)

g = TGraphErrors(len(xx),xx,yy,ex,ey)
g.SetTitle("Event Reconstruction Efficiency vs. #eta p_{t}")
g.GetXaxis().SetTitle("#eta p_{t} (GeV)")
g.GetYaxis().SetTitle("Event Efficiency")
g.SetMarkerStyle(20)
g.SetMarkerColor(kRed)
g.Draw("AP") 
c1.Update()
input("<CR> to continue")  





