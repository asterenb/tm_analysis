from ROOT import TFile, TTree, TH1D, TH2D, TLorentzVector, TCanvas, TLegend
from ROOT import kBlue, kRed, kFALSE
import numpy as np 
from math import sqrt 

def SWtrig(nMuons,muon,charge,threshold) :
    if nMuons < 1 : return False 
    nMuons = min(nMuons,4)
    for i in range(nMuons) :
        if muon[i].Pt() < threshold : continue 
        for j in range(i+1,nMuons) :
            if muon[j].Pt() < threshold : continue
            if charge[i]*charge[j] > 0. : continue
            DR = muon[i].DeltaR(muon[j])
            if DR > 1.2 : continue 
            return True
    return False 

inFile = TFile.Open("fourMuon_000.root")
inTree= inFile.Get("Events")
genTree = inFile.Get("GenEvents")
inTree.AddFriend(genTree)

nentries = inTree.GetEntries()
print("nentries={0:d}".format(nentries))

# make a single histogram 
fOut = TFile('fourMuon_hist.root', 'recreate' )

hmuonPt_all = TH1D('hmuonPt_all','hmuonPt_all',100, 0., 50.) 
hmuonPt_all.SetLineColor(kBlue)
hmuonPt_all.GetXaxis().SetTitle("muon p_{t} (GeV)")
hmuonPt_good = TH1D('hmuonPt_good','hmuonPt_good',100, 0., 50.) 
hmuonPt_good.SetLineColor(kRed)

hDR_all = TH1D('hDR_all','hDR_all',100, 0., 0.1) 
hDR_all.SetLineColor(kBlue)
hDR_all.GetXaxis().SetTitle("track #DeltaR")
hDR_good = TH1D('hDR_good','hDR_good',100, 0., 0.1) 
hDR_good.SetLineColor(kRed)

hPt_eta_all = TH1D('hPt_eta_all','hPt_eta_all',100, 0., 100.) 
hPt_eta_all.SetLineColor(kBlue)
hPt_eta_all.GetXaxis().SetTitle("#eta p_{t} (GeV)")
hPt_eta_good = TH1D('hPt_eta_good','hPt_eta_good',100, 0., 100.) 
hPt_eta_good.SetLineColor(kRed)

hTrig_all = TH1D('hTrig_all','hTrig_all',100, 0., 100.) 
hTrig_all.SetLineColor(kBlue)
hTrig_all.GetXaxis().SetTitle("#eta p_{t} (GeV)")
hTrig_good = TH1D('hTrig_good','hTrig_good',100, 0., 100.) 
hTrig_good.SetLineColor(kRed)

hnMuon = TH1D('hnMuon','hnMuon',10,0.,10.)
hnMuon.GetXaxis().SetTitle("Number of reconstructed muons")

hTrigPt_all = TH1D('hTrigPt_all','hTrigPt_all',100,0.,40.)
hTrigPt_all.SetLineColor(kBlue)
hTrigPt_good = TH1D('hTrigPt_good','hTrigPt_good',100,0.,40.)
hTrigPt_good.SetLineColor(kRed)

verbose = 0 
muon, muon_gen = [], []
triggerThreshold = 5. 
for i in range(4) : 
    muon.append(TLorentzVector())
    muon_gen.append(TLorentzVector())

printCount = 0 
for count, e in enumerate(inTree) :
    hnMuon.Fill(e.nMuons)
    good = e.nMuons > 3 
    trigFired = e.trigFired > 0

    # fill the trigger histograms    
    pt_list = [e.pt_gen_1,e.pt_gen_2,e.pt_gen_3,e.pt_gen_4]
    if True :     # reco level 
        if e.nMuons > 0 : pt_list.append(e.pt_1)
        if e.nMuons > 1 : pt_list.append(e.pt_2) 
        if e.nMuons > 2 : pt_list.append(e.pt_3)
        if e.nMuons > 3 : pt_list.append(e.pt_4)
    if len(pt_list) > 1 : 
        pt_list.sort(reverse=True) 
        if pt_list[0] > 0. :
            hTrigPt_all.Fill(pt_list[1])
            if trigFired : hTrigPt_good.Fill(pt_list[1])
    
    #if not e.nMuons > 2 : continue 
    muon[0].SetPtEtaPhiM(e.pt_1,e.eta_1,e.phi_1,0.107)
    muon[1].SetPtEtaPhiM(e.pt_2,e.eta_2,e.phi_2,0.107)
    muon[2].SetPtEtaPhiM(e.pt_3,e.eta_3,e.phi_3,0.107)
    muon[3].SetPtEtaPhiM(e.pt_4,e.eta_4,e.phi_4,0.107)
    charge = [] 
    charge.append(e.charge_1)
    charge.append(e.charge_2) 
    charge.append(e.charge_3)
    charge.append(e.charge_4) 

    muon_gen[0].SetPtEtaPhiM(e.pt_gen_1,e.eta_gen_1,e.phi_gen_1,0.107)
    muon_gen[1].SetPtEtaPhiM(e.pt_gen_2,e.eta_gen_2,e.phi_gen_2,0.107)
    muon_gen[2].SetPtEtaPhiM(e.pt_gen_3,e.eta_gen_3,e.phi_gen_3,0.107)
    muon_gen[3].SetPtEtaPhiM(e.pt_gen_4,e.eta_gen_4,e.phi_gen_4,0.107)
    charge_gen = [] 
    charge_gen.append(e.charge_gen_1)
    charge_gen.append(e.charge_gen_2) 
    charge_gen.append(e.charge_gen_3)
    charge_gen.append(e.charge_gen_4)

    # calculate the 2D RMS in the eta-phi plane
    etaRMS = np.std(np.array([e.eta_gen_1,e.eta_gen_2,e.eta_gen_3,e.eta_gen_4]))
    phiRMS = np.std(np.array([e.phi_gen_1,e.phi_gen_2,e.phi_gen_3,e.phi_gen_4]))
    DR_RMS = sqrt(etaRMS**2 + phiRMS**2) 

    hPt_eta_all.Fill(e.pt_gen_eta)
    if good : hPt_eta_good.Fill(e.pt_gen_eta)

    goodSWtrig = SWtrig(e.nMuons,muon,charge,triggerThreshold) 
    if e.nMuons > 3 :
        hTrig_all.Fill(e.pt_gen_eta)
        if goodSWtrig : hTrig_good.Fill(e.pt_gen_eta)
            
    printFlag = False 
    for i in range(4) :
        dPmin = 9999.
        DRmin = 9999.
        for j in range(4):
            DR = muon_gen[i].DeltaR(muon[j])
            DRmin = min(DRmin,DR)
            dP = abs(muon_gen[i].Pt()-muon[j].Pt())/muon_gen[i].Pt()
            dPmin = min(dPmin,dP)

        hmuonPt_all.Fill(muon_gen[i].Pt())
        goodTrack = DRmin < 0.005 and dPmin < 0.06

        if goodTrack : 
            hmuonPt_good.Fill(muon_gen[i].Pt())

        # calculate the closest DR for this track 
        DR = 9999.
        for j in range(i+1,4): DR = min(DR,muon_gen[i].DeltaR(muon_gen[j]))
        if muon_gen[i].Pt() > 5. :
            hDR_all.Fill(DR)
            if goodTrack : hDR_good.Fill(DR)

    printFlag = True        
    if printFlag and printCount < 20 :
        printCount += 1 
        print("\nGen Muons: count={0:d} nMuons={1:d} DR_RMS={2:8.4f} trigFired={3}".format(count,e.nMuons,DR_RMS,trigFired))
        print("    pt      eta     phi  jBest    DR     dP")
        for i in range(4) :
            # get the track that matches  
            dPmin, DR, jBest = 99., 99., -1
            for j in range(4):
                DR = min(DR,muon_gen[i].DeltaR(muon[j]))
                if DR < 0.005 :
                    dP = abs(muon_gen[i].Pt()-muon[j].Pt())/muon_gen[i].Pt()
                    if dP < dPmin : 
                        jBest = j
                        dPmin = dP
                        DRmin = DR
            st1 = ""
            if jBest > -1 : st1 = "{0:6d}{1:8.4f}{2:8.4f}".format(jBest,DRmin,dPmin)
            print("{0:8.2f}{1:8.4f}{2:8.4f}{3:s}".format(muon_gen[i].Pt(),muon_gen[i].Eta(),muon_gen[i].Phi(),st1))
            
        print("Reco Muons nMuons={0:d}: \n    pt      eta     phi ".format(e.nMuons))
        for i in range(4) :
            print("{0:8.2f}{1:8.4f}{2:8.4f}".format(muon[i].Pt(),muon[i].Eta(),muon[i].Phi()))


fOut.cd()
fOut.Write()
fOut.Close()        
