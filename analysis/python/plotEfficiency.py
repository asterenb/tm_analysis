from posixpath import devnull
from ROOT import TFile, TTree, TH1D, TH2D, TLorentzVector, TCanvas, TLegend, TGraphErrors, TF1
from ROOT import kBlue, kRed, kFALSE
from array import array
from math import sqrt
import sys

def getArgs() :
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("-v","--verbose",default=0,type=int,help="Print level.")
    parser.add_argument("-f","--fileName",default="fourMuon_hist.root",help="Input file.")
    parser.add_argument("-t","--tag",default="Pt_eta",help="Tag for quantity to be plotted.")
    return parser.parse_args()

args = getArgs() 
fileName = args.fileName 
tag = args.tag

xAxis = {"muonPt":"Muon p_{t} (GeV)", "DR":"Track #DeltaR", "Pt_eta":"#eta p_{t} (GeV)", "eta_eta":"#eta pseudorapidity",
         "dPhi":"#phi_{+} - #phi_{-}", "TrigPt":"p_{t} of subleading muon (GeV)","Trig":"#eta p_{t} (GeV)"}

inFile = TFile.Open(fileName)
h_all = inFile.Get("h{0:s}_all".format(tag))
h_good = inFile.Get("h{0:s}_good".format(tag))

c1 = TCanvas("c1")
c1.cd()
#c1.SetLogy()
h_all.Draw()
h_good.Draw("SAME") 
c1.Update()
input("<CR> to continue")  

# compute the efficiency curve
nBins = h_all.GetNbinsX()
xx, yy, ex, ey = array('d'), array('d'),  array('d'), array('d') 
for i in range(nBins-1)  :
    num, den = h_good.GetBinContent(i+1), h_all.GetBinContent(i+1)
    if den > 0. :
        eff = num/den
        err = sqrt(eff*(1.-eff)/den)
        x, y, e = h_all.GetBinCenter(i+1), eff, err
        if y > 0. :
            xx.append(x)
            yy.append(y)
            ex.append(0.) 
            ey.append(e)

g = TGraphErrors(len(xx),xx,yy,ex,ey) 
g.SetTitle("Reconstruction Efficiency")
g.GetXaxis().SetTitle(xAxis[tag])
g.GetYaxis().SetTitle("Efficiency")
g.SetMarkerStyle(20)
g.SetMarkerColor(kRed)
if tag == "muonPt" :
    fun = TF1("fun","[0]/(1.+exp(([1]-x)/[2]))")
    #g.Fit("fun")
g.Draw("AP")

c1.Update()
input("<CR> to continue")  





