from posixpath import devnull
from ROOT import TFile, TH1D, TLorentzVector, TCanvas, TGraph, TVector3 
from ROOT import kBlue, kRed, kFALSE
from array import array
from math import sqrt, exp 
import numpy as np 
import utils.configuration as configuration
config = configuration.Configuration()

def getArgs() :
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("-v","--verbose",default=0,type=int,help="Print level.")
    parser.add_argument("-f","--fileName",default="fourMuon_hist.root",help="Input file.")
    parser.add_argument("-t","--threshold",default = 0.,type=float,help="Turn-on curve threshold.")
    parser.add_argument("-p","--plateau",default = 1.0,type=float,help="Plateau reduction factor.")
    return parser.parse_args()

def getRatio(h_all,h_good) :
    p_t,  A = array('d'), array('d')
    for i in range(h_all.GetNbinsX()) :
        acc = 0.
        if h_all.GetBinContent(i) > 0. : acc = h_good.GetBinContent(i)/h_all.GetBinContent(i)
        p_t.append(h_all.GetBinCenter(i))
        A.append(acc)
    g = TGraph(len(p_t),p_t,A)
    return p_t, g

def mapBins(h,g) :
    eps = 1.e-4
    lumi, B_2mu = 101., 5.8e-6    # lumi in fb^-1 
    p_t,  N_2mu = array('d'), array('d')
    for i in range(5,h.GetNbinsX()) :
        low = h.GetBinLowEdge(i+1)
        high = low + h.GetBinWidth(i+1)
        accPoints =[]
        for j in range(g.GetN()) :
            x = g.GetPointX(j) 
            if x > low-eps and x < high+eps : 
                accPoints.append(g.GetPointY(j))
        if len(accPoints) < 1 : continue 
        acc = np.mean(np.array(accPoints))
        xSec = h.GetBinContent(i)
        p_t.append(0.5*(low+high))
        N_2mu.append(lumi*B_2mu*acc*xSec)
        #print("{0:7.2f} {1:7.2f} {2:7.1f}".format(low,high,lumi*B_2mu*acc*xSec))
    return p_t, N_2mu

def getN_2mu(h_xsec) :
    #h_xsec.Draw()
    #input("<CR> to continue")    
    inFile2 = TFile.Open(config.two_mu_acc)
    g_acc = inFile2.Get("acceptance_EtaTo2Mu")
    p_t, N_2mu = mapBins(h_xsec,g_acc)
    inFile2.Close()
    return p_t, N_2mu

def SWtrig(N,nMuons,muon,charge,threshold) :
    if nMuons < 1 : return False 
    nMuons = min(nMuons,N)
    for i in range(nMuons) :
        if muon[i].Pt() < threshold : continue 
        for j in range(i+1,nMuons) :
            if muon[j].Pt() < threshold : continue
            if charge[i]*charge[j] > 0. : continue
            DR = muon[i].DeltaR(muon[j])
            if DR > 1.2 : continue 
            return True
    return False 

def getAcceptance(args, h_xsec, N) :
    # _2 and _4 refer to the two-muon and four-muon MC samples 
    hPt_eta_all = TH1D('hPt_eta_all_2','hPt_eta_all_2',100, 0., 100.) 
    hPt_eta_all.SetLineColor(kBlue)
    hPt_eta_all.GetXaxis().SetTitle("#eta p_{t} (GeV)")
    hPt_eta_good = TH1D('hPt_eta_good_2','hPt_eta_good_2',100, 0., 100.) 
    hPt_eta_good.SetLineColor(kRed)

    hTrack_all = TH1D('hTrack_all','hTrack_all',100,0.,100.)
    hTrack_good = TH1D('hTrack_good','hTrack_good',100,0.,100.)

    muon, muon_gen, charge = [], [], []
    for i in range(N) : 
        muon.append(TLorentzVector())
        muon_gen.append(TLorentzVector())
        charge.append(0.)

    if N == 2 : inFile = TFile.Open("twoMuon_000.root")
    else : inFile = TFile.Open("fourMuon_000.root")
    inTree = inFile.Get("Events")
    genTree = inFile.Get("GenEvents")
    inTree.AddFriend(genTree)

    for count, e in enumerate(inTree) :
        hPt_eta_all.Fill(e.pt_gen_eta)
        goodEvent = e.nMuons >= N  
        #if e.nMuons < N : continue 
        weight = 1. 

        muon[0].SetPtEtaPhiM(e.pt_1,e.eta_1,e.phi_1,0.107)
        muon[1].SetPtEtaPhiM(e.pt_2,e.eta_2,e.phi_2,0.107)
        charge[0] = e.charge_1
        charge[1] = e.charge_2 
        muon_gen[0].SetPtEtaPhiM(e.pt_gen_1,e.eta_gen_1,e.phi_gen_1,0.107)
        muon_gen[1].SetPtEtaPhiM(e.pt_gen_2,e.eta_gen_2,e.phi_gen_2,0.107)
        if N == 4 :
            muon[2].SetPtEtaPhiM(e.pt_3,e.eta_3,e.phi_3,0.107)
            muon[3].SetPtEtaPhiM(e.pt_4,e.eta_4,e.phi_4,0.107)
            charge[2] = e.charge_3
            charge[3] = e.charge_4 
            muon_gen[2].SetPtEtaPhiM(e.pt_gen_3,e.eta_gen_3,e.phi_gen_3,0.107)
            muon_gen[3].SetPtEtaPhiM(e.pt_gen_4,e.eta_gen_4,e.phi_gen_4,0.107)

        printTrack = goodEvent and N==4 and count < 10000 and e.pt_gen_eta > 19. and e.pt_gen_eta < 20.
        for i in range(N) :
            pt = muon_gen[i].Pt()
            hTrack_all.Fill(pt)
            dPmin, DRmin = 9999., 9999.
            for j in range(N):
                DRmin = min(DRmin, muon_gen[i].DeltaR(muon[j]))
                dPmin = min(dPmin, abs(pt-muon[j].Pt())/pt)
            #goodTrack = DRmin < 0.01 and dPmin < 0.10 and pt > args.threshold
            goodTrack = DRmin < 0.01 and dPmin < 0.10 
            if not goodTrack : 
                if printTrack : print("N={0:d} count={1:3d} i={2:d} pt={3:7.1f} bad track".format(N,count,i,pt))
                goodEvent = False 
                break
            wt = args.plateau/(1. + exp((args.threshold-pt)/0.383))
            #wt = args.plateau
            weight *= wt
            if printTrack : 
                if i==0 : print(" ")
                print("N={0:d} count={1:3d} pt={2:7.1f} wt={3:.4f} weight={4:.4f} pt_eta={5:7.1f}".format(
                N,count,pt,wt,weight,e.pt_gen_eta))
            hTrack_good.Fill(muon_gen[i].Pt(),wt)
            
        if not goodEvent : continue 
        #if weight > 1. : print("eta_pt={0:7.1f} weight={1:6.3f}".format(e.pt_gen_eta,weight))
        triggerThreshold = 4.8 
        goodSWtrig = SWtrig(N,e.nMuons,muon,charge,triggerThreshold) 
        if True or goodSWtrig : hPt_eta_good.Fill(e.pt_gen_eta,weight)

    inFile.Close()
    p_t, g = getRatio(hPt_eta_all,hPt_eta_good)
    p_t, gTrack = getRatio(hTrack_all,hTrack_good)

    # map acceptance values onto bins defined in h_xsec
    eps = 1.e-4
    p_t,  A = array('d'), array('d')
    for i in range(5,h_xsec.GetNbinsX()) :
        low = h_xsec.GetBinLowEdge(i+1)
        high = low + h_xsec.GetBinWidth(i+1)
        accPoints =[]
        for j in range(g.GetN()) :
            x = g.GetPointX(j) 
            if x > low-eps and x < high+eps : 
                accPoints.append(g.GetPointY(j))
        if len(accPoints) < 1 : continue 
        acc = np.mean(np.array(accPoints))
        p_t.append(0.5*(low+high))
        A.append(acc) 

    #p_t, A = mapBins(h_xsec,g)
    if False :
        print("In getAcceptance N={0:d}".format(N))
        for i in range(len(p_t)) :
            print("{0:3d} {1:5.1f} {2:8.1f} {3:8.1f} {4:8.3f} {5:8.3f} ".format(
                i,p_t[i],hPt_eta_good.GetBinContent(i),hPt_eta_all.GetBinContent(i),g.GetPointY(i),A[i]))
    return p_t, A, gTrack 

# begin execution here

args = getArgs() 
B_2mu, B_4mu = 5.8e-6, 4.0e-9
inFile1 = TFile.Open(config.corrected_xsec) 
h_xsec = inFile1.Get("h_corrXsec")
p_t, N_2mu = getN_2mu(h_xsec) 
p_t, A_2mu, gTrack_2mu = getAcceptance(args, h_xsec, 2)
p_t, A_4mu, gTrack_4mu = getAcceptance(args, h_xsec, 4)

N_4mu_sum = 0.
N_4mu = np.zeros_like(A_2mu)
if True :
    print(" i  p_t    N_2mu     A_2mu    A_4mu    N_4mu   N_sum")
    for i in range(len(p_t)-1) :
        if A_2mu[i] > 0. : N_4mu[i] = (B_4mu/B_2mu)*N_2mu[i]*(A_4mu[i]/A_2mu[i])
        N_4mu_sum += N_4mu[i] 
        print("{0:3d} {1:5.1f} {2:9.1f} {3:8.5f} {4:10.8f} {5:8.3f} {6:8.3f}".format(
            i,p_t[i],N_2mu[i],A_2mu[i],A_4mu[i],N_4mu[i],N_4mu_sum))
    print("N_4mu_sum={0:.1f}".format(N_4mu_sum))


# write out graphs for plotting 

fOut = TFile("./data_out/acceptance_t{0:.2f}_p{1:.2f}_n{2:.2f}.root".format(args.threshold,args.plateau,N_4mu_sum),"recreate")
fOut.cd() 

#V3 = TVector3(args.threshold, args.plateau, N_4mu_sum)
#V3.Write() 

gA_2mu = TGraph(len(p_t),p_t,A_2mu)
gA_2mu.SetName("gA_2mu") 
gA_2mu.SetTitle("Two Muon Event Acceptance")
gA_2mu.GetXaxis().SetTitle("#eta p_{t} (GeV)")
gA_2mu.GetYaxis().SetTitle("Acceptance")
gA_2mu.SetMarkerStyle(20)
gA_2mu.SetMarkerColor(kBlue)
gA_2mu.Write()

gA_4mu = TGraph(len(p_t),p_t,A_4mu)
gA_4mu.SetName("gA_4mu") 
gA_4mu.SetTitle("Four Muon Event Acceptance")
gA_4mu.GetXaxis().SetTitle("#eta p_t (GeV)")
gA_4mu.GetYaxis().SetTitle("Acceptance")
gA_4mu.SetMarkerStyle(20)
gA_4mu.SetMarkerColor(kRed)
gA_4mu.Write()

gTrack_2mu.SetTitle("Two Muon Track Acceptance")
gTrack_2mu.SetName("gTrack_2mu") 
gTrack_2mu.GetXaxis().SetTitle("#mu p_t (GeV)")
gTrack_2mu.GetYaxis().SetTitle("Acceptance")
gTrack_2mu.SetMarkerStyle(20)
gTrack_2mu.SetMarkerColor(kBlue)
gTrack_2mu.Write()

gTrack_4mu.SetTitle("Four Muon Track Acceptance")
gTrack_4mu.SetName("gTrack_4mu") 
gTrack_4mu.GetXaxis().SetTitle("#mu p_t (GeV)")
gTrack_4mu.GetYaxis().SetTitle("Acceptance")
gTrack_4mu.SetMarkerStyle(20)
gTrack_4mu.SetMarkerColor(kRed)
gTrack_4mu.Write() 
fOut.Write() 

fOut.Close()


exit() 


