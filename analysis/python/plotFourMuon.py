from ROOT import TH1D, TFile, kRed, kBlue, TCanvas, TLegend 

inFile = TFile.Open("../data_out/fourMuon_v4.root")
inTree= inFile.Get("Events")

hMass_all = TH1D('hMass_all','hMass_all',80,0.4,0.8)

hMass_good = TH1D('hMass_good','hMass_good',80,0.4,0.8)
hMass_good.GetXaxis().SetTitle('M_{4#mu} GeV')
hMass_good.GetYaxis().SetTitle('Counts/0.010 GeV')
hMass_good.SetLineColor(kBlue)
hMass_good.SetLineWidth(2)
hMass_good.SetMaximum(30.) 
hMass_good.SetStats(False)

hMass_bkg = TH1D('hMass_bkg','hMass_bkg',80,0.4,0.8)
hMass_bkg.SetMarkerColor(kRed)
hMass_bkg.SetMarkerStyle(20)
#hMass_bkg.SetMarkerSize(2)
hMass_bkg.SetLineWidth(2)
hMass_bkg.SetLineColor(kRed)

for e in inTree :
    mass = e.mass
    if abs(e.charge_4mu) > 0.5 : continue 
    hMass_all.Fill(mass)
    if e.nGood < 4 : hMass_bkg.Fill(mass,0.15)
    if e.nGood > 3 : hMass_good.Fill(mass)

c1 = TCanvas("c1")
c1.cd()
#c1.SetLogy()
hMass_good.Draw()
hMass_bkg.Draw("SAME L") 

legend = TLegend(0.10,0.75,0.45,0.90)
#legend.SetHeader("Decay Mode","C")  #  option "C" allows to center the header
legend.AddEntry(hMass_good,"Matched muons #geq 4","L")
legend.AddEntry(hMass_bkg,"Matched muons < 4 (x 0.15)","L")
legend.Draw()
c1.Update()
input("<CR> to continue")  

