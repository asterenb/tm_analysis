from typing import OrderedDict
import ROOT as rt
import utils.histos_from_csv as csv_utils
from utils.fitter import fitter_2mu as fitter
import utils.configuration as configuration
config = configuration.Configuration()

colors = [rt.kAzure, rt.kViolet+7, rt.kBlue-9, rt.kMagenta+3]

f = rt.TFile.Open(config.m2mu_vs_pt2mu)

low_high_pt_split = config.low_high_pt_split
pt_edges = config.pt_edges
pt_centers = [(pt_edges[i]+pt_edges[i+1])/2 for i in range(len(pt_edges)-1)]

mass = rt.RooRealVar("mass", "m_{#mu#mu} [GeV]", 0.5, 0.6)

# Open file to store all the fits
f_out = rt.TFile.Open(config.fit_variations_f, "RECREATE")

nFits = 4
fitters = OrderedDict({
    'Voigtian_Cheb4': fitter(mass, bkg_model='Cheb4', sig_model='Voigtian'),
    'DoubleGauss_Cheb4': fitter(mass, bkg_model='Cheb4', sig_model='DoubleGauss'),
    'Voigtian_Cheb3': fitter(mass, bkg_model='Cheb3', sig_model='Voigtian'),
    'Voigtian_Cheb2': fitter(mass, bkg_model='Cheb2', sig_model='Voigtian')
})

sum_yields = [0.] * nFits
N_2mu, N_2mu_err = [{} for j in range(nFits)], [{} for j in range(nFits)]
chi2 = [{} for j in range(nFits)]
for i in range(1, len(pt_centers)+1):
    if pt_centers[i-1] < 5:
        for j in range(nFits):
            N_2mu[j][i], N_2mu_err[j][i] = 0., 0.
            chi2[j][i] = -1.
        continue
    h = f.Get(f"bin{i}/h_m2mu")
    massframe = mass.frame(rt.RooFit.Title(f"Signal + background fit for p_{{T}} in [{pt_edges[i-1]}, {pt_edges[i]}] GeV"))
    dh = rt.RooDataHist("dh", "dh", mass, rt.RooFit.Import(h))
    dh.plotOn(massframe, rt.RooFit.DrawOption("E"), rt.RooFit.LineColor(rt.kRed), rt.RooFit.MarkerSize(0.3), rt.RooFit.MarkerColor(rt.kRed))
    results = [fit.model.fitTo(dh, rt.RooFit.Save()) for fit in fitters.values()]
    for j, fit in enumerate(fitters.values()):
        fit.model.plotOn(massframe, rt.RooFit.LineColor(colors[j]))
    f_out.mkdir(f"bin{i}")
    f_out.cd(f"bin{i}")
    w = rt.RooWorkspace(f"w_bin{i}", f"workspace_bin{i}")
    for fit in fitters.values():
        w.Import(fit.model)
    w.Import(dh)
    w.Write()
    massframe.Write()
    for res in results:
        res.Write()

    # Extract the signal yield and error from the fits
    for j, (fit_name, fit) in enumerate(fitters.items()):
        N_2mu[j][i] = fit.nsig.getVal()
        sum_yields[j] += N_2mu[j][i]
        N_2mu_err[j][i] = fit.nsig.getError()
        chi2[j][i] = massframe.chiSquare(fit_name + "_Norm[mass]", "h_dh", results[j].floatParsFinal().getSize())


for j, fit_name in enumerate(fitters.keys()):
    all_data = []
    for i, pt in enumerate(pt_centers):
        i_bin = i + 1
        d = OrderedDict({
            'bin': i_bin, 
            'pt_center': pt,
            'pt_low': pt_edges[i],
            'pt_high': pt_edges[i+1],
            'N_2mu': N_2mu[j][i_bin],
            'N_2mu_error': N_2mu_err[j][i_bin],
            'chi2_dof': chi2[j][i_bin]
        })
        all_data.append(d)

    # Write out information in csv format rather than histos
    # (Use histo facility in utils for the histograms)
    csv_utils.write_csv(f'{config.fit_variations_p}/fits_{fit_name}.csv', all_data)

    print('Fit name: ', fit_name)
    print("total signal yield: ", sum_yields[j])