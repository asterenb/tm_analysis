import ROOT as rt
from utils.CMSStyle import get_canvas_and_frame, setTDRStyle, setCMSLumiStyle
from utils.fitter import fitter_2mu as fitter
import utils.configuration as configuration
cfg = configuration.Configuration()
setTDRStyle()

def fit_2mu_signal(h):
    mass = rt.RooRealVar("mass", "m_{#mu#mu} [GeV]", 0.51, 0.59)
    fit_total = fitter(mass, sig_model='Voigtian', bkg_model='Cheb3')
    # fit_total.set_sig_params(config.two_mu_fit_params.sig_model['all'])
    # fit_total.set_bkg_params(config.two_mu_fit_params.bkg_model['all'])
    massframe = mass.frame(rt.RooFit.Title("Fit to a double-Gaussian plus two-param. Chebychev"))
    dh = rt.RooDataHist("dh", "dh", mass, rt.RooFit.Import(h))
    dh.plotOn(massframe, rt.RooFit.Name("data"), rt.RooFit.DrawOption("E"), 
        rt.RooFit.LineColor(rt.kBlack), rt.RooFit.MarkerSize(0.3), 
        rt.RooFit.MarkerColor(rt.kBlack))
    fit_total.model.fitTo(dh, rt.RooFit.PrintLevel(-1))
    fit_total.model.plotOn(massframe, Name="fit", LineColor=rt.kRed)
    fit_total.model.plotOn(massframe, Components={fit_total.bkg()}, LineColor=rt.kGreen, LineStyle='--')
    yld = fit_total.nsig.getVal()
    error = fit_total.nsig.getError()
    print(f"Signal yield: {yld:n} +/- {error:n} events")
    return massframe, yld, error

f = rt.TFile.Open("../data_in/eta_all.root")
h_mass = f.Get("mass")

c1, _ = get_canvas_and_frame(0.2, 0.1, 1.4, 3e6, "m_{#mu#mu} [GeV]", "Events")
h_mass.Draw("SAME")
c1.Update()

c1_2, _ = get_canvas_and_frame(0.2, 0, 1.4, 50, "m_{#mu#mu} [GeV]", "p^{#mu#mu}_{T} [GeV]", dim=3) 
h_mass_pt = f.Get("mass_pt")
h_mass_pt.GetZaxis().SetTitle("Events")
h_mass_pt.GetZaxis().SetTitleOffset(1.25)
h_mass_pt.Draw("SAME COLZ")
c1_2.Update()

f_fit = rt.TFile.Open('../data_in/twoMuon_data.root')
tt = f_fit.Get("Events")
selection = "mass>0.1 && nVtx>0 && nMuons==2 && nGood>1"
hTwoMu = rt.TH1D("hTwoMu", "hTwoMu", 100, 0.5, 0.6)
tt.Draw("mass>>hTwoMu", selection, "")

c3 = rt.TCanvas()
massframe, yld, error = fit_2mu_signal(hTwoMu)
massframe.Draw()
massframe.GetXaxis().SetNdivisions(510)
massframe.SetMinimum(1.0e6)
massframe.SetMaximum(1.8e6)
massframe.GetYaxis().SetTitleOffset(1.5)
setCMSLumiStyle(c3, 10, era='scouting')

leg3 = rt.TLegend(0.35, 0.7, 0.83, 0.9)
leg3.AddEntry("data", "Muon scouting data", "lep")
leg3.AddEntry("fit", "Double-Gaussian + 2-param. Chebychev", "l")
leg3.AddEntry("", f"Signal yield = {yld:n} events", "")
leg3.Draw()
c3.Update()


f2 = rt.TFile.Open("../data_in/fourMuon_data_v4.root")
tt = f2.Get("Events")

c4, _ = get_canvas_and_frame(0.46, 0, 0.9, 120, "m_{4#mu} [GeV]", "Events")

minX = 0.46+0.00275
maxX = 0.9+0.00275
h_4mu = rt.TH1D("h_4mu", "h_4mu", 80, minX, maxX)
h_4mu_nGood = rt.TH1D("h_4mu_nGood", "h_4mu_nGood", 80, minX, maxX)
h_4mu_nGood.SetLineColor(rt.kBlack)
h_4mu_nGood.SetMarkerColor(rt.kBlack)
h_4mu_nBad = rt.TH1D("h_4mu_nBad", "h_4mu_nBad", 80, minX, maxX)
h_4mu_nBad.SetLineColor(rt.kRed)
h_4mu_nBad.SetLineWidth(2)
tt.Draw("mass>>h_4mu", "mass<1.2 && nGood>-1 && charge_4mu==0", "SAME")
leg4 = rt.TLegend(0.25, 0.6, 0.55, 0.8)
leg4.AddEntry(h_4mu, "Scouting dataset", "l")
leg4.Draw()
c4.Update()

c5, _ = get_canvas_and_frame(0.46+0.00275, 0, 0.9+0.00275, 60, "m_{4#mu} [GeV]", "Events")
tt.Draw("mass>>h_4mu_nGood", "mass<1.2 && nGood>3 && charge_4mu==0", "SAME EP E0")
tt.Draw("mass>>h_4mu_nBad", "(0.15)*(mass<1.2 && nGood<=3 && charge_4mu==0)", "SAME HIST")
leg5 = rt.TLegend(0.25, 0.6, 0.7, 0.8)
leg5.AddEntry(h_4mu_nGood, "Matched #mu >= 4", "lep")
leg5.AddEntry(h_4mu_nBad, "Matched #mu < 4 (x0.15)", "l")
leg5.Draw()
c5.Update()

f3 = rt.TFile.Open(cfg.fns['four_mu_ntuple_MC'])
t3 = f3.Get("Events")

c6, _ = get_canvas_and_frame(0.02, 10, 0.12, 1e4, "Reconstructed m^{2}_{#mu_{i}#mu_{j}} [GeV^{2}]", "Events")
c6.SetLogy()
h_M12 = rt.TH1D("h_M12", "h_M12", 50, 0.02, 0.12)
h_M34 = rt.TH1D("h_M34", "h_M34", 50, 0.02, 0.12)
h_M12.SetLineColor(rt.kBlue)
h_M34.SetLineColor(rt.kMagenta)
t3.Draw("mass_12*mass_12>>h_M12", "", "SAME")
t3.Draw("mass_34*mass_34>>h_M34", "", "SAME")
leg6 = rt.TLegend(0.7, 0.7, 0.8, 0.9)
leg6.AddEntry(h_M12, "m^{2}_{#mu_{1}#mu_{2}}", "l")
leg6.AddEntry(h_M34, "m^{2}_{#mu_{3}#mu_{4}}", "l")
leg6.Draw()
c6.Update()

c7, _ = get_canvas_and_frame(0.46, 0, 1.2, 500, "m_{4#mu} [GeV]", "Events")
h_4mu_long = rt.TH1D("h_4mu_long", "h_4mu_long", 100, 0.46, 1.2)
h_4mu_long_nGood = rt.TH1D("h_4mu_long_nGood", "h_4mu_long_nGood", 50, 0.46, 1.2)
tt.Draw("mass>>h_4mu_long", "mass<1.2 && nGood>-1", "SAME")
leg7 = rt.TLegend(0.25, 0.6, 0.55, 0.8)
leg7.AddEntry(h_4mu_long, "Scouting dataset", "l")
leg7.Draw()
c7.Update()

c8, _ = get_canvas_and_frame(0.46, 0, 1.2, 300, "m_{4#mu} [GeV]", "Events")
tt.Draw("mass>>h_4mu_long_nGood", "mass<1.2 && nGood>3", "SAME EP")
leg8 = rt.TLegend(0.25, 0.65, 0.65, 0.8)
leg8.AddEntry(h_4mu_long, "Scouting dataset, 4 good #mu", "lep")
leg8.Draw()
fit = rt.TF1("GausPlusPol2", "gaus(0)+pol2(3)+gaus(6)", 0.6, 1.2)
fit.SetParameters(45.8, 0.94, 0.014, 106.6, -411.1, 425.3, 10.0, 0.547, 0.014)
h_4mu_long_nGood.Fit(fit)
fit.Draw("SAME")
c8.Update()

c9, frame9 = get_canvas_and_frame(0.537, 0, 0.557, 70000, "Generated m_{4#mu} [GeV]", "Events")
frame9.GetYaxis().SetTitleOffset(1.5)
frame9.GetXaxis().SetNdivisions(505)
t4 = f3.Get("GenEvents")
h_genMass = rt.TH1D("h_genMass", "h_genMass", 50, 0.537, 0.557)
t4.Draw("mass_gen_eta>>h_genMass", "", "SAME")
leg9 = rt.TLegend(0.20, 0.65, 0.45, 0.8)
leg9.AddEntry(h_genMass, "#eta #rightarrow 4#mu MC simulation", "l")
leg9.Draw()
c9.Update()

c10, _ = get_canvas_and_frame(0, 0, 70, 1700, "Generated p_{T} [GeV]", "Events")
h_genPt = rt.TH1D("h_genPt", "h_genPt", 70, 0, 70)
t4.Draw("pt_gen_eta>>h_genPt", "", "SAME")
leg10 = rt.TLegend(0.45, 0.75, 0.75, 0.85)
leg10.AddEntry(h_genMass, "#eta #rightarrow 4#mu MC simulation", "l")
leg10.Draw()
c10.Update()