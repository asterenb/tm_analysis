import ROOT as rt
import utils.CMSStyle as CMSStyle
import utils.configuration as configuration
cfg = configuration.Configuration()
CMSStyle.setTDRStyle()

# f_fits = rt.TFile.Open(config.two_mu_fits)
# w10 = f_fits.Get("bin10/w")
# w28 = f_fits.Get("bin28/w")

# mass = rt.RooRealVar("mass", "M_{#mu#mu} [GeV]", 0.52, 0.57)
# data10 = w10.data('dh').createHistogram("hist1", mass)
# data28 = w28.data('dh').createHistogram("hist2", mass)

# data10.Scale(10.0)
# data28.Scale(15.0)

# frame = mass.frame(rt.RooFit.Title(""))

f_total = rt.TFile.Open(cfg.fns['two_mu_ntuple_data'])
t = f_total.Get("Events")
h_mass = rt.TH1D("h_mass", "h_mass", 25, 0.517, 0.577)
h_bin10 = rt.TH1D("h_bin10", "h_bin10", 25, 0.517, 0.577)
h_bin28 = rt.TH1D("h_bin28", "h_bin28", 25, 0.517, 0.577)

t.Draw("mass>>h_mass", "mass>0.5 && mass<0.6 && nMuons==2 && nGood>1", "GOFF")
t.Draw("mass>>h_bin10", "mass>0.5 && mass<0.6 && p_t > 10 && p_t < 11 && nMuons==2 && nGood>1", "GOFF")
t.Draw("mass>>h_bin28", "mass>0.5 && mass<0.6 && p_t > 28 && p_t < 29 && nMuons==2 && nGood>1", "GOFF")

c1 = rt.TCanvas()
c1.SetTicks()
frame = c1.DrawFrame(0.517, 1.1e6, 0.577, 5e6)
frame.GetXaxis().SetNdivisions(508)
frame.GetXaxis().SetTitle("m_{#mu#mu} [GeV]")
frame.GetYaxis().SetTitleOffset(1.6)
frame.GetYaxis().SetTitle("Events / 0.05 GeV")

h_mass.Draw("SAME HIST P E")

h_bin10.Scale(7.5)
h_bin10.SetMarkerColor(rt.kBlue)
h_bin10.SetLineColor(rt.kBlue)
h_bin10.Draw("SAME HIST P E")

h_bin28.Scale(150.0)
h_bin28.SetMarkerColor(rt.kRed)
h_bin28.SetLineColor(rt.kRed)
h_bin28.Draw("SAME HIST P E")

# frame.Draw("SAME")

leg = rt.TLegend(0.47, 0.68, 0.79, 0.86)
leg.AddEntry(h_mass, "All p_{T}^{#mu#mu}", "lep")
leg.AddEntry(h_bin10, "10 < p_{T}^{#mu#mu} < 11 GeV (x7.5)", "lep")
leg.AddEntry(h_bin28, "28 < p_{T}^{#mu#mu} < 29 GeV (x150)", "lep")
leg.Draw()

# Print CMS logo and lumi
CMSStyle.setCMSLumiStyle(c1, 10, era='scouting')

c1.Update()