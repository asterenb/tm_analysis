import ROOT as rt
import utils.CMSStyle as CMSStyle
from utils.fitter import fitter_2mu as fitter
import utils.configuration as configuration
cfg = configuration.Configuration()
CMSStyle.setTDRStyle()

c1, frame = CMSStyle.get_canvas_and_frame(0, 0.54, 100, 0.56, "p_{T}^{#mu#mu} [GeV]", "m_{#mu#mu} [GeV]")
frame.GetYaxis().SetTitleOffset(1.7)
f = rt.TFile.Open(cfg.fns['fit_masses'])
g = f.Get("Graph")
fit = rt.TF1('fit', 'pol1',  10,  90)
g.Fit(fit, "R")
g.Draw("SAME P E0 Z")
fit.Draw("SAME")

leg = rt.TLegend(0.4, 0.65, 0.80, 0.85)
leg.AddEntry(g, "Mean mass per p_{T} bin", "lep")
leg.AddEntry(fit, "Linear fit (10-90 GeV p_{T}^{#mu#mu})", "l")
leg.AddEntry("", "constant = (547.32 #pm 0.05) MeV", "")
leg.AddEntry("", "slope = (-0.0054 #pm 0.003) MeV", "")
leg.Draw()

c1.Update()