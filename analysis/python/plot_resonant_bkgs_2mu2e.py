import ROOT as rt
import utils.CMSStyle as CMSStyle

CMSStyle.setTDRStyle()

modes = ['2mu2e', 'pipiee', 'mumugamma', 'pipipi0', '3pi0',   \
    'pipigamma', 'gammagamma', 'eegamma']

def lbl(*parts):
    greek = {'pp': '#pi^{+}', 'pm': '#pi^{-}', 'p': '#pi^{0}',
     'mp': '#mu^{+}', 'mm': '#mu^{-}', 'g': '#gamma', 'ep': 'e^{+}', 'em': 'e^{-}'}
    label = '#eta #rightarrow'
    for p in parts:
        label += f' {greek[p]}'
    return label

titles = [lbl('mp', 'mm', 'ep', 'em'), lbl('pp', 'pm', 'ep', 'em'), \
    lbl('mp', 'mm', 'g'), lbl('pp', 'pm', 'p'), lbl('p', 'p', 'p'), \
    lbl('pp', 'pm', 'g'), lbl('g', 'g'), lbl('ep', 'em', 'g')] 

markers = [rt.kFullSquare, rt.kFullTriangleUp, rt.kFullStar, \
    rt.kFullCrossX, rt.kFullTriangleDown, \
    rt.kFullDoubleDiamond, rt.kFullCircle, rt.kFullCross]

colors = [rt.kRed, rt.kCyan, rt.kBlue, rt.kYellow, \
     rt.kMagenta, rt.kGreen, rt.kOrange, rt.kBrownCyan]

sizes = [0.8, 0.9, 1.0, 0.9, 0.9, 1.1, 0.8, 1.0]

files = {}
for mode in modes:
    files[mode] = rt.TFile.Open(f'../../toyMC/eta/{mode}_2mu2e.root')

hists = {}
for m, f in files.items():
    hists[m] = f.Get("hMass2")

c1 = rt.TCanvas()
c1.SetLogy()
c1.SetTicks()
#frame = c1.DrawFrame(0.4, 1e-7, 0.8, 1e5)
frame = c1.DrawFrame(0.45, 1e-7, 0.8, 1e5)
frame.GetXaxis().SetTitle("m_{2#mu2e} [GeV]")
#frame.GetYaxis().SetTitle("Events / 0.01 GeV")
frame.GetYaxis().SetTitle("Events / 5 MeV")

leg = rt.TLegend(0.55, 0.6, 0.79, 0.87)
# leg.SetBorderSize(1)
for i, h in enumerate(hists.values()):
    h.SetTitle(titles[i])
    h.Rebin()
    h.SetMarkerColor(colors[i]-3)
    h.SetLineColor(colors[i]-3)
    h.SetMarkerStyle(markers[i])
    h.SetMarkerSize(sizes[i]+0.2)
    h.Draw("SAME HIST P")
    leg.AddEntry(h, h.GetTitle(), "p")

leg.Draw()

# Print CMS logo and lumi
#CMSStyle.setCMSLumiStyle(c1, 10, era='Run2')
CMSStyle.setCMSLumiStyle(c1, 10, era='2022')

c1.RedrawAxis()
c1.Update()
input("press enter to exit.")
