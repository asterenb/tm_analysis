
# estimate the effect of shifting the mass scale on the eta peak position

from ROOT import TFile, TTree, TH1D, TH2D, TLorentzVector, TCanvas, TLegend
from ROOT import kBlue, kRed, kFALSE
import numpy as np 
from math import sqrt 

inFile = TFile.Open("fourMuon_000.root")
inTree= inFile.Get("Events")
genTree = inFile.Get("GenEvents")
inTree.AddFriend(genTree)

nentries = inTree.GetEntries()
print("nentries={0:d}".format(nentries))
 
fOut = TFile('pScale_hist.root', 'recreate' )

hMass = TH1D("hMass","hMass",100,0.45,0.65)
hMass.GetXaxis().SetTitle("m_{4#mu} GeV")
hMass.GetYaxis().SetTitle("Counts per 0.002 GeV")

verbose = 0 
muon, muon_gen = [], []
for i in range(4) : 
    muon.append(TLorentzVector())
    muon_gen.append(TLorentzVector())

scale = 1.00
for count, e in enumerate(inTree) :
    if e.nMuons < 4 : continue 

    muon[0].SetPtEtaPhiM(scale*e.pt_1,e.eta_1,e.phi_1,0.107)
    muon[1].SetPtEtaPhiM(scale*e.pt_2,e.eta_2,e.phi_2,0.107)
    muon[2].SetPtEtaPhiM(scale*e.pt_3,e.eta_3,e.phi_3,0.107)
    muon[3].SetPtEtaPhiM(scale*e.pt_4,e.eta_4,e.phi_4,0.107)

    muon_gen[0].SetPtEtaPhiM(e.pt_gen_1,e.eta_gen_1,e.phi_gen_1,0.107)
    muon_gen[1].SetPtEtaPhiM(e.pt_gen_2,e.eta_gen_2,e.phi_gen_2,0.107)
    muon_gen[2].SetPtEtaPhiM(e.pt_gen_3,e.eta_gen_3,e.phi_gen_3,0.107)
    muon_gen[3].SetPtEtaPhiM(e.pt_gen_4,e.eta_gen_4,e.phi_gen_4,0.107)

    mass = (muon[0] + muon[1] + muon[2] + muon[3]).M()
    hMass.Fill(mass)
            
    
fOut.cd()
fOut.Write()
fOut.Close()        
