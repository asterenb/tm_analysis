import ROOT as rt
import utils.configuration as configuration
cfg = configuration.Configuration()

f = rt.TFile.Open(cfg.fns['two_mu_ntuple_data'])
t = f.Get("Events")

pt_edges = cfg.pt_edges
n_mass_bins = cfg.n_mass_bins

# Draw the 2D pT vs mass plot into the histogram
h_mass_pt = rt.TH2D("h_mass_pt", "h_mass_pt", n_mass_bins, 0.5, 0.6, len(pt_edges)-1, pt_edges.astype(float))
t.Draw("p_t:mass>>h_mass_pt", "mass>0.1 && nVtx>0 && nMuons==2 && nGood>1", "COLZ GOFF")

# Open file to store all the histograms
f_out = rt.TFile.Open(cfg.fns['m2mu_vs_pt2mu'], "RECREATE")

# Make plots for each bin of pT
for i in range(1, h_mass_pt.GetNbinsY()+1):
    h_projX = h_mass_pt.ProjectionX(f"{i}", i, i)
    h_projX.SetName("h_m2mu")

    f_out.mkdir(f"bin{i}")
    f_out.cd(f"bin{i}")
    h_projX.Write()

# Now plot across the entire pT spectrum
f_out.mkdir(f"binAll")
f_out.cd(f"binAll")

h_projX = h_mass_pt.ProjectionX(f"{i}", 0, -1)
h_projX.SetName("h_m2mu")
h_projX.Write()

f_out.Close()