import ROOT as rt
import utils.CMSStyle as CMSStyle
import utils.histos_from_csv as histos
from uncertainties import ufloat
import utils.configuration as configuration
cfg = configuration.Configuration()


CMSStyle.setTDRStyle()

only_uncorrected = False

# First get the histos from the utils library
h_corr_xsec = histos.get_corrected_xsec()
h_uncorr_xsec = histos.get_uncorrected_xsec()

# Plot the corrected and uncorrected histos
c1 = rt.TCanvas()
c1.SetLogy()
c1.SetLogx()
c1.SetTicks()
frame = c1.DrawFrame(5, 1e4, 99, 1e14)
frame.GetXaxis().SetMoreLogLabels()
frame.GetXaxis().SetTitle("#eta meson p_{T} [GeV]")
frame.GetXaxis().SetTitleOffset(1.4)
frame.GetYaxis().SetTitle("#frac{d#sigma}{dp_{T}} [fb GeV^{-1}]")
frame.GetYaxis().SetTitleOffset(1.7)

if only_uncorrected is False:
    h_corr_xsec.SetLineWidth(2)
    h_corr_xsec.SetMarkerColor(rt.kGreen-1)
    h_corr_xsec.SetLineColor(rt.kGreen-1)
    h_corr_xsec.SetMarkerStyle(rt.kFullCircle)
    h_corr_xsec.Draw("SAME E")
h_uncorr_xsec.SetLineWidth(2)
h_uncorr_xsec.SetMarkerColor(rt.kPink-9)
h_uncorr_xsec.SetLineColor(rt.kPink-9)
h_uncorr_xsec.SetMarkerStyle(rt.kFullCircle)
h_uncorr_xsec.Draw("SAME E")

if only_uncorrected is False:
    power_fit = rt.TF1("power_fit", "[0]*x^[1]", 6, 100)
    power_fit.SetParameters(6.22e14, -5)
    power_fit.SetParameter(1, -5)
    h_corr_xsec.Fit(power_fit, "R")
    fit_result0 = ufloat(power_fit.GetParameter(0), power_fit.GetParError(0))
    fit_result1 = ufloat(power_fit.GetParameter(1), power_fit.GetParError(1))
    power_fit.SetLineColor(rt.kRed+1)
    power_fit.SetLineWidth(3)
    power_fit.Draw("SAME")

if only_uncorrected is True:
    leg = rt.TLegend(0.35, 0.65, 0.85, 0.8)
else:
    leg = rt.TLegend(0.35, 0.6, 0.85, 0.85)
leg.AddEntry(h_uncorr_xsec, h_uncorr_xsec.GetTitle(), "lep")
if only_uncorrected is False:
    leg.AddEntry(h_corr_xsec, h_corr_xsec.GetTitle(), "lep")
    leg.AddEntry(power_fit, "Fit: p_{0} / p_{T}^{p_{1}}", "l")
    leg.AddEntry("", f" -- p_{{0}}: {fit_result0}", "")
    leg.AddEntry("", f" -- p_{{1}}: {fit_result1}", "")
leg.SetLineWidth(0)
leg.Draw()

# Print CMS logo and lumi
CMSStyle.setCMSLumiStyle(c1, 10, era='scouting')

# Redraw axis on top and update canvas
c1.RedrawAxis()
c1.Update()

f_out = rt.TFile.Open(cfg.fns['xsecs'], "RECREATE")
h_corr_xsec.Write()
h_uncorr_xsec.Write()
power_fit.Write()
f_out.Close()