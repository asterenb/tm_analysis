from typing import OrderedDict
import ROOT as rt
import numpy as np
import utils.histos_from_csv as csv_utils
import utils.configuration as configuration
cfg = configuration.Configuration()

N_2mu_baseline, header = csv_utils.read_csv(cfg.fns['two_mu_csv'])

N_2mu_systs = {}
for k,v in cfg.fns['fit_systs'].items():
    N_2mu_systs[k] = csv_utils.read_csv(v)

maxs, mins = {}, {}
for i in range(1, len(N_2mu_systs['Voigtian_Cheb4'][0])+1):
    maxs[i] = -1e10
    mins[i] = 1e10
    for data in N_2mu_systs.values():
        N = float(data[0][i-1][4])
        chi2 = float(data[0][i-1][6])
        if abs(chi2-1.0) < 0.2 and chi2 > 0.0:
            maxs[i] = max(maxs[i], N)
            mins[i] = min(mins[i], N)

diffs = {k: abs(maxs[k]-mins[k])/2 if abs(maxs[k]-mins[k])<1e10 else 0. for k in maxs.keys()}

header.append('N_2mu_syst_error')
for i, row in enumerate(N_2mu_baseline):
    row.append(str(diffs[i+1]))

csv_utils.write_csv(cfg.fns['two_mu_csv_with_syst'], N_2mu_baseline, header)
