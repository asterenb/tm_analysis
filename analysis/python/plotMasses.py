#! /usr/bin/env python
from ROOT import TFile, TH1D, TCanvas, TLorentzVector, gPad
import sys
from math import sqrt
import numpy as np

def getArgs() :
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("-v","--verbose",default=0,type=int,help="Print level.")
    parser.add_argument("-f","--inFileName",default='data1.root',help="File to be analyzed.")
    parser.add_argument("-m","--maxPrint",default=0,type=int,help="Maximum number of events to print.")
    return parser.parse_args()
         
def goodVertex(e,j) :
    sum, div = 0, 2**j
    for k in range(4) :
        if (e.vtxList[k]/div % 2 == 1) : sum += int(e.charge[k])   
    return sum == 0 

# begin execution here 

args = getArgs()
nPrint = args.maxPrint 

inFile = TFile(args.inFileName) 
inFile.cd()
t = inFile.Get("Events")
nentries = t.GetEntries()
print("nEntries={0:d}".format(nentries))

h_m13 = TH1D("h_m13","h_m13",60,0.2,0.35)
h_m24 = TH1D("h_m24","h_m24",60,0.2,0.35)
h_m14 = TH1D("h_m14","h_m14",60,0.2,0.35)
h_m23 = TH1D("h_m23","h_m23",60,0.2,0.35)

h_lowMass = TH1D("h_lowMass","h_lowMass",60,0.2,0.35)
h_highMass = TH1D("h_highMass","h_highMass",60,0.2,0.35)

for i, e in enumerate(t) :
    #if e.mass > 0.56 or e.mass < 0.54 : continue
    if e.mass > 0.62 or e.mass < 0.56 : continue
    if e.nGood < 4 : continue
    if e.nGood < 4 : continue
    muons, chargeList = [], []    
    for j in range(4) :
        muons.append(TLorentzVector())
        muons[j].SetPtEtaPhiM(e.pt[j], e.eta[j], e.phi[j], 0.1057)
        chargeList.append(e.charge[j])

    # this will produce a list where the two negative charge muons come first
    # sdx = sorted index
    sdx= np.argsort(np.array(chargeList))
    if ((e.charge[sdx[0]] + e.charge[sdx[1]] > -1.5) or 
        (e.charge[sdx[2]] + e.charge[sdx[3]] <  1.5)) :
        print("Charge error for event={0:i} chargeList={1:s} sortIndex={2:s}".format(i,str(chargeList), str(sortIndex)))

    # there are two possible pairings 
    pair13, pair24 = muons[sdx[0]] + muons[sdx[2]], muons[sdx[1]] + muons[sdx[3]]
    m13, m24 = pair13.M(), pair24.M()
    mSum13 = m13 + m24
    pair14, pair23 = muons[sdx[0]] + muons[sdx[3]], muons[sdx[1]] + muons[sdx[2]]
    m14, m23 = pair14.M() , pair23.M()
    mSum14 = m14 + m23
    if mSum13 < mSum14 :
        if m13 < m24 : lowMass, highMass  = m13, m24
        else : lowMass, highMass  = m24, m13
    else :
        if m14 < m23 : lowMass, highMass  = m14, m23
        else : lowMass, highMass  = m23, m14

    h_m13.Fill(m13)
    h_m24.Fill(m24)
    h_m14.Fill(m14)
    h_m23.Fill(m23)
    h_lowMass.Fill(lowMass)
    h_highMass.Fill(highMass)

    if nPrint > 0  :
        nPrint -= 1
        print("\nMass={0:10.3f} nVtx={1:2d} nGood={2:3d}".format(e.mass,e.nVtx,e.nGood))
        print("Muons\n  #  Q    pt      eta     phi     vtx")
        for j in range(4) :
            print("{0:3d}{1:3d}{2:8.3f}{3:8.3f}{4:8.3f}{5:8b}".format(j,int(e.charge[j]),e.pt[j],e.eta[j],e.phi[j],e.vtxList[j])) 
    
        print("Vertices:\n  #     x       y       z      chi2  good".format(e.nVtx))
        for j in range(min(e.nVtx,4)) :
            print("{0:3d}{1:8.4f}{2:8.4f}{3:8.4f}{4:8.2f} {5}".format(j,e.vtx_x[j],e.vtx_y[j],e.vtx_z[j],e.vtx_chi2[j],goodVertex(e,j)))

        print("Sorted index = {0:s}".format(str(sdx)))
        print(" m13={0:7.3f} m24={1:7.3f} m14={2:7.3f} m23={3:7.3f} lowMass={4:7.3f} highMass={5:7.3f}".format(
            m13, m24, m14, m23, lowMass, highMass)
        )


# Draw the histograms
c1 = TCanvas("c1","Muon Mass Distributions",1200,700)
c1.Divide(2,2)
c1.cd(1) 
h_m13.Draw()
h_m13.GetXaxis().SetTitle("m13 GeV")
c1.cd(2)
h_m24.Draw()
h_m24.GetXaxis().SetTitle("m24 GeV")
c1.cd(3)
h_m14.Draw()
h_m14.GetXaxis().SetTitle("m14 GeV")
c1.cd(4)
h_m23.Draw()
h_m23.GetXaxis().SetTitle("m23 GeV")
c1.Update()
print("Enter <CR> to continue") 
raw_input()

# Draw the histograms
c2 = TCanvas("c1","Muon Mass Distributions",1200,700)
c2.Divide(2,1)
c2.cd(1) 
h_lowMass.Draw()
h_lowMass.GetXaxis().SetTitle("lower mass GeV")
c2.cd(2)
h_highMass.Draw()
h_highMass.GetXaxis().SetTitle("higher mass GeV")
c2.Update()
print("Enter <CR> to continue") 
raw_input()




