from posixpath import devnull
from ROOT import TFile, TTree, TH1D, TH2D, TLorentzVector, TCanvas, TLegend, TGraphErrors
from ROOT import kBlue, kRed, kFALSE
from array import array
from math import sqrt
import sys

fileName = "fourMuon_hist.root"
if len(sys.argv) > 1 : fileName = sys.argv[1] 

inFile = TFile.Open(fileName)
hmuonPt_all = inFile.Get("hmuonPt_all")
hmuonPt_good = inFile.Get("hmuonPt_good")

c1 = TCanvas("c1")
c1.cd()
#c1.SetLogy()
hmuonPt_all.Draw()
hmuonPt_good.Draw("SAME") 
c1.Update()
input("<CR> to continue")  

# compute the efficiency curve
nBins = hmuonPt_all.GetNbinsX()
xx, yy, ex, ey = array('d'), array('d'),  array('d'), array('d') 
for i in range(nBins-1)  :
    num, den = hmuonPt_good.GetBinContent(i+1), hmuonPt_all.GetBinContent(i+1)
    if den > 0. :
        eff = num/den
        err = sqrt(eff*(1.-eff)/den)
        x, y, e = hmuonPt_all.GetBinCenter(i+1), eff, err
        if y > 0. :
            xx.append(x)
            yy.append(y)
            ex.append(0.) 
            ey.append(e)

g = TGraphErrors(len(xx),xx,yy,ex,ey)
g.SetTitle("Reconstruction Efficiency vs. Muon p_{t}")
g.GetXaxis().SetTitle("Muon p_{t} (GeV)")
g.GetYaxis().SetTitle("Track Efficiency")
g.SetMarkerStyle(20)
g.SetMarkerColor(kRed)
g.Draw("AP") 
c1.Update()
input("<CR> to continue")  





