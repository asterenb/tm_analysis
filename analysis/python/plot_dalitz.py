import ROOT as rt
import utils.CMSStyle as CMSStyle
import utils.configuration as configuration
cfg = configuration.Configuration()

CMSStyle.setTDRStyle()

f = rt.TFile.Open(cfg.fns['four_mu_ntuple_data'])
tt = f.Get("Events")

h = rt.TH1D("hFourMuons", "hFourMuons", 10, 0.53, 0.57)
h.SetMarkerColor(rt.kBlack)
h.SetMarkerStyle(20)
h.SetMarkerSize(0.6)
h.SetLineColor(rt.kBlack)
# h.SetLineWidth(0)
# Earlier versions of the ntuple:
# selection = "invMass > 0.1 && invMass < 1.0 && nGood > 3"
selection = "mass > 0.53 && mass < 0.57 && nGood > 3 && nVtx > 0 && charge_4mu == 0"
tt.Draw("mass>>hFourMuons", selection, "GOFF")
df = rt.RDataFrame(tt)
df = df \
    .Filter(selection) \
    .Define("px", "pt*cos(phi)") \
    .Define("py", "pt*sin(phi)") \
    .Define("et", "sqrt(pt*pt + 0.105*0.105)") \
    .Define("m12", "2*0.105*0.105 + 2*(et[0]*et[1]*cosh(eta[0]-eta[1]) - px[0]*px[1] - py[0]*py[1])") \
    .Define("m34", "2*0.105*0.105 + 2*(et[2]*et[3]*cosh(eta[2]-eta[3]) - px[2]*px[3] - py[2]*py[3])") \
    
hist2D = df.Histo2D(("dalitz2D", "dalitz2D", 50, 0, .13, 50, 0, .13), "m12", "m34")
hist_m12 = df.Histo1D(("m12", "m12", 6, 0.02, .12), "m12")
hist_m34 = df.Histo1D(("m34", "m34", 6, 0.02, .12), "m34")
    # .Define("m34", "2*0.105*0.105 + 2*pt[2]*pt[3]*(cosh(eta[2]-eta[3])-cos(phi[2]-phi[3]))") \

hist2D.GetXaxis().SetTitle("m_{12}^{2} [GeV^{2}]")
hist2D.GetYaxis().SetTitle("m_{34}^{2} [GeV^{2}]")
hist2D.GetYaxis().SetTitleOffset(1.5)
c1 = rt.TCanvas()
c1.SetTicks()
hist2D.Draw()

# Draw CMS logo and lumi
CMSStyle.setCMSLumiStyle(c1, 10, era='scouting')

c1.Update()

c2 = rt.TCanvas()
c2.SetTicks()
c2.SetLogy()
frame = c2.DrawFrame(0.02, 0.5, 0.12, 1000)
frame.GetXaxis().SetTitle("Reconstructed m_{ij}^{2} [GeV^{2}]")
frame.GetYaxis().SetTitle("Events")
hist_m12.SetMarkerColor(rt.kBlue)
hist_m12.SetLineColor(rt.kBlue)
hist_m12.Draw("SAME PE")
hist_m34.SetMarkerColor(rt.kMagenta)
hist_m34.SetLineColor(rt.kMagenta)
hist_m34.Draw("SAME PE")

f3 = rt.TFile.Open("../data_in/fourMuon_MC.root")
t3 = f3.Get("Events")
t4 = f3.Get("GenEvents")
t3.AddFriend(t4, "gen")
h_M12 = rt.TH1D("h_M12", "h_M12", 6, 0.02, 0.12)
h_M34 = rt.TH1D("h_M34", "h_M34", 6, 0.02, 0.12)
h_M12.SetLineColor(rt.kBlue)
h_M12.SetLineWidth(2)
h_M34.SetLineColor(rt.kMagenta)
h_M34.SetLineWidth(2)
t3.Draw("mass_12*mass_12>>h_M12", "gen.weight*(mass > 0.46275 && mass < 0.90275 && nGood > 3)", "SAME HIST")
t3.Draw("mass_34*mass_34>>h_M34", "gen.weight*(mass > 0.46275 && mass < 0.90275 && nGood > 3)", "SAME HIST")

leg = rt.TLegend(0.5, 0.7, 0.9, 0.87)
leg.AddEntry("m12", "m_{12}^{2} data", "lep")
leg.AddEntry("m34", "m_{34}^{2} data", "lep")
leg.AddEntry(h_M12, "m_{12}^{2} MC", "l")
leg.AddEntry(h_M34, "m_{34}^{2} MC", "l")
leg.Draw()

CMSStyle.setCMSLumiStyle(c2, 10, era='scouting')

c2.RedrawAxis()
c2.Update()