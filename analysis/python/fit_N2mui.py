from typing import OrderedDict
import ROOT as rt
import numpy as np
import csv, pprint
from utils.fitter import fitter_2mu as fitter
import utils.configuration as configuration
cfg = configuration.Configuration()

f = rt.TFile.Open(cfg.fns['m2mu_vs_pt2mu'])

low_high_pt_split = cfg.low_high_pt_split
pt_edges = cfg.pt_edges

pt_centers = [(pt_edges[i]+pt_edges[i+1])/2 for i in range(len(pt_edges)-1)]

# Declare observable mass
mass = rt.RooRealVar("mass", "m_{#mu#mu} [GeV]", 0.51, 0.59)

# Open file to store all the fits
f_out = rt.TFile.Open(cfg.fns['two_mu_fits'], "RECREATE")

fit_very_low_pt = fitter(mass, bkg_model='Cheb3', sig_model='DoubleGauss')
fit_medium_pt = fitter(mass, bkg_model='Cheb3', sig_model='DoubleGauss')
fit_high_pt = fitter(mass, bkg_model='Cheb2', sig_model='DoubleGauss')
fit_total = fitter(mass, bkg_model='Cheb3', sig_model='DoubleGauss')

# For each pT bin, fit the invMass and add the yield and errors to the histos
tot_yields = 0.
yields, errors = {}, {}
masses = rt.TGraphErrors("fit_masses")
for i in range(1, len(pt_centers)+1):
    if pt_centers[i-1] < 5:
        yld, error = 0., 0.
        yields[i] = yld
        errors[i] = error
        continue
    h_projX = f.Get(f"bin{i}/h_m2mu")
    massframe = mass.frame(rt.RooFit.Title(f"Signal + background fit for p_{{T}} in [{pt_edges[i-1]}, {pt_edges[i]}] GeV"))
    dh = rt.RooDataHist("dh", "dh", mass, rt.RooFit.Import(h_projX))
    dh.plotOn(massframe, rt.RooFit.DrawOption("E"), rt.RooFit.LineColor(rt.kRed), rt.RooFit.MarkerSize(0.3), rt.RooFit.MarkerColor(rt.kRed))
    dh.statOn(massframe, rt.RooFit.Layout(0.13, 0.53, 0.35))

    if pt_centers[i-1] > low_high_pt_split:
        fit = fit_high_pt
    elif pt_centers[i-1] < 7:
        fit = fit_very_low_pt
    else:
        fit = fit_medium_pt

    if str(i) in cfg.two_mu_fit_params.sig_model.keys():
            fit.set_sig_params(cfg.two_mu_fit_params.sig_model[str(i)])
    
    result = fit.model.fitTo(dh, rt.RooFit.Save())
    # fit the high-pT model twice to better converge
    if fit == fit_high_pt:
        fit.model.fitTo(dh, rt.RooFit.PrintLevel(-1))

    fit.model.plotOn(massframe)
    fit.model.plotOn(massframe, Components={fit.bkg()}, LineColor=rt.kGreen, LineStyle='--')
    fit.model.paramOn(massframe, rt.RooFit.Layout(0.55, 0.85, 0.7))
 
    masses.AddPoint(pt_centers[i-1], fit.mean_val())
    masses.SetPointError(masses.GetN()-1, masses.GetErrorX(masses.GetN()-1), fit.mean_err())
    # model.Print("t")
    # massframe.Draw()

    f_out.mkdir(f"bin{i}")
    f_out.cd(f"bin{i}")
    w = rt.RooWorkspace("w","workspace")
    w.Import(fit.model)
    w.Import(dh)
    w.Write()
    massframe.Write()
    result.Write()

    # Extract the signal yield and error from the fit
    yld = fit.nsig.getVal()
    yields[i] = yld
    tot_yields += yld
    error = fit.nsig.getError()
    errors[i] = error

# Now fit across the entire pT range
h_projX = f.Get("binAll/h_m2mu")
massframe = mass.frame(rt.RooFit.Title(f"Signal + background fit for all p_{{T}}"))
dh = rt.RooDataHist("dh", "dh", mass, rt.RooFit.Import(h_projX))
dh.plotOn(massframe, rt.RooFit.DrawOption("E"), rt.RooFit.LineColor(rt.kRed), rt.RooFit.MarkerSize(0.3), rt.RooFit.MarkerColor(rt.kRed))
dh.statOn(massframe, rt.RooFit.Layout(0.13, 0.53, 0.35))
fit = fit_total # fit_very_low_pt
fit.model.fitTo(dh, rt.RooFit.PrintLevel(-1))
fit.model.plotOn(massframe)
fit.model.plotOn(massframe, Components={fit.bkg()}, LineColor=rt.kGreen, LineStyle='--')
fit.model.paramOn(massframe, rt.RooFit.Layout(0.55, 0.85, 0.7))

c1 = rt.TCanvas()
massframe.Draw()
c1.Modified()
c1.Update()
f_out.mkdir(f"binAll")
f_out.cd(f"binAll")
w = rt.RooWorkspace("w","workspace")
w.Import(fit.model)
w.Import(dh)
w.Write()
massframe.Write()

f_masses = rt.TFile.Open(cfg.fns['fit_masses'], "RECREATE")
masses.Write()
f_masses.Close()

# f_out.Close()

# f_acc_nominal = rt.TFile.Open(config.two_mu_acc)
# g_acc_nominal = f_acc_nominal.Get("acceptance_EtaTo2Mu")
# acc_fit_nominal = f_acc_nominal.Get("accFit")

# f_acc_plus = rt.TFile.Open("../data_out/acceptance_EtaTo2Mu_plus10percent.root")# config.two_mu_acc.replace("EtaTo2Mu", "EtaTo2Mu_plus10percent"))
# g_acc_plus = f_acc_plus.Get("acceptance_EtaTo2Mu")
# acc_fit_plus = f_acc_plus.Get("accFit")

all_data = []
for i, pt in enumerate(pt_centers):
    i_bin = i + 1
    # acc = acc_fit_nominal.Eval(pt)
    # acc = g_acc_nominal.Eval(pt)
    # acc_error = abs(acc_fit_plus.Eval(pt) - acc)
    # acc_error = abs(g_acc_plus.Eval(pt) - acc)
    d = OrderedDict({
        'bin': i_bin, 
        'pt_center': pt,
        'pt_low': pt_edges[i],
        'pt_high': pt_edges[i+1],
        # 'A_2mu': acc,
        # 'A_2mu_error': acc_error,
        'N_2mu': yields[i_bin],
        'N_2mu_error': errors[i_bin]
    })
    all_data.append(d)

# f_out2 = rt.TFile.Open("../data_out/corrected_xsec.root", "RECREATE")
# g_corrXsec.Write()
# f_out2.Close()

# Write out information in csv format rather than histos
# (Use histo facility in utils for the histograms)
with open(cfg.fns['two_mu_csv'], 'w') as f:
    writer = csv.DictWriter(f, fieldnames=all_data[0].keys())
    writer.writeheader()
    writer.writerows(all_data)

pprint.pprint(all_data)
print("total signal yield: ", tot_yields)

tot_fit_yield = fit.nsig.getVal()
print(f"Total fit yield: {tot_fit_yield}")
