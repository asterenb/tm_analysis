import ROOT as rt
import utils.CMSStyle as CMSStyle
import utils.configuration as configuration

config = configuration.Configuration()

CMSStyle.setTDRStyle()

f = rt.TFile.Open(config.four_mu_ntuple_data)
tt = f.Get("Events")

h_signal = rt.TH1D("h_signal", "h_signal", 10, 0, 100)
h_sideband = rt.TH1D("h_sideband", "h_sideband", 10, 0, 100)

tt.Draw("pt_4mu>>h_signal", "mass>0.53 && mass<0.57 && nGood>3 && charge_4mu==0", "GOFF")
tt.Draw("pt_4mu>>h_sideband", "mass>0.6 && mass<0.9 && nGood>0 && charge_4mu==0", "GOFF")

c1 = rt.TCanvas()
frame = c1.DrawFrame(0, 0, 100, 1)
frame.GetXaxis().SetTitle("p_{T}^{4#mu} [GeV]")
frame.GetYaxis().SetTitle("A. U.")

h_sideband.SetMarkerColor(rt.kBlue+1)
h_sideband.SetLineColor(rt.kBlue+1)
h_sideband.DrawNormalized("SAME HIST")
h_sideband.DrawNormalized("SAME E0")

h_signal.SetMarkerColor(rt.kOrange+1)
h_signal.SetLineColor(rt.kOrange+1)
h_signal.DrawNormalized("SAME HIST")
h_signal.DrawNormalized("SAME E0")

leg = rt.TLegend(0.3, 0.6, 0.85, 0.8)
leg.AddEntry(h_signal, "m_{4#mu} #in [0.53, 0.57] GeV (signal window)", "lep")
leg.AddEntry(h_sideband, "m_{4#mu} #in [0.6, 0.9] GeV (sideband)", "lep")
leg.Draw()

CMSStyle.setCMSLumiStyle(c1, 10, era='scouting')

c1.RedrawAxis()
c1.Update()