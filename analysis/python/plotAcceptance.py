from ROOT import TCanvas, TFile, TLegend, TGraph
from ROOT import kBlue 
import glob
from array import array 

def plotGraph(g,plateauMode) :
    c1 = TCanvas("c1")
    c1.cd() 
    legend = TLegend(0.70,0.40,0.90,0.90) 
    legend.SetHeader("p_{t} threshold","C")  #  option "C" allows to center the header
    for i, key in enumerate(g.keys()) :
        colorCode, markerCode = (i % 4) + 1, 20 + int(i/4)  
        print("i={0:d} key={1:s}".format(i,key))
        g[key].SetMarkerColor(colorCode)
        g[key].SetMarkerStyle(markerCode)
        if i == 0 : 
            g[key].GetXaxis().SetLimits(0.,100.)
            g[key].Draw("AP")
        else : g[key].Draw("SAME P") 
        legend.AddEntry(g[key],key+ " GeV","P")
    legend.Draw()   
    c1.Update()
    input("<CR> to continue")  
    c1.Close() 

plateauMode = True

if plateauMode : files = glob.glob("./data_out/acceptance_t0.00*.root")
else : files = glob.glob("./data_out/acceptance*.root")

gA_2mu, gA_4mu, gTrack_2mu, gTrack_4mu = {}, {}, {}, {} 
xx, yy = array('d'), array('d')
for file in files :
    vals = file.split('/')[2].split("_")
    threshold = vals[1].lstrip("t")
    if plateauMode and float(threshold) > 0. : continue 
    plateau = vals[2].lstrip("p")
    N_4mu = vals[3].lstrip("n").rstrip(".root") 
    if plateauMode : xx.append(float(plateau))
    else : xx.append(float(threshold))
    yy.append(float(N_4mu))
    print("file={0:s} threshold={1:s} plateau={2:s} N_4mu={3:s}".format(file,threshold,plateau,N_4mu))
    inFile = TFile.Open(file)
    if plateauMode : key = plateau
    else : key = threshold 
    gA_2mu[key]= inFile.Get("gA_2mu")
    #gA_2mu[key].Draw("AP")
    #input("<CR> to continue")
    gA_4mu[key]= inFile.Get("gA_4mu")
    gTrack_2mu[key]= inFile.Get("gTrack_2mu")
    gTrack_4mu[key]= inFile.Get("gTrack_4mu")

if not plateauMode :
    plotGraph(gA_2mu,plateauMode)
    plotGraph(gA_4mu,plateauMode)
    #plotGraph(gTrack_2mu)
    #plotGraph(gTrack_4mu)

# make graph of yield vs p_t threshold (or plateau)
print("len(xx)={0:d} xx={1:s} yy={2:s}".format(len(xx),str(xx),str(yy))) 
g = TGraph(len(xx),xx,yy)
c1 = TCanvas("c1")
c1.cd() 
c1.SetGrid() 
g.SetMarkerColor(kBlue)
g.SetMarkerStyle(20) 
if plateauMode :
    g.SetTitle("Yield vs. Plateau Level")    
    g.GetXaxis().SetTitle("Single Track Relative Efficiency")
    g.GetYaxis().SetTitle("#eta #rightarrow 4 #mu yield")
    #g.GetXaxis().SetLimits(0.4,1.)    
else :
    g.SetTitle("Yield vs. Track Threshold")
    g.GetYaxis().SetTitle("#eta #rightarrow 4#mu Yield")
    g.GetXaxis().SetTitle("track threshold p_{t} (GeV)")
    g.GetXaxis().SetLimits(0.,10.)
g.Draw("AP")
c1.Update()
input("<CR> to continue")  
c1.Close() 





