import ROOT as rt
import utils.blinding as blinding
import utils.CMSStyle as CMSStyle
import utils.histos_from_csv as histos
import numpy as np
import utils.configuration as configuration
cfg = configuration.Configuration()

CMSStyle.setTDRStyle()

bin_edges = np.linspace(0, 100, 101)
h_N_4mu_MC_BRs_before = histos.get_4mu_predictions(include_acc=False, bin_edges=bin_edges)
h_N_4mu_MC_BRs_after = histos.get_4mu_predictions(include_acc=True, bin_edges=bin_edges)

# Pick one BR (middle one) to plot
h_before_acc = h_N_4mu_MC_BRs_before[1]
h_after_acc = h_N_4mu_MC_BRs_after[1]

# Plot the middle prediction now and write all of them to file later
h_before_acc.SetMarkerColor(415)
h_before_acc.SetLineColor(415)
h_after_acc.SetMarkerColor(rt.kOrange)
h_after_acc.SetLineColor(rt.kOrange)

# Load 4-mu acceptance to plot
f_acc = rt.TFile.Open(cfg.fns['four_mu_acc'])
g_acc = f_acc.Get("acceptance_EtaTo4Mu")
# Load but don't use fit for now --- incorrect at low pT:
# accFit = f_acc.Get("accFit")

# Make canvas
c1 = rt.TCanvas()
c1.SetLogy()
c1.SetTicks()

# Draw frame
frame = c1.DrawFrame(0, 0.1, 50, 1e6)
frame.GetXaxis().SetTitle("p_{T}^{4#mu} [GeV]")
frame.GetYaxis().SetTitle("Events / bin")

# Draw eta-->4mu yield before and after acceptance
h_before_acc.Draw("SAME")
h_after_acc.Draw("SAME")
# Draw acceptance and fit
g_acc.Scale(100.)
rt.gStyle.SetPalette(rt.kBlackBody)
rt.gROOT.ForceStyle()
g_acc.SetMarkerColor(1433)
g_acc.SetLineColor(1433)
g_acc.SetMarkerStyle(20)
g_acc.Draw("SAME PLZ")
# accFit.SetParameter(0, accFit.GetParameter(0)*100.0)
# accFit.SetRange(10, 60)
# accFit.Draw("SAME")

# Make legend
leg = rt.TLegend(0.35, 0.65, 0.83, 0.85)
leg.AddEntry(h_before_acc, h_before_acc.GetTitle(), "lep")
leg.AddEntry(h_after_acc, h_after_acc.GetTitle(), "lep")
leg.AddEntry(g_acc, "#eta #rightarrow 4#mu acceptance (in %, right axis)", "lep")
leg.AddEntry("", "#bar{B} #in [1.2, 12] #times 10^{-9}", "")
leg.Draw()

# Make axis on the RHS for acceptance
rA = rt.TGaxis(50, 0.1, 50, 1e2, 0.1, 100, 505, "+GL")
rA.SetTitle("Acceptance / 2 GeV (%)")
rA.SetTitleOffset(1.3)
rA.SetTitleFont(frame.GetYaxis().GetLabelFont())
rA.SetTitleSize(frame.GetYaxis().GetLabelSize())
rA.SetLabelFont(frame.GetYaxis().GetLabelFont())
rA.SetLabelSize(frame.GetYaxis().GetLabelSize())
rA.SetTitleColor(g_acc.GetMarkerColor())
rA.SetLabelColor(g_acc.GetMarkerColor())
rA.Draw()

# Print CMS logo and lumi
CMSStyle.setCMSLumiStyle(c1, 10, era='scouting')

# Redraw axis on top and update canvas
c1.RedrawAxis()
c1.Update()