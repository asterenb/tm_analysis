from fileinput import filename
import ROOT as rt
import utils.CMSStyle as CMSStyle
import argparse
import csv
import numpy as np
import utils.configuration as configuration
cfg = configuration.Configuration()

parser = argparse.ArgumentParser()
parser.add_argument('decay_mode', type=int, metavar='DECAY', choices=[2,4], default=4)
args = parser.parse_args()
mode = args.decay_mode
if mode == 4:
    file_in = cfg.fns['four_mu_ntuple_MC']
    file_out_csv = cfg.fns['four_mu_csv']
    file_out_root = cfg.fns['four_mu_acc']
else:
    file_in = cfg.fns['two_mu_ntuple_MC']
    file_out_csv = cfg.fns['two_mu_acc_csv']
    file_out_root = cfg.fns['two_mu_acc']

CMSStyle.setTDRStyle()
rt.gStyle.SetPalette(rt.kBlackBody)
rt.gROOT.ForceStyle()

f_in = rt.TFile.Open(file_in)
tt = f_in.Get("Events")
df = rt.RDataFrame(tt)

trig_sel = "trigFired==1"
reco_sel = f"nMuons=={mode}"
acc_sel = f"nGood>{mode-1} && nVtx>0 && pt[0] > 3.0 && pt[1] > 3.0"

maxRange = 70.0
nBins = int(maxRange)
h_gen = df.Histo1D(("h_gen", "Generated events", nBins, 0., float(maxRange)), "pt_gen_eta")
h_trig = df.Filter(trig_sel).Histo1D(("h_trig", "Triggered events", nBins, 0., float(maxRange)), "pt_gen_eta")
h_reco = df.Filter(reco_sel).Histo1D(("h_reco", "Reconstructed events", nBins, 0., float(maxRange)), "pt_gen_eta")
h_acc = df.Filter(" && ".join([trig_sel, reco_sel, acc_sel])).Histo1D(("h_acc", "Accepted events", nBins, 0., float(maxRange)), "pt_gen_eta")

c1 = rt.TCanvas()
c1.SetTicks()

frame = c1.DrawFrame(0, 0, maxRange, 2000)
frame.GetXaxis().SetTitle("#eta meson p_{T} [GeV]")
frame.GetYaxis().SetTitle("Events")
frame.GetYaxis().SetTitleOffset(1.3)

h_gen_cl = h_gen.Clone()
h_gen_cl.SetLineWidth(2)
h_gen_cl.SetLineColor(rt.kBlack)
h_gen_cl.Draw("SAME")
h_trig_cl = h_trig.Clone()
h_trig_cl.SetLineWidth(2)
h_trig_cl.Draw("PLC PMC SAME")
h_reco_cl = h_reco.Clone()
h_reco_cl.SetLineWidth(2)
h_reco_cl.Draw("PLC PMC SAME")
h_acc_cl = h_acc.Clone()
h_acc_cl.SetLineWidth(2)
h_acc_cl.Draw("PLC PMC SAME")

leg = rt.TLegend(0.4, 0.7, 0.8, 0.9, f"Unweighted MC simulation: #eta #rightarrow {mode}mu")
leg.AddEntry(h_gen_cl)
leg.AddEntry(h_trig_cl)
leg.AddEntry(h_reco_cl)
leg.AddEntry(h_acc_cl)
leg.SetLineWidth(0)
leg.Draw()

# Print CMS logo and lumi
CMSStyle.setCMSLumiStyle(c1, 10, era='scouting')

# Redraw axis on top and update canvas
c1.RedrawAxis()
c1.Update()

c2 = rt.TCanvas()
frame = c2.DrawFrame(0, 0, maxRange, 1.1)
frame.GetXaxis().SetTitle("#eta meson p_{T} [GeV]")
frame.GetYaxis().SetTitle("Efficiency")
c2.SetTicks()

trigEff = rt.TGraphAsymmErrors(h_trig.GetPtr(), h_gen.GetPtr())
trigEff.SetTitle("Trigger efficiency")
trigEff.SetMarkerStyle(rt.kFullCircle)
trigEff.SetMarkerSize(0.8)
trigEff.Draw("PZ PLC PMC SAME")

recoEff = rt.TGraphAsymmErrors(h_reco.GetPtr(), h_gen.GetPtr())
recoEff.SetTitle("Reconstruction efficiency")
recoEff.SetMarkerStyle(rt.kFullCircle)
recoEff.SetMarkerSize(0.8)
recoEff.Draw("PZ PLC PMC SAME")

accEff = rt.TGraphAsymmErrors(h_acc.GetPtr(), h_gen.GetPtr())
accEff.SetName(f"acceptance_EtaTo{mode}Mu")
accEff.SetTitle("Acceptance")
accEff.SetMarkerStyle(rt.kFullCircle)
accEff.SetMarkerSize(0.8)
accEff.Draw("PZ PLC PMC SAME")

fitf = rt.TBinomialEfficiencyFitter(h_acc.GetPtr(), h_gen.GetPtr())

if mode == 4:
    accFit = rt.TF1("accFit", "([0]/(1+TMath::Exp(-[1]*(x-[2]))))", 0, 12)
    accFit.SetParameters(1, 2, 4)
    # accFit = new TF1("accFit", "[0]*TMath::TanH([1]*(x-[2]))", 0, 12)
    # accFit.SetParameters(0.1745, 0.065, 16.2)
else:
    accFit = rt.TF1("accFit", "[0]*TMath::TanH([1]*(x-[2]))", 0, 12)
    accFit.SetParameters(0.7, 0.06, 5.4)
accFit.SetRange(5,maxRange)

status = fitf.Fit(accFit, "IR")
if status == 0:
    accFit.Draw("SAME")

leg_title = f"Efficiencies (MC simulation): #eta #rightarrow {mode}#mu"
if mode == 2:
    leg = rt.TLegend(0.4, 0.7, 0.8, 0.9, leg_title)
else:
    leg = rt.TLegend(0.4, 0.43, 0.8, 0.63, leg_title)
leg.AddEntry(trigEff, trigEff.GetTitle(), "lep")
leg.AddEntry(recoEff, recoEff.GetTitle(), "lep")
leg.AddEntry(accEff, accEff.GetTitle(), "lep")
leg.SetLineWidth(0)
leg.Draw()

# Print CMS logo and lumi
CMSStyle.setCMSLumiStyle(c2, 10, era='scouting')

# Redraw axis on top and update canvas
c2.RedrawAxis()
c2.Update()

pt_edges = cfg.pt_edges
n_bins = len(pt_edges)-1

h_gen2 = df.Histo1D(("h_gen2", "Generated events", n_bins, pt_edges), "pt_gen_eta")
h_trig2 = df.Filter(trig_sel).Histo1D(("h_trig2", "Triggered events", n_bins, pt_edges), "pt_gen_eta")
h_reco2 = df.Filter(reco_sel).Histo1D(("h_reco2", "Reconstructed events", n_bins, pt_edges), "pt_gen_eta")
h_acc2 = df.Filter(" && ".join([trig_sel, reco_sel, acc_sel])).Histo1D(("h_acc2", "Accepted events", n_bins, pt_edges), "pt_gen_eta")

c3 = rt.TCanvas()
c3.SetTicks()

frame = c3.DrawFrame(0, 0, maxRange, 1.1)
frame.GetXaxis().SetTitle("#eta meson p_{T} [GeV]")
frame.GetYaxis().SetTitle("Events")
frame.GetYaxis().SetTitleOffset(1.3)

trigEff2 = rt.TGraphAsymmErrors(h_trig2.GetPtr(), h_gen2.GetPtr())
trigEff2.SetTitle("Trigger efficiency")
trigEff2.SetMarkerStyle(rt.kFullCircle)
trigEff2.SetMarkerSize(0.8)
trigEff2.Draw("PZ PLC PMC SAME")

recoEff2 = rt.TGraphAsymmErrors(h_reco2.GetPtr(), h_gen2.GetPtr())
recoEff2.SetTitle("Reconstruction efficiency")
recoEff2.SetMarkerStyle(rt.kFullCircle)
recoEff2.SetMarkerSize(0.8)
recoEff2.Draw("PZ PLC PMC SAME")

accEff2 = rt.TGraphAsymmErrors(h_acc2.GetPtr(), h_gen2.GetPtr())
accEff2.SetName(f"acceptance_EtaTo{mode}Mu_forPlots")
accEff2.SetTitle("Acceptance")
accEff2.SetMarkerStyle(rt.kFullCircle)
accEff2.SetMarkerSize(0.8)
accEff2.Draw("PZ PLC PMC SAME")

if mode == 2:
    leg2 = rt.TLegend(0.4, 0.7, 0.8, 0.9, leg_title)
else:
    leg2 = rt.TLegend(0.4, 0.43, 0.8, 0.63, leg_title)
leg2.AddEntry(trigEff2, trigEff2.GetTitle(), "lep")
leg2.AddEntry(recoEff2, recoEff2.GetTitle(), "lep")
leg2.AddEntry(accEff2, accEff2.GetTitle(), "lep")
leg2.SetLineWidth(0)
leg2.Draw()

# Print CMS logo and lumi
CMSStyle.setCMSLumiStyle(c3, 10, era='scouting')

c3.Update()

outEff = accEff2 # or accEff

data_out = []
for i in range(1, n_bins - outEff.GetN() - 1):
    x = (pt_edges[i-1] + pt_edges[i]) / 2
    y = 0.0
    row = [i, x, pt_edges[i-1], pt_edges[i], y, 0., 0.]
    data_out.append(row)
for i in range(0, outEff.GetN()):
    x = outEff.GetPointX(i)
    # We must use the finer acceptance binning
    # otherwise we get unreasonable low-pt behavior
    # y = accFit.Eval(x)
    y = outEff.GetPointY(i)
    # y = accEff.Eval(x)
    row = [i+6, \
            x, \
            x - outEff.GetErrorXlow(i), \
            x + outEff.GetErrorXhigh(i), \
            y, \
            outEff.GetErrorYlow(i), \
            outEff.GetErrorYhigh(i), \
        ]
    data_out.append(row)
# For now copy the acc of the 55--70 GeV bin to the 70--100 GeV bin
data_out.append([outEff.GetN()+6, 85.0, 70.0, 100.0, row[4], row[5], row[6]])
data_out.insert(0, ['bin', 'pt_center', 'pt_low', 'pt_high', f'A_{mode}mu', f'A_{mode}mu_error_low', f'A_{mode}mu_error_high'])

with open(file_out_csv, 'w') as f:
    writer = csv.writer(f)
    writer.writerows(data_out)

f_out = rt.TFile.Open(file_out_root, "RECREATE")
accEff.SetMarkerColor(rt.kBlack)
accEff.SetLineColor(rt.kBlack)
accEff.Write()
accEff2.SetMarkerColor(rt.kBlack)
accEff2.SetLineColor(rt.kBlack)
accEff2.Write()
accFit.Write()
f_out.Close()
