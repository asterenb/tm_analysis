import ROOT as rt
import utils.CMSStyle as CMSStyle
import math
from array import array
from utils.blinding import *
import utils.configuration as configuration
cfg = configuration.Configuration()

# CMSStyle.setTDRStyle()

# Given corrected eta meson differential production x-sec (from Eta->2mu), 
# predicted BR(Eta->4mu), and scouting lumi, 
# computes differential yield of eta to 4 mu events, and MC gen weights

f_xsec = rt.TFile.Open(cfg.fns['xsecs'])
h_corrXsec = f_xsec.Get("corr_xsec")

BR = get_blinded_BR() # can't look at the actual BR (i.e. don't print it out)
lumi = 101.3 # 1/fb

h_nEtaTo4Mu = h_corrXsec.Clone()
h_nEtaTo4Mu.SetName("h_nEtaTo4Mu")
h_nEtaTo4Mu.SetTitle("")
h_nEtaTo4Mu.Scale(BR * lumi)
h_nEtaTo4Mu.SetMarkerStyle(rt.kFullCircle)
h_nEtaTo4Mu.SetMarkerSize(0.8)
h_nEtaTo4Mu.SetDrawOption("E")
h_nEtaTo4Mu.SetMaximum(1e7)
h_nEtaTo4Mu.GetYaxis().SetTitle("Events")
h_nEtaTo4Mu.GetXaxis().SetTitle("p_{T}^{#mu#mu} [GeV]")
h_nEtaTo4Mu.SetStats(0)

# now take MC eta to 4 mu events and compute weight based on the hist above
# weight = N_phys / N_MC bin-by-bin in eta meson pT
# also compute sum of weights -> integral of h_EtaTo4Mu or N_Phys

f_ntuple = rt.TFile.Open(cfg.fns['four_mu_ntuple_MC'])#, "UPDATE")
tree = f_ntuple.Get("Events")

h_pt_temp = rt.TH1D("h_pt_temp", "h_pt_temp", 50, 0., 50.)
tree.Draw("pt_gen_eta>>h_pt_temp", "", "GOFF")
h_pt = h_corrXsec.Clone()
h_pt.SetBinContent(0, 1.0)
h_pt.SetBinContent(-1, 1.0)
for bin_i in range(1, h_pt.GetNbinsX()+1):
    if h_pt_temp.FindBin(h_pt.GetBinCenter(bin_i)) >= h_pt_temp.GetNbinsX():
        h_pt.SetBinContent(bin_i, h_pt_temp.GetBinContent(h_pt_temp.GetNbinsX()))
        h_pt.SetBinError(bin_i, math.sqrt(h_pt.GetBinContent(bin_i)))
    else:
        h_pt.SetBinContent(bin_i, h_pt_temp.GetBinContent(h_pt_temp.FindBin(h_pt.GetBinCenter(bin_i))))
        h_pt.SetBinError(bin_i, math.sqrt(h_pt.GetBinContent(bin_i)))
h_pt.SetMarkerStyle(rt.kFullCircle)
h_pt.SetMarkerSize(0.8)
h_pt.SetMarkerColor(rt.kBlack)

c1 = rt.TCanvas()
c1.SetTicks()

h_ratio = rt.TRatioPlot(h_nEtaTo4Mu, h_pt)
h_ratio.SetH1DrawOpt("E")
h_ratio.SetH2DrawOpt("E")
h_ratio.SetGraphDrawOpt("PZA")
h_ratio.Draw("nogrid")
h_ratio.GetLowerRefGraph().SetMarkerStyle(rt.kFullCircle)
h_ratio.GetLowerRefGraph().SetMarkerSize(0.8)
h_ratio.GetLowerRefGraph().SetMarkerColor(rt.kRed+1)
h_ratio.GetLowYaxis().SetNdivisions(4)
h_ratio.GetLowerRefYaxis().SetTitle("Weights")
h_ratio.GetLowerRefXaxis().SetTitleOffset(1.25)
h_ratio.GetLowerRefGraph().SetMinimum(1e-6)
h_ratio.GetLowerRefGraph().SetMaximum(0.9e+3)
h_ratio.GetLowerPad().SetLogy()
h_ratio.GetUpperPad().SetLogy()
h_ratio.GetUpperPad().SetTitle("")
h_ratio.GetLowerPad().SetTitle("")

leg = rt.TLegend(0.3, 0.7, 0.8, 0.83)
leg.AddEntry(h_pt, "Unweighted p_{T} spectrum from #eta #rightarrow 4#mu MC", "lep")
leg.AddEntry(h_nEtaTo4Mu, "Differential #eta #rightarrow 4#mu production from #eta #rightarrow 2#mu", "lep")
leg.SetLineWidth(0)
upper_pad = h_ratio.GetUpperPad()
upper_pad.cd()
leg.Draw()

CMSStyle.setCMSLumiStyle(h_ratio.GetUpperPad(), 10, era='scouting')
c1.Update()

gr_ratio = h_ratio.GetLowerRefGraph().Clone()
f_out = rt.TFile.Open(cfg.fns['gen_weights'], "RECREATE")
gr_ratio.SetName("weights")
gr_ratio.Write()
# f_out.Close()

# f_ntuple.cd()

tree_friend = rt.TTree('weights_tt', 'weights_tt')
weight = array( 'f', [ 0 ] )
tree_friend.Branch('weight', weight, 'weight/F')

for i in range(tree.GetEntries()):
    tree.GetEntry(i)
    weight[0] = max(0., gr_ratio.Eval(tree.pt_gen_eta))
    tree_friend.Fill()

tree_friend.Write()#"", rt.TObject.kOverwrite)

f_out.Close()
f_ntuple.Close()