from tkinter import N
import ROOT as rt
import utils.CMSStyle as CMSStyle
from utils.fitter import fitter_4mu as fitter
import utils.configuration as configuration
config = configuration.Configuration()

CMSStyle.setTDRStyle()

f = rt.TFile.Open(config.four_mu_ntuple_data)

tt = f.Get("Events")

minX = 0.46+0.00275
maxX = 0.9+0.00275

h = rt.TH1D("hFourMuons", "hFourMuons", 80, minX, maxX)
h.SetMarkerColor(rt.kBlack)
h.SetMarkerStyle(20)
h.SetMarkerSize(0.6)
h.SetLineColor(rt.kBlack)

c1 = rt.TCanvas()
c1.SetTicks()
frame = c1.DrawFrame(minX, 0, maxX, 80)
frame.GetYaxis().SetTitle("Events")
frame.GetXaxis().SetTitle("m_{4#mu} [GeV]")

# Earlier versions of the ntuple:
# selection = "invMass > 0.1 && invMass < 1.0 && nGood > 3"
selection = "mass > 0.1 && mass < 1.0 && nGood > 3 && nVtx > 0 && charge_4mu == 0"
tt.Draw("mass>>hFourMuons", selection, "GOFF")


# Construct observable
t = rt.RooRealVar("m_{4#mu}","m_{4#mu} [GeV]", minX, maxX)
t.setRange("peak", 0.53, 0.57)
t.setRange("full", minX, maxX)

# Construct fit model
fit = fitter(mass=t, bkg_model='Threshold', sig_model='Johnson')

data = rt.RooDataHist("data_obs", "data_obs", t, rt.RooFit.Import(h))

result = fit.model.fitTo(data, rt.RooFit.Save())
# fit.scb.setConstant()
# fit.ncb.setConstant()
# fit.mcb.setConstant()
# fit.acb.setConstant()
# fit.save_workspace(data, "test_wspace.root")

frame = t.frame()
data.plotOn(frame, Name="Data", DrawOption="PEZ")
fit.model.plotOn(frame, Name="Bkg", Components={fit.bkg()}, LineWidth=1, LineStyle='--', LineColor=rt.kGreen-1)
fit.model.plotOn(frame, Name="Sig", Components={fit.sig()}, LineWidth=1, LineStyle='-.', LineColor=rt.kRed+1)
fit.model.plotOn(frame, Name="Tot", LineWidth=1, LineColor=rt.kBlue)

c2 = rt.TCanvas()
frame.SetMaximum(60)
frame.Draw("SAME")

# Compute yields
ndata = h.Integral(h.FindBin(0.53), h.FindBin(0.57))
sig_int = fit.sig().createIntegral(t, rt.RooFit.NormSet(t), rt.RooFit.Range("peak"))
bkg_int = fit.bkg().createIntegral(t, rt.RooFit.NormSet(t), rt.RooFit.Range("peak"))
tot_int = fit.model.createIntegral(t, rt.RooFit.NormSet(t), rt.RooFit.Range("peak"))
nsig = sig_int.getVal() * fit.nsig.getVal()
nbkg = bkg_int.getVal() * fit.nbkg.getVal()
ntot = tot_int.getVal() * (fit.nsig.getVal() + fit.nbkg.getVal())

leg = rt.TLegend(0.4, 0.65, 0.8, 0.85)
leg.SetHeader("N: m_{4#mu} #in [0.53, 0.57] GeV")
leg.SetLineWidth(0)
leg.AddEntry("Sig", f"Signal, N = {nsig:.1f}", "l")
leg.AddEntry("Bkg", f"Background, N = {nbkg:.1f}", "l")
leg.AddEntry("Tot", f"Sum, N = {ntot:.1f}", "l")
leg.AddEntry("Data", f"Data, N = {ndata:n}", "lep")
leg.Draw()

# Print CMS logo and lumi
CMSStyle.setCMSLumiStyle(c2, 10, era='scouting')

print(result.Print())

print(f"Signal yield error in peak region: {sig_int.getVal() * fit.nsig.getError()}")
print(f"Background yield error in peak region: {bkg_int.getVal() * fit.nbkg.getError()}")

c2.Update()
