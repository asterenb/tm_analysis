# Analysis workflow

The typical analysis workflow is described below. All quoted scripts are in the `python` directory.

## 0. Setup the environment

First source some ROOT version (preferably a recent version such as 6.2X.YY) either in your computer or on CVMFS.

Then source the bash environment invoking:

```bash
source env.sh
```

in the main `analysis` folder. (It will complain if it can't find ROOT.)

## 1. Measure $`N_{4\mu}`$ in data

This is done with the `fit_N4mu_data.py` script:

```bash
python3 -i fit_N4mu_data.py
```

which produces a plot where $`N_{4\mu}`$ can be read off manually. (TODO: put this number into a file to be read automatically by the BR script.)

## 2. Measure $`N_{2\mu}^i`$ for all $`p_T`$ bins $`i`$ in data

A similar script exists for $`N_{2\mu}^i`$ as a function of $`p_T`$:

```bash
python3 -i fit_N2mui.py
```

This saves a file called `data_out/two_mu_yields.csv` with the extracted yield for each pT bin and associated fit uncertainty. Also a ROOT file with all the fits (and chi2 + pull information) is saved in `data_out/fits.root`.

(The canvases inside `fits.root` can be printed out to .png and .pdf with the `print_fit_canvas.py` script.)

## 3. Measure $`A_{4\mu}`$

The CMS acceptance to four-muon decays as a function of $`p_T`$ is measured with a simulation sample by invoking the script:

```bash
python3 -i plot_eta_acceptance.py 4
```

where the `4` corresponds to the four-muon decay. This produces a .csv file `data_out/four_mu_acc.csv` and a ROOT file `data_out/acceptance_EtaTo4Mu.root` with the actual plot and also a fit if desired.

## 4. Measure $`A_{2\mu}^i`$ for all $`p_T`$ bins $`i`$

Similarly, the two-muon decay acceptance is computed with:

```bash
python3 -i plot_eta_acceptance.py 2
```

This produces a .csv file `data_out/two_mu_acc.csv` and a ROOT file `data_out/acceptance_EtaTo2Mu.root` with the plot.

## 5. Measure systematics for $`N_{4\mu}`$ (fit variations)

Fit variation systematics can be estimated by varying the background and/or signal model in [fit_N4mu_data.py](https://gitlab.cern.ch/asterenb/tm_analysis/-/blob/master/analysis/python/fit_N4mu_data.py#L41). The default signal model is `CB` and the default background model is `Threshold`. After changing one or both of those, re-run the script and record the new signal yield. The BR is linearly proportional to this yield so the variation informs the systematic uncertainty directly.

## 7. Measure systematics for $`N_{2\mu}^i`$ (fit variations)

TBC.

## 8. Calculate $`B_{4\mu}`$

The branching ratio can be calculated with the script:

```bash
python3 -i calc_B_4mu.py
```

This takes as input all the .csv files abovea and a hardcoded number for $`N_{4\mu}`$, and computes the final (blinded) branching ratio, printing it out to the screen.

# Additional measurements / plots / scripts

Some other useful scripts are not part of the "main" analysis workflows. These are described here.
