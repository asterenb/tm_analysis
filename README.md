# TM_Analysis

Analysis tools for search for True Muonium and rare eta meson decays.

## ntupler

Scripts to ntuplize the scouting data and the MC simulation samples.

## analysis

Analysis scripts in Python3 to run over the ntuples and compute the observables that go into the branching ratio calculation.

Check the [README](analysis/README.md) for more details.

## toyMC

Scripts for toy MC studies of the resonant backgrounds.
